import { FormProps } from '../components/Form';
import { Gutter } from 'ant-design-vue/lib/grid/Row';
const horizontal: Gutter = 20;
const vertical: Gutter = 12;
// 表格通用样式
export const tableStyleSetting: any = {
  showIndexColumn: false,
  immediate: true,
  showSummary: false,
  clickToRowSelect: false,
  showTableSetting: true,
  useSearchForm: true,
  striped: true,
  bordered: true,
  actionColumn: {
    title: '操作',
    width: 150,
    flag: 'ACTION',
    dataIndex: 'action',
  },
};
// 表格内form通用样式
export const tableFormStyleSetting: FormProps = {
  rowProps: {
    gutter: [horizontal, vertical],
  },
  baseColProps: {
    style: {
      fontSize: '14px',
      height: '32px',
      borderRadius: '2px',
    },
    md: { span: 8, offset: 0 },
    lg: { span: 6, offset: 0 },
    xl: { span: 4, offset: 0 },
    xxl: { span: 4, offset: 0 },
  },
};
// 弹窗通用间距样式
export const modalStyleSetting: any = {
  bodyStyle: {
    margin: '10px',
  },
  width: 580,
};
// 弹窗内form通用样式
export const modalFormStyleSetting: FormProps = {
  rowProps: {
    gutter: horizontal,
  },
  wrapperCol: {
    style: {
      marginBottom: '4px',
    },
  },
  labelCol: {
    style: {
      marginBottom: '12px',
      padding: 0,
    },
  },
  baseColProps: {
    span: 12,
  },
};
// 抽屉内form通用样式
export const publicDrawerTableFormStyleSetting: FormProps = {
  rowProps: {
    gutter: [horizontal, vertical],
  },
  baseColProps: {
    style: {
      fontSize: '14px',
      height: '32px',
      borderRadius: '2px',
    },
    md: { span: 12, offset: 0 },
    lg: { span: 8, offset: 0 },
    xl: { span: 6, offset: 0 },
    xxl: { span: 6, offset: 0 },
  },
};
