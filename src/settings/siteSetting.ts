// github repo url
export const GITHUB_URL = 'https://gitee.com/tpsonwell_admin/thingsvue';

// doc
export const DOC_URL = 'https://alidocs.dingtalk.com/i/p/ZR2PmKNyAk7XvpO7ZBGLvgyO0AAQnXL1';

// site url
export const SITE_URL = 'https://gitee.com/tpsonwell_admin/thingsvue';
