import { FormSchema } from '/@/components/Form/index';
import {
  getRuleChainList,
  getDashboards,
  getQueues,
} from '/@/api/thingsborad/profile/deviceProfile/deviceProfile';
export const schemasAdd: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    colProps: {
      span: 12,
    },
    required: true,
  },
  {
    field: 'defaultRuleChainId',
    component: 'ApiSelect',
    label: '默认规则链',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'CORE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
      itemProps: {},
    },
  },
  {
    field: 'defaultDashboardId',
    component: 'ApiSelect',
    label: '移动端仪表盘',
    helpMessage: '被移动端应用用作资产详情仪表板',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getDashboards,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'title',
        sortOrder: 'ASC',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
      listHeight: 160,
    },
  },
  {
    field: 'defaultQueueName',
    component: 'ApiSelect',
    label: '队列',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getQueues,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        serviceType: 'TB_RULE_ENGINE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'name',
      immediate: true,
    },
  },
  {
    field: 'defaultEdgeRuleChainId',
    component: 'ApiSelect',
    label: '默认边缘规则链',
    helpMessage: '作为规则链，用于在边缘上处理属于此资产配置中资产的传入数据',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'EDGE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
  },
  {
    field: 'image',
    component: 'Upload',
    label: '资产配置图片',
    slot: 'img',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];

export const schemasImport: FormSchema[] = [
  {
    field: 'Json',
    component: 'Upload',
    label: '设备配置',
    colProps: {
      span: 24,
    },
  },
];
