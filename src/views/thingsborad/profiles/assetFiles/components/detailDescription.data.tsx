import { DescItem } from '/@/components/Description/index';
export const DescriptionSchema: DescItem[] = [
  {
    field: 'name',
    label: '名称',
  },
  {
    field: 'defaultRuleChain',
    label: '默认规则链',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'defaultDashboard',
    label: '移动端仪表盘',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'defaultQueueName',
    label: '队列',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'defaultEdgeRuleChain',
    label: '默认边缘规则链',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'description',
    label: '说明',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'image',
    label: '资产配置图片',
    render: (data) => {
      if (data === null || data === '') {
        return <span>— —</span>;
      } else {
        return (
          <div style="width:150px;height:150px">
            <img src={data} alt="资产配置图片" />
          </div>
        );
      }
    },
  },
];
