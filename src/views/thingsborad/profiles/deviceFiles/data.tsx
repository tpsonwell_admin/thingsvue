import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { FormSchema } from '/@/components/Form';
export const getBasicColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    fixed: 'left',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '配置类型',
    dataIndex: 'type',
    sorter: true,
  },
  {
    title: '传输方式',
    dataIndex: 'transportType',
    sorter: true,
  },
  {
    title: '说明',
    dataIndex: 'description',
    sorter: true,
  },
  {
    title: '默认',
    dataIndex: 'default',
    sorter: true,
    customRender: ({ record }) => {
      const icon = record.default
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    label: '',
    componentProps: {
      placeholder: '设备配置名称',
    },
    component: 'Input',
  },
];
