import { FormSchema } from '/@/components/Form/index';
import {
  getRuleChainList,
  getDashboards,
  getQueues,
} from '/@/api/thingsborad/profile/deviceProfile/deviceProfile';
export const schemas: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    colProps: {
      span: 12,
    },
    required: true,
  },
  {
    field: 'defaultRuleChainId',
    component: 'ApiSelect',
    label: '默认规则链',
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'CORE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
    colProps: {
      span: 12,
    },
  },
  {
    field: 'defaultDashboardId',
    component: 'ApiSelect',
    label: '移动端仪表盘',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getDashboards,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'title',
        sortOrder: 'ASC',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
  },
  {
    field: 'defaultQueueName',
    component: 'ApiSelect',
    label: '队列',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getQueues,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        serviceType: 'TB_RULE_ENGINE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'name',
      immediate: true,
    },
  },
  {
    field: 'defaultEdgeRuleChainId',
    component: 'ApiSelect',
    label: '默认边缘规则链',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'EDGE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
  },
  {
    field: 'firmwareId',
    component: 'Select',
    label: '分配的固件',
    colProps: {
      span: 12,
    },
    componentProps: {},
  },
  {
    field: 'softwareId',
    component: 'Select',
    label: '分配的软件',
    colProps: {
      span: 12,
    },
    componentProps: {},
  },
  {
    field: 'image',
    component: 'Upload',
    slot: 'img',
    label: '设备配置图片',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];
