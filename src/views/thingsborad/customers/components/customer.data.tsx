import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { DescItem } from '/@/components/Description';
import dayjs from 'dayjs';

export const columns: BasicColumn[] = [
  {
    title: '部门名称',
    dataIndex: 'deptName',
    width: 160,
    align: 'left',
  },
  {
    title: '排序',
    dataIndex: 'orderNo',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '备注',
    dataIndex: 'deptName',
    width: 180,
  },
];
export const customersColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.title === null ? '--' : record.title;
    },
  },
  {
    title: '电子邮件',
    dataIndex: 'email',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.email === null ? '--' : record.email;
    },
  },
  {
    title: '城市',
    dataIndex: 'city',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.city === null ? '--' : record.city;
    },
  },
];

export const adminColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '名字',
    dataIndex: 'firstName',
    width: 160,
    sorter: true,
  },
  {
    title: '姓氏',
    dataIndex: 'lastName',
    width: 160,
    sorter: true,
  },
  {
    title: '电子邮件',
    dataIndex: 'email',
    width: 160,
    sorter: true,
  },
];

export const detailSchema: DescItem[] = [
  {
    label: '标题',
    field: 'title',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: (_val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    label: '电子邮件',
    field: 'email',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '手机号码',
    field: 'phone',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val.replace(/^\+86/, '')}</span>;
    },
  },

  {
    label: '省份',
    field: 'state',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '城市',
    field: 'city',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '邮政编码',
    field: 'zip',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '地址',
    field: 'address',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return <span>{val === null || val === '' ? '— —' : val.description}</span>;
    },
  },
];

export const userColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '名字',
    dataIndex: 'firstName',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.firstName === '' || record.firstName === null ? '--' : record.firstName;
    },
  },
  {
    title: '电子邮件',
    dataIndex: 'email',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.email === '' || record.email === null ? '--' : record.email;
    },
  },
];

export const propertyColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '名称',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '资产配置',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '标签',
    dataIndex: 'deptName',
    width: 180,
  },
];

export const devicesColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '名称',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '设备配置',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '标签',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '状态',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '是否网关',
    dataIndex: 'deptName',
    width: 180,
  },
];

export const lineColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '标题',
    dataIndex: 'deptName',
    width: 180,
  },
];

export const routerColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '名称',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '边缘类型',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '标签',
    dataIndex: 'deptName',
    width: 180,
  },
];

export const userdetailSchema: DescItem[] = [
  {
    label: '电子邮件',
    field: 'email',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '名字',
    field: 'firstName',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: (_val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },

  {
    label: '手机号码',
    field: 'phone',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val.replace(/^\+86/, '')}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return (
        <span>{val.description === '' || val.description === null ? '— —' : val.description}</span>
      );
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  // {
  //   field: 'field6',
  //   component: 'Switch',
  //   label: '包括客户实体',
  //   colProps: {
  //     span: 8,
  //   },
  //   labelWidth: 200,
  // },
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入关键字',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '标题',
    component: 'Input',
    required: true,
  },
  {
    field: 'deptName2',
    label: '描述',
    component: 'InputTextArea',
    required: false,
  },
  {
    field: 'orderNo',
    label: '允许白标',
    component: 'Checkbox',
    required: false,
  },
  {
    field: 'deptName3',
    label: '主页仪表板',
    component: 'Input',
    required: false,
  },
  {
    field: 'orderNo2',
    label: '隐藏主页仪表板工具栏',
    component: 'Checkbox',
    required: false,
  },
  {
    field: 'status',
    label: '国家',
    component: 'Select',
    defaultValue: '',
    componentProps: {
      options: [],
    },
  },
  {
    field: 'status2',
    label: '城市',
    component: 'Select',
    defaultValue: '',
    componentProps: {
      options: [],
    },
  },
  {
    field: 'status3',
    label: '省/自治区/直辖市',
    component: 'Select',
    defaultValue: '',
    componentProps: {
      options: [],
    },
  },
  {
    field: 'status4',
    label: '邮政编码',
    component: 'Select',
    defaultValue: '',
    componentProps: {
      options: [],
    },
  },
  {
    field: 'deptName4',
    label: '地址',
    component: 'Input',
    required: false,
  },
  {
    field: 'deptName5',
    label: '地址2',
    component: 'Input',
    required: false,
  },
];
