export const keyWordsTranslation = {
  GENERAL: '通用',
  ALARM: '告警',
  DEVICE_ACTIVITY: '设备活动',
  ENTITY_ACTION: '实体操作',
  ALARM_COMMENT: '告警评论',
  RULE_ENGINE_COMPONENT_LIFECYCLE_EVENT: '规则引擎生命周期事件',
  ALARM_ASSIGNMENT: '告警分配',
  NEW_PLATFORM_VERSION: '新的平台版本',
  ENTITIES_LIMIT: '实体限制',
  API_USAGE_LIMIT: 'API使用限制',
  RULE_NODE: '规则节点',
  RATE_LIMITS: '速率限制',
};

export const resetNotificationData = {
  prefs: {
    GENERAL: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    ALARM: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    DEVICE_ACTIVITY: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    ENTITY_ACTION: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    ALARM_COMMENT: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    RULE_ENGINE_COMPONENT_LIFECYCLE_EVENT: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    ALARM_ASSIGNMENT: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    NEW_PLATFORM_VERSION: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    ENTITIES_LIMIT: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    API_USAGE_LIMIT: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    RULE_NODE: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
    RATE_LIMITS: {
      enabled: true,
      enabledDeliveryMethods: {
        SMS: true,
        EMAIL: true,
        WEB: true,
      },
    },
  },
};
