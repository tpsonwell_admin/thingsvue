import dayjs from 'dayjs';
export const echartsPublicOptions: any = (dateKey?: string) => {
  let minInterval = 0;
  let maxInterval = 0;
  if (dateKey) {
    switch (dateKey) {
      case 'day':
        minInterval = 3600 * 1000 * 5;
        maxInterval = 3600 * 1000 * 5;
        break;
      case 'month':
        minInterval = 3600 * 1000 * 24 * 10;
        maxInterval = 3600 * 1000 * 24 * 10;
        break;
      case 'year':
        minInterval = 3600 * 1000 * 24 * 30 * 2;
        maxInterval = 3600 * 1000 * 24 * 30 * 2;
        break;
    }
  } else {
    minInterval = 3600 * 1000 * 24;
    maxInterval = 3600 * 1000 * 24;
  }
  return {
    grid: {
      left: '0%',
      right: '0%',
      bottom: '0%',
      top: '18%',
      containLabel: true,
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
    },
    dataZoom: {
      type: 'inside',
    },
    xAxis: {
      type: 'time',
      minInterval,
      maxInterval,
      boundaryGap: false,
      axisLabel: {
        formatter: {
          year: '{yyyy}',
          month: '{MMM}',
          day: '{d}',
          hour: '{HH}:{mm}',
          minute: '{HH}:{mm}',
          second: '{HH}:{mm}:{ss}',
          millisecond: '{hh}:{mm}:{ss} {SSS}',
          none: '{yyy}-{MM}-{dd} {hh}:{mm}:{ss} {SSS}',
        },
      },
    },
    yAxis: {
      type: 'value',
    },
  };
};

export const emptyOptions: any = {
  title: {
    text: '暂无数据',
    x: 'center',
    y: 'center',
    textStyle: {
      color: '#fff',
      fontWeight: 'normal',
      fontSize: 16,
    },
  },
};

export const getSeriesData = (list, dateKey?: string) => {
  let length = 0;
  switch (dateKey) {
    case 'day':
      length = 24;
      break;
    case 'month':
      length = dayjs().daysInMonth();
      break;
    case 'year':
      length = 12;
      break;
  }
  const result = new Array(length).fill(0);
  console.log(result);
  if (list.length === 0) return [];
};
