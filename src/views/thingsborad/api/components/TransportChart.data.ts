export const historyCmdObject = {
  transport: {
    historyCmd: {
      keys: ['transportMsgCountHourly', 'transportDataPointsCountHourly'],
      agg: 'SUM',
    },
  },
  rule: {
    historyCmd: {
      keys: ['ruleEngineExecutionCountHourly'],
      agg: 'SUM',
    },
  },
  JavaScript: {
    historyCmd: {
      keys: ['jsExecutionCountHourly', 'tbelExecutionCountHourly'],
      agg: 'SUM',
    },
  },
  storageData: {
    historyCmd: {
      keys: ['storageDataPointsCountHourly'],
      agg: 'SUM',
    },
  },
  alarm: {
    historyCmd: {
      keys: ['createdAlarmsCountHourly'],
      agg: 'SUM',
    },
  },
  notifications: {
    historyCmd: {
      keys: ['emailCountHourly', 'smsCountHourly'],
      agg: 'SUM',
    },
  },
};
