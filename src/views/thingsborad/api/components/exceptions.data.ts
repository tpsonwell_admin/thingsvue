import { BasicColumn } from '/@/components/Table/src/types/table';
export const tableSchema: BasicColumn[] = [
  {
    title: '时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    // sorter: true,
  },
  {
    title: '规则链',
    dataIndex: 'ruleChainName',
  },
  {
    title: '规则节点	',
    dataIndex: 'ruleNodeName',
  },
  {
    title: '最近错误',
    dataIndex: 'message',
  },
];
