export interface chartINT {
  transportDataPointsCountHourly: object;
  ruleEngineExecutionCountHourly: object;
  jsExecutionCountHourly: object;
  storageDataPointsCountHourly: object;
  createdAlarmsCountHourly: object;
  emailCountHourly: object;
}

const cmd_1Key = [
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportMsgLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportMsgCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportDataPointsLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportDataPointsCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
];

const cmd_2Key = [
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineExecutionLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineExecutionCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'ruleEngineApiState',
  },
];

const cmd_3Key = [
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'jsExecutionApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'tbelExecutionApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'tbelExecutionLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'tbelExecutionCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'tbelExecutionApiState',
  },
];

const cmd_4Key = [
  {
    type: 'TIME_SERIES',
    key: 'dbApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'storageDataPointsLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'storageDataPointsCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'dbApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'dbApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'dbApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'dbApiState',
  },
];

const cmd_5Key = [
  {
    type: 'TIME_SERIES',
    key: 'alarmApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'createdAlarmsLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'createdAlarmsCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'alarmApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'alarmApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'alarmApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'alarmApiState',
  },
];

const cmd_6Key = [
  {
    type: 'TIME_SERIES',
    key: 'emailApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'emailLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'emailCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'smsApiState',
  },
  {
    type: 'TIME_SERIES',
    key: 'smsLimit',
  },
  {
    type: 'TIME_SERIES',
    key: 'smsCount',
  },
  {
    type: 'TIME_SERIES',
    key: 'transportApiState',
  },
];

export interface cardGrid {
  title: string;
  key: string;
  value: number;
}
export interface TIME_SERIES {
  ts: number;
  value: string;
}
export interface entityFilter {
  type: string;
  singleEntity?: { id: string; entityType: string };
  resolveMultiple?: boolean;
  assetNameFilter?: string;
  assetTypes?: Array<string>;
}
export interface pageLink {
  pageSize: number;
  page: number;
  sortOrder: {
    key: {
      type: string;
      key: string;
    };
    direction: string;
  };
}
export interface entityFields {
  type: string;
  key: string;
}
export interface query {
  entityFilter?: entityFilter;
  pageLink?: pageLink;
  entityFields?: entityFields[];
  latestValues: entityFields[];
}
export interface entityDataCmd {
  query: query;
  cmdId?: number;
}
export const findParams = {
  entityFilter: {
    type: 'apiUsageState',
    resolveMultiple: false,
  },
  pageLink: {
    pageSize: 1,
    page: 0,
    sortOrder: {
      key: {
        type: 'ENTITY_FIELD',
        key: 'createdTime',
      },
      direction: 'DESC',
    },
  },
  entityFields: [
    {
      type: 'ENTITY_FIELD',
      key: 'name',
    },
    {
      type: 'ENTITY_FIELD',
      key: 'label',
    },
    {
      type: 'ENTITY_FIELD',
      key: 'additionalInfo',
    },
  ],
};
// 初始上传
export const entityDataCmds: entityDataCmd[] = [
  {
    query: {
      latestValues: cmd_1Key,
    },
  },
  {
    query: {
      latestValues: cmd_2Key,
    },
  },
  {
    query: {
      latestValues: cmd_3Key,
    },
  },
  {
    query: {
      latestValues: cmd_4Key,
    },
  },
  {
    query: {
      latestValues: cmd_5Key,
    },
  },
  {
    query: {
      latestValues: cmd_6Key,
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      latestValues: [],
    },
  },
  {
    query: {
      entityFilter: {
        type: 'assetType',
        resolveMultiple: true,
        assetNameFilter: '',
        assetTypes: ['TbServiceQueue'],
      },
      latestValues: [],
    },
  },
  {
    query: {
      entityFilter: {
        type: 'assetType',
        resolveMultiple: true,
        assetNameFilter: '',
        assetTypes: ['TbServiceQueue'],
      },
      latestValues: [],
    },
  },
  {
    query: {
      entityFilter: {
        type: 'assetType',
        resolveMultiple: true,
        assetNameFilter: '',
        assetTypes: ['TbServiceQueue'],
      },
      latestValues: [],
    },
  },
];

export const latestDataCmds = [
  {
    cmdId: 1,
    latestCmd: {
      keys: cmd_1Key,
    },
  },
  {
    cmdId: 2,
    latestCmd: {
      keys: cmd_2Key,
    },
  },
  {
    cmdId: 3,
    latestCmd: {
      keys: cmd_3Key,
    },
  },
  {
    cmdId: 4,
    latestCmd: {
      keys: cmd_4Key,
    },
  },
  {
    cmdId: 5,
    latestCmd: {
      keys: cmd_5Key,
    },
  },
  {
    cmdId: 6,
    latestCmd: {
      keys: cmd_6Key,
    },
  },
  {
    cmdId: 7,
    tsCmd: {
      keys: ['transportMsgCountHourly', 'transportDataPointsCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 8,
    tsCmd: {
      keys: ['ruleEngineExecutionCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 9,
    tsCmd: {
      keys: ['jsExecutionCountHourly', 'tbelExecutionCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 10,
    tsCmd: {
      keys: ['storageDataPointsCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 11,
    tsCmd: {
      keys: ['createdAlarmsCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 12,
    tsCmd: {
      keys: ['emailCountHourly', 'smsCountHourly'],
      limit: 1000,
    },
  },
  {
    cmdId: 13,
    tsCmd: {
      keys: ['successfulMsgs', 'failedMsgs', 'tmpFailed'],
      limit: 1000,
    },
  },
  {
    cmdId: 14,
    tsCmd: {
      keys: ['timeoutMsgs', 'tmpTimeout'],
      limit: 1000,
    },
  },
  {
    cmdId: 15,
    tsCmd: {
      keys: ['ruleEngineException', 'ruleEngineException', 'ruleEngineException'],
      limit: 100,
    },
  },
];
