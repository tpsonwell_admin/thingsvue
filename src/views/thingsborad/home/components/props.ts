import { PropType } from 'vue';
import { usePermission } from '/@/hooks/web/usePermission';
const { hasPermission } = usePermission();

export const basicProps = {
  width: {
    type: String as PropType<string>,
    default: '100%',
  },
  height: {
    type: String as PropType<string>,
    default: '147px',
  },
};

export const getQuickLinks = () => {
  const QUICK_LINKS = {
    TbhomePage: {
      label: '首页',
      value: 'TbhomePage',
      permission: true,
    },
    Devices: {
      label: '设备',
      value: 'Devices',
      permission: hasPermission(['TENANT_ADMIN', 'CUSTOMER_USER']),
    },
    alarm: {
      label: '告警',
      value: 'alarm',
      permission: hasPermission(['TENANT_ADMIN', 'CUSTOMER_USER']),
    },
    Version: {
      label: '版本控制',
      value: 'Version',
      permission: hasPermission(['TENANT_ADMIN']),
    },
    TbdashboardsPage: {
      label: '仪表盘',
      value: 'TbdashboardsPage',
      permission: hasPermission(['TENANT_ADMIN', 'CUSTOMER_USER']),
    },
    TbrulePage: {
      label: '规则链',
      value: 'TbrulePage',
      permission: hasPermission(['TENANT_ADMIN']),
    },
    Widgets: {
      label: '部件包',
      value: 'Widgets',
      permission: hasPermission(['TENANT_ADMIN']),
    },
    TbcustomersPage: {
      label: '客户',
      value: 'TbcustomersPage',
      permission: hasPermission(['TENANT_ADMIN']),
    },
    Asset: {
      label: '资产',
      value: 'Asset',
      permission: hasPermission(['TENANT_ADMIN', 'CUSTOMER_USER']),
    },
    DeviceFiles: {
      label: '设备配置',
      value: 'DeviceFiles',
      permission: hasPermission(['TENANT_ADMIN']),
    },
    Notification: {
      label: '通知中心',
      value: 'Notification',
      permission: true,
    },
  };

  const filteredLinks = {};

  Object.keys(QUICK_LINKS).forEach((item) => {
    QUICK_LINKS[item].permission && (filteredLinks[item] = QUICK_LINKS[item]);
  });

  return filteredLinks;
};

/**
 * @description 将bit转换为Gb
 * @param bit 单位为bit数值
 * @returns 返回Gb位单位数值
 */
export const bitToGbit = (bit: string | number) => {
  return Math.round(Number(bit) / 1024 / 1024 / 1024);
};
