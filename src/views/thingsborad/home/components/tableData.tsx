import { BasicColumn } from '/@/components/Table/src/types/table';
import { Button } from '/@/components/Button';

export const refundTableData: any[] = [
  {
    t1: 1234561,
    t2: '矿泉水 550ml',
    t3: '12421432143214321',
    t4: '2.00',
    t5: 1,
    t6: 1,
  },
  {
    t1: 1234562,
    t2: '矿泉水 550ml',
    t3: '12421432143214321',
    t4: '2.00',
    t5: 2,
    t6: 2,
  },
  {
    t1: 1234562,
    t2: '矿泉水 550ml',
    t3: '12421432143214321',
    t4: '2.00',
    t5: 2,
    t6: 3,
  },
  {
    t1: 1234562,
    t2: '矿泉水 550ml',
    t3: '12421432143214321',
    t4: '2.00',
    t5: 2,
    t6: 4,
  },
];

export const refundTableSchema: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'title',
    sorter: true,
  },
  {
    title: '最近查看',
    sorter: true,
    dataIndex: 'lastVisited',
  },
];
