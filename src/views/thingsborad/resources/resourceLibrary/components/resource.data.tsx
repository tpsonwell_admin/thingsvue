import { BasicColumn, FormSchema } from '/@/components/Table';
import { Icon } from '/@/components/Icon';
import { DescItem } from '/@/components/Description';
import dayjs from 'dayjs';
//下拉多选框 选项接口SelectModal和一个selectList数组来存储所有的选项。
export interface SelectModal {
  value: string;
  label: string;
}

// 表格列配置
export const resourceTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 150,
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '标题',
    width: 150,
    dataIndex: 'title',
    sorter: true,
  },
  {
    title: '资源类型',
    width: 150,
    dataIndex: 'resourceType',
  },
  {
    title: '系统',
    width: 100,
    dataIndex: 'etag',
    customRender: ({ record }) => {
      return <Icon icon="material-symbols:check-box-outline" />;
      // return record.etag !== null ? (
      //   <Icon icon="ci:checkbox-unchecked" />
      // ) : (
      //   <Icon icon="material-symbols:check-box-outline" />
      // );
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入搜索关键字',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const resourceSelectList: SelectModal[] = [
  { label: 'All', value: 'All' },
  { label: 'LWM2M model', value: 'LWM2M_MODEL' },
  { label: 'PKCS #12', value: 'PKCS_12' },
  { label: 'JKS', value: 'JKS' },
  { label: 'Js module', value: 'JS_MODULE' },
];

export const addResourceSelectList: SelectModal[] = [
  { label: 'LWM2M model', value: 'LWM2M_MODEL' },
  { label: 'PKCS #12', value: 'PKCS_12' },
  { label: 'JKS', value: 'JKS' },
  { label: 'Js module', value: 'JS_MODULE' },
];

// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    label: '创建时间',
    field: 'createdTime',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    label: '标题',
    field: 'title',
  },
  {
    label: '资源类型',
    field: 'resourceType',
  },
];
