import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Tag } from 'ant-design-vue';
import dayjs from 'dayjs';

// 表格列配置
export const widgetsTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    customRender: ({ record }) => {
      return <span>{dayjs(record.createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
    sorter: true,
  },
  {
    title: '部件标题',
    width: 150,
    dataIndex: 'deviceName',
  },
  {
    title: '部件类型',
    width: 100,
    dataIndex: 'deviceConfiguration',
  },
  {
    title: 'System',
    width: 100,
    dataIndex: 'system',
  },
  {
    title: 'Deprecated',
    width: 100,
    dataIndex: 'deprecated',
    customRender: ({ record }) => {
      const state = record.state ? '活动' : '非活动';
      return <Tag color="red">{state}</Tag>;
    },
  },
];
// 表格列配置
export const componentTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    width: 150,
    dataIndex: 'createdTime',
    customRender: ({ record }) => {
      return <span>{dayjs(record.createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
    sorter: true,
  },
  {
    title: '标题',
    width: 150,
    dataIndex: 'deviceName',
  },
  {
    title: '系统',
    width: 100,
    dataIndex: 'system',
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    component: 'Input',
    label: '',
  },
];
