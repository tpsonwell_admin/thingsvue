import Chart from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/chart/index.vue';
import Card from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/card/index.vue';
import ArarmTable from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/table/index.vue';
import { setChartExampleData } from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/chart/index.data';
import { setTableExampleData } from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/table/index.data';

export const tabs = {
  'charts.basic_timeseries': Chart,
  'cards.attributes_card': Card,
  'gpio_widgets.basic_gpio_control': '',
  'alarm_widgets.alarms_table': ArarmTable,
  'cards.html_card': '',
};

export const getExampleData = {
  'charts.basic_timeseries': setChartExampleData,
  'alarm_widgets.alarms_table': setTableExampleData,
};

export const switchType = (type: string): string => {
  let fqn = '';
  switch (type) {
    case 'timeseries':
      fqn = 'charts.basic_timeseries';
      break;
    case 'latest':
      fqn = 'cards.attributes_card';
      break;
    case 'rpc':
      fqn = 'gpio_widgets.basic_gpio_control';
      break;
    case 'alarm':
      fqn = 'alarm_widgets.alarms_table';
      break;
    case 'static':
      fqn = 'cards.html_card';
      break;
  }
  return fqn;
};
