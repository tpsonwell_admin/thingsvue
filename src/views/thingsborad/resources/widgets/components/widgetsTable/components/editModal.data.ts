import { FormSchema } from '/@/components/Form/index';
export const editModalSchema: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '部件标题',
    componentProps: {
      placeholder: '请输入部件标题',
    },
    required: true,
  },
  {
    field: 'tags',
    component: 'Select',
    label: '部件标签',
    componentProps: {
      placeholder: '请输入部件标签',
      mode: 'tags',
      maxTagCount: 'responsive',
      open: false,
    },
  },
  {
    field: 'deprecated',
    label: '是否废弃',
    component: 'Switch',
  },
  {
    field: 'image',
    component: 'Input',
    label: '图片',
    slot: 'upload',
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '部件描述',
    componentProps: {
      placeholder: '请输入部件描述',
    },
    colProps: {
      span: 24,
    },
  },
];
