import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Icon } from '/@/components/Icon';
import { DescItem } from '/@/components/Description/index';
import { Image, Tag } from 'ant-design-vue';
import dayjs from 'dayjs';

// 表格列配置
export const widgetsTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '部件标题',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '部件类型',
    dataIndex: 'widgetType',
    customRender: ({ value }) => {
      let text = '';
      switch (value) {
        case 'timeseries':
          text = 'Timeseries';
          break;
        case 'latest':
          text = '最新值';
          break;
        case 'rpc':
          text = '控件部件';
          break;
        case 'alarm':
          text = '告警部件';
          break;
        case 'static':
          text = '静态部件';
          break;
      }
      return text;
    },
  },
  {
    title: '系统',
    dataIndex: 'system',
    customRender: ({ value }) => {
      const icon = value ? 'ant-design:check-square-outlined' : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
  },
  {
    title: '已废弃',
    dataIndex: 'deprecated',
    customRender: ({ value }) => {
      const icon = value ? 'ant-design:check-square-outlined' : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
    sorter: true,
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    component: 'Input',
    label: '',
    componentProps: {
      placeholder: '请输入关键字',
    },
  },
];

// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'title',
    label: '标题',
  },
  {
    field: 'image',
    label: '图片预览',
    render: (val: any) => {
      return <Image src={val} width={100} height={100}></Image>;
    },
  },
  {
    field: 'description',
    label: '描述',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'tags',
    label: '标签',
    render: (val: any) => {
      if (!Array.isArray(val) || val.length === 0) {
        return '— —';
      }
      return (
        <div>
          {val.map((item: string, index: number) => (
            <Tag key={index} color="blue">
              {item}
            </Tag>
          ))}
        </div>
      );
    },
  },
];
