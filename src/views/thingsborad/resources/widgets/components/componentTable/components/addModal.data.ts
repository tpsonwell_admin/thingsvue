import { FormSchema } from '/@/components/Form';

export const schemas: FormSchema[] = [
  {
    field: 'title',
    component: 'Input',
    label: '标题',
    required: true,
  },
  {
    field: 'order',
    component: 'InputNumber',
    componentProps: {
      precision: 0,
    },
    label: 'Order',
  },
  {
    field: 'image',
    component: 'Input',
    label: '图片',
    slot: 'upload',
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '描述',
    colProps: {
      span: 24,
    },
  },
];
