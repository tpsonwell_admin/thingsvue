import { FormSchema } from '/@/components/Form';
import { ref } from 'vue';
import { getWidgets } from '/@/api/thingsborad/resources/widgets/widgets';
import { useDebounceFn } from '@vueuse/shared';

export const schemas: FormSchema[] = [
  {
    field: 'widgetIds',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
        tenantOnly: true,
        fullSearch: false,
        deprecatedFilter: 'ALL',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        mode: 'multiple',
        maxTagCount: 'responsive',
        placeholder: '请选择设备配置',
        api: getWidgets,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id.id',
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
    label: '添加部件',
    colProps: {
      span: 24,
    },
  },
];
