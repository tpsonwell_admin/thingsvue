import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Icon } from '/@/components/Icon';
import { DescItem } from '/@/components/Description/index';
import dayjs from 'dayjs';
import { Image } from 'ant-design-vue';
// 表格列配置
export const componentTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '标题',
    dataIndex: 'title',
    sorter: true,
  },
  {
    title: '系统',
    dataIndex: 'system',
    customRender: ({ value }) => {
      const icon = value ? 'ant-design:check-square-outlined' : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    component: 'Input',
    label: '',
    componentProps: {
      placeholder: '请输入关键字',
    },
  },
];

export const uploadSchema: FormSchema[] = [
  {
    field: 'isIncluding',
    component: 'CheckboxGroup',
    label: '',
    componentProps: {
      options: [
        {
          label:
            'Include bundle widgets in exported data (otherwise only referenced widget FQNs will be exported)',
          value: 'isIncluding',
        },
      ],
    },
    defaultValue: 'isIncluding',
  },
];

// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'title',
    label: '标题',
  },
  {
    field: 'image',
    label: '图片预览',
    render: (val: any) => {
      return <Image src={val} width={100} height={100}></Image>;
    },
  },
  {
    field: 'description',
    label: '描述',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'order',
    label: 'Order',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
];
