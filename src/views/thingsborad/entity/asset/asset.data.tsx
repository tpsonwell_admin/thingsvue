import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { FormSchema } from '/@/components/Form';
import { DescItem } from '/@/components/Description/index';
import { getAssetProfileInfos } from '/@/api/thingsborad/entity/entity';
import dayjs from 'dayjs';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';
import { RoleEnum } from '/@/enums/roleEnum';
import { usePermission } from '/@/hooks/web/usePermission';
const { hasPermission } = usePermission();
export const assetTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '资产配置	',
    dataIndex: 'assetProfileName',
    sorter: true,
  },
  {
    title: '标签',
    dataIndex: 'label',
    sorter: true,
  },
  {
    title: '客户',
    dataIndex: 'customerTitle',
    customRender: ({ record }) => {
      return record.customerIsPublic ? '公开 ' : record.customerTitle;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
    sorter: true,
  },
  {
    title: '公开',
    dataIndex: 'customerIsPublic',
    customRender: ({ record }) => {
      const icon = record.customerIsPublic
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    label: '',
    componentProps: () => {
      return {
        placeholder: '查找关键字',
      };
    },
    component: 'Input',
  },
  {
    field: 'assetProfileId',
    label: '',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        placeholder: '请选择设备配置',
        api: getAssetProfileInfos,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id.id',
        immediate: false,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
  },
];

export const detailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
    render: (val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'name',
    label: '资产名称',
  },
  {
    field: 'assetConfiguration',
    label: '资产配置',
  },
  {
    field: 'label',
    label: '标签',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'customer',
    label: '客户',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'isOpen',
    label: '公开',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
];
