import { FormSchema } from '/@/components/Form/index';
import { getAssetProfileInfos, getCustomerList } from '/@/api/thingsborad/entity/entity';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const schemas: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '资产名称',
    required: true,
  },
  {
    field: 'label',
    component: 'Input',
    label: '标签',
  },
  {
    field: 'assetConfiguration',
    component: 'ApiSelect',
    label: '资产配置',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        placeholder: '请选择资产配置',
        api: getAssetProfileInfos,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id',
        objectToJson: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
    required: true,
  },
  {
    field: 'customer',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        placeholder: '请选择设备配置',
        api: getCustomerList,
        params: params.value,
        resultField: 'data',
        labelField: 'title',
        valueField: 'id.id',
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
    label: '分配给客户',
  },
  {
    field: 'remark',
    component: 'InputTextArea',
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];
