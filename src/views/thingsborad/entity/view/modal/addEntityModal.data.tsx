import { FormSchema } from '/@/components/Form/index';
import {
  getEntityAssetList,
  getEntityDeviceList,
  getAttributeKeys,
  getTimeseries,
} from '/@/api/thingsborad/entity/entity';
import { ref, nextTick } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const schemas: FormSchema[] = [
  {
    field: 'entityName',
    component: 'Input',
    componentProps: {
      placeholder: '请输入名称',
    },
    label: '名称',
    required: true,
  },
  {
    field: 'entityViewType',
    component: 'Input',
    label: '实体视图类型',
    slot: 'entityViewType',
    required: true,
  },
  {
    field: 'type',
    component: 'Select',
    label: '目标实体',
    componentProps: () => {
      return {
        allowClear: false,
        placeholder: '请输入类型',
        options: [
          {
            label: '设备',
            value: 'DEVICE',
          },
          {
            label: '资产',
            value: 'ASSET',
          },
        ],
      };
    },
    required: true,
    defaultValue: 'DEVICE',
  },
  {
    field: 'device',
    component: 'ApiSelect',
    label: '设备',
    ifShow: (value) => {
      return value?.model?.type == 'DEVICE';
    },
    required: true,
  },
  {
    field: 'asset',
    component: 'ApiSelect',
    label: '资产',
    ifShow: (value) => {
      return value?.model?.type == 'ASSET';
    },
    required: true,
  },
  {
    field: 'divider-attribute-propagation',
    component: 'Divider',
    label: '属性传播',
    componentProps: {
      orientation: 'center',
      style: {
        fontSize: '18px',
        fontWeight: 600,
      },
    },
    colProps: {
      span: 24,
    },
  },
  {
    field: 'clientProperties',
    label: '客户端属性',
    component: 'ApiSelect',
    dynamicDisabled: ({ model }) => {
      return !model.device && !model.asset;
    },
  },
  {
    field: 'sharedProperty',
    label: '共享属性',
    component: 'ApiSelect',
    dynamicDisabled: ({ model }) => {
      return !model.device && !model.asset;
    },
  },
  {
    field: 'serverProperties',
    label: '服务端属性',
    component: 'ApiSelect',
    dynamicDisabled: ({ model }) => {
      return !model.device && !model.asset;
    },
  },
  {
    field: 'divider-time-series-data',
    component: 'Divider',
    label: '时间序列数据',
    componentProps: {
      orientation: 'center',
      style: {
        fontSize: '18px',
        fontWeight: 600,
      },
    },
    colProps: {
      span: 24,
    },
  },
  {
    field: 'timeSeries',
    label: '时间序列',
    component: 'ApiSelect',
    dynamicDisabled: ({ model }) => {
      return !model.device && !model.asset;
    },
  },
  {
    field: 'dateRange',
    component: 'RangePicker',
    label: '日期范围',
    componentProps: {
      showTime: true,
    },
  },
  {
    field: 'remark',
    component: 'InputTextArea',
    componentProps: {
      placeholder: '请输入说明',
    },
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];
