import { FormSchema } from '/@/components/Table';
import { getCustomerList } from '/@/api/thingsborad/entity/entity';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const customerSchemas: FormSchema[] = [
  {
    field: 'customer',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        placeholder: '请选择设备配置',
        api: getCustomerList,
        params: params.value,
        resultField: 'data',
        labelField: 'title',
        valueField: 'id.id',
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
    label: '请选择客户分配设备',
    colProps: {
      span: 24,
    },
    required: true,
  },
];
