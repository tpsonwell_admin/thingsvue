import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { FormSchema } from '/@/components/Form';
import { getEntityViewTypeList } from '/@/api/thingsborad/entity/entity';
import { DescItem } from '/@/components/Description/index';
import dayjs from 'dayjs';
import { RoleEnum } from '/@/enums/roleEnum';
import { usePermission } from '/@/hooks/web/usePermission';
const { hasPermission } = usePermission();
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';
export const entityTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '实体视图类型	',
    dataIndex: 'type',
    sorter: true,
  },
  {
    title: '客户',
    dataIndex: 'customerTitle',
    customRender: ({ record }) => {
      return record.isOpen ? '公开 ' : record.customer;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
    sorter: true,
  },
  {
    title: '公开',
    dataIndex: 'isOpen',
    customRender: ({ record }) => {
      const icon = record.isOpen
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'type',
    label: '',
    component: 'ApiSelect',
    componentProps: {
      placeholder: '请选择实体视图类型',
      listHeight: 160,
      api: getEntityViewTypeList,
      params: {
        page: 0,
        pageSize: 50,
      },
      labelField: 'type',
      valueField: 'type',
      immediate: false,
    },
  },
  {
    field: 'textSearch',
    label: '',
    componentProps: {
      placeholder: '请输入关键字',
    },
    component: 'Input',
  },
];

export const detailSchema: DescItem[] = [
  {
    field: 'createTime',
    label: '创建时间',
    render: (_val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'entityViewName',
    label: '实体视图名称',
  },
  {
    field: 'entityViewType',
    label: '实体视图类型',
  },
  {
    field: 'clientProperties',
    label: '客户端属性',
    render: (val: any) => {
      return <span>{val ? val.join(',') : '— —'}</span>;
    },
  },
  {
    field: 'sharedProperty',
    label: '共享属性',
    render: (val: any) => {
      return <span>{val ? val.join(',') : '— —'}</span>;
    },
  },
  {
    field: 'serverProperties',
    label: '服务端属性',
    render: (val: any) => {
      return <span>{val ? val.join(',') : '— —'}</span>;
    },
  },
  {
    field: 'timeseries',
    label: '时间序列',
    render: (val: any) => {
      return <span>{val ? val.join(',') : '— —'}</span>;
    },
  },
  {
    field: 'customer',
    label: '客户',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'isOpen',
    label: '公开',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
  {
    field: 'description',
    label: '描述',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
];
