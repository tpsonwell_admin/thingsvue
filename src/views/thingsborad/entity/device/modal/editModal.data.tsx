import { FormSchema } from '/@/components/Table';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';
import { getDeviceProfileInfos } from '/@/api/thingsborad/entity/entity';

export const deviceSchemas: FormSchema[] = [
  {
    field: 'deviceProfileId',
    component: 'Input',
    label: '',
    show: false,
  },
  {
    field: 'name',
    component: 'Input',
    componentProps: {
      placeholder: '请输入设备名称',
    },
    label: '名称',
    required: true,
  },
  {
    field: 'deviceProfileName',
    component: 'ApiSelect',
    label: '设备配置',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        placeholder: '请选择设备名称',
        showArrow: false,
        allowClear: false,
        showSearch: true,
        listHeight: 160,
        api: getDeviceProfileInfos,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id',
        objectToJson: true,
        immediate: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
        onChange: (e) => {
          console.log(e);
        },
      };
    },
    required: true,
  },
  {
    field: 'label',
    component: 'Input',
    componentProps: {
      placeholder: '请输入设备标签',
    },
    label: '标签',
  },
  {
    field: 'firmware',
    component: 'Select',
    label: '分配的固件',
  },
  {
    field: 'software',
    component: 'ApiSelect',
    label: '分配的软件',
  },
  {
    field: 'isGateway',
    component: 'Switch',
    label: '是否网关',
  },
  {
    field: 'isCover',
    component: 'Switch',
    label: '覆盖已连接设备的活动时间',
    ifShow: (value) => {
      return value?.model?.isGateway;
    },
  },
  {
    field: 'remark',
    component: 'InputTextArea',
    componentProps: {
      placeholder: '请输入说明',
    },
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];
