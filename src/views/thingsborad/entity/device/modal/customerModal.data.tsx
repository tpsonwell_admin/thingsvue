import { FormSchema } from '/@/components/Table';
import { getCustomerList } from '/@/api/thingsborad/entity/entity';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const customerSchemas: FormSchema[] = [
  {
    field: 'customer',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 50,
        textSearch: '',
      });
      return {
        listHeight: 160,
        api: getCustomerList,
        params: params.value,
        resultField: 'data',
        labelField: 'title',
        valueField: 'id.id',
        showSearch: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
        immediate: true,
      };
    },
    label: '请选择客户分配设备',
    colProps: {
      span: 24,
    },
    required: true,
  },
];
