import { UseFormReturnType, useForm, FormProps } from '/@/components/Form';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import { FormSchema } from '/@/components/Table';
import { Button } from '/@/components/Button';
import { useMessage } from '/@/hooks/web/useMessage';
import { Icon } from '/@/components/Icon';
import { modalFormStyleSetting } from '/@/settings/thingsBoardDesign';
import { usePermission } from '/@/hooks/web/usePermission';
import { RoleEnum } from '/@/enums/roleEnum';
const { hasPermission } = usePermission();

// 生成随机字符串
function generate(length?: number): string {
  if (Boolean(typeof length == 'undefined') || length == null) {
    length = 1;
  }
  const l = length > 10 ? 10 : length;
  const str = Math.random().toString(36).substr(2, l);
  if (str.length >= length) {
    return str;
  }
  return str.concat(generate(length - str.length));
}

const TOKENSchemas: FormSchema[] = [
  {
    field: 'token',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入访问令牌',
      };
    },
    label: '访问令牌',
    renderComponentContent: ({ values }) => {
      const { setFieldsValue } = tabsFormList[0].Form[1];
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      return {
        suffix: () => {
          return (
            <Button
              size="small"
              type="text"
              onClick={() => {
                if (!values.token || values.token == '') {
                  setFieldsValue({
                    token: generate(20),
                  });
                } else {
                  clipboardRef.value = values.token;
                  createMessage.success('复制成功');
                }
              }}
            >
              <Icon
                icon={values.token ? 'ant-design:copy-outlined' : 'ant-design:sync-outlined'}
              ></Icon>
            </Button>
          );
        },
      };
    },
    colProps: {
      span: 24,
    },
    required: true,
  },
];
const X509Schemas: FormSchema[] = [
  {
    field: 'certificate',
    component: 'InputTextArea',
    componentProps: {
      placeholder: '请输入PEM 格式的证书',
    },
    label: 'PEM 格式的证书',
    colProps: {
      span: 24,
    },
    required: true,
  },
];
const MQTTschemas: FormSchema[] = [
  {
    field: 'id',
    component: 'Input',
    componentProps: {
      placeholder: '请输入客户端ID',
    },
    label: '客户端ID',
    renderComponentContent: ({ values }) => {
      const { setFieldsValue } = tabsFormList[2].Form[1];
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      return {
        suffix: () => {
          return (
            <Button
              size="small"
              type="text"
              onClick={() => {
                if (!values.id || values.id == '') {
                  setFieldsValue({
                    id: generate(20),
                  });
                } else {
                  clipboardRef.value = values.id;
                  createMessage.success('复制成功');
                }
              }}
            >
              <Icon
                icon={values.id ? 'ant-design:copy-outlined' : 'ant-design:sync-outlined'}
              ></Icon>
            </Button>
          );
        },
      };
    },
    required: true,
  },
  {
    field: 'username',
    component: 'Input',
    componentProps: {
      placeholder: '请输入用户名',
    },
    label: '用户名',
    renderComponentContent: ({ values }) => {
      const { setFieldsValue } = tabsFormList[2].Form[1];
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      return {
        suffix: () => {
          return (
            <Button
              size="small"
              type="text"
              onClick={() => {
                if (!values.username || values.username == '') {
                  setFieldsValue({
                    username: generate(20),
                  });
                } else {
                  clipboardRef.value = values.username;
                  createMessage.success('复制成功');
                }
              }}
            >
              <Icon
                icon={values.username ? 'ant-design:copy-outlined' : 'ant-design:sync-outlined'}
              ></Icon>
            </Button>
          );
        },
      };
    },
    required: true,
  },
  {
    field: 'password',
    component: 'InputPassword',
    componentProps: {
      placeholder: '请输入密码',
    },
    label: '密码',
    // renderComponentContent: ({ values }) => {
    //   const { setFieldsValue } = tabsFormList[2].Form[1];
    //   const { clipboardRef } = useCopyToClipboard();
    //   const { createMessage } = useMessage();
    //   return {
    //     suffix: () => {
    //       return (
    //         <Button
    //           type="text"
    //           onClick={() => {
    //             if (!values.password || values.password == '') {
    //               setFieldsValue({
    //                 id: Date.now(),
    //               });
    //             } else {
    //               clipboardRef.value = values.password;
    //               createMessage.success('复制成功');
    //             }
    //           }}
    //         >
    //           <Icon
    //             icon={values.password ? 'ant-design:copy-outlined' : 'ant-design:sync-outlined'}
    //           ></Icon>
    //         </Button>
    //       );
    //     },
    //   };
    // },
    required: true,
  },
];
const baseFormConfig: Partial<FormProps> = {
  layout: 'vertical',
  showActionButtonGroup: false,
  ...modalFormStyleSetting,
  disabled: hasPermission(RoleEnum.CUSTOMER_USER),
};
type TabsFormType = {
  key: string;
  tab: string;
  forceRender?: boolean;
  Form: UseFormReturnType;
};
export const tabsFormList: TabsFormType[] = [
  {
    key: 'ACCESS_TOKEN',
    tab: 'Access Token',
    forceRender: true,
    Form: useForm(Object.assign({ schemas: TOKENSchemas }, baseFormConfig) as FormProps),
  },
  {
    key: 'X509_CERTIFICATE',
    tab: 'X.509',
    forceRender: true,
    Form: useForm(Object.assign({ schemas: X509Schemas }, baseFormConfig) as FormProps),
  },
  {
    key: 'MQTT_BASIC',
    tab: 'MQTT Basic',
    forceRender: true,
    Form: useForm(Object.assign({ schemas: MQTTschemas }, baseFormConfig) as FormProps),
  },
];
