import { FormSchema } from '/@/components/Form';
import { BasicColumn } from '/@/components/Table/src/types/table';

type StepsType = {
  title: string;
};
// 步骤表单数组
export const stepList: StepsType[] = [
  {
    title: '选择一个文件',
  },
  {
    title: '导入配置',
  },
  {
    title: '选择列类型',
  },
  {
    title: '创建新实体',
  },
  {
    title: '完成',
  },
];

export const registerStepFormSchema: FormSchema[] = [
  {
    field: 'currentStep',
    component: 'InputNumber',
    label: '',
    slot: 'step',
    defaultValue: 0,
    colProps: {
      span: 24,
    },
  },
  {
    field: 'upload',
    component: 'Upload',
    label: '设备文件',
    slot: 'upload',
    show: (opt) => {
      return opt.model.currentStep == 0;
    },
  },
  {
    field: 'separator',
    component: 'Select',
    label: 'CSV分隔符',
    componentProps: {
      placeholder: 'CSV分隔符*',
      options: [
        { label: ',', value: ',' },
        { label: ';', value: ';' },
        { label: '|', value: '|' },
        { label: 'Tab', value: 'Tab' },
      ],
    },
    show: (opt) => {
      return opt.model.currentStep == 1;
    },
    defaultValue: ',',
  },
  {
    field: 'containsColumn',
    component: 'Checkbox',
    label: '第一行包含列',
    defaultValue: true,
    show: (opt) => {
      return opt.model.currentStep == 1;
    },
  },
  {
    field: 'updateProperties',
    component: 'Checkbox',
    label: '更新属性/遥测',
    componentProps: {
      placeholder: '数值',
    },
    defaultValue: true,
    show: (opt) => {
      return opt.model.currentStep == 1;
    },
  },
  {
    field: 'table',
    component: 'Input',
    label: '',
    slot: 'table',
    colProps: {
      span: 24,
    },
    show: (opt) => {
      return opt.model.currentStep == 2;
    },
  },
  {
    field: 'loading',
    component: 'Input',
    label: '',
    slot: 'loading',
    colProps: {
      span: 24,
    },
    show: (opt) => {
      return opt.model.currentStep == 3;
    },
  },
  {
    field: 'result',
    component: 'Input',
    label: '',
    slot: 'result',
    colProps: {
      span: 24,
    },
    show: (opt) => {
      return opt.model.currentStep == 4;
    },
  },
];

export const registerTableSchema: BasicColumn[] = [
  {
    title: '示例值数据',
    dataIndex: 'exampleValue',
    width: 100,
  },
  {
    title: '列类型',
    dataIndex: 'columnType',
    edit: true,
    editComponent: 'Select',
    editComponentProps: {
      options: [
        { label: '名称', value: 'NAME' },
        { label: '类型', value: 'TYPE' },
        { label: '标签', value: 'LABEL' },
        { label: '说明', value: 'DESCRIPTION' },
        { label: '共享属性', value: 'SHARED_ATTRIBUTE' },
        { label: '服务器属性', value: 'SERVER_ATTRIBUTE' },
        { label: 'Timeseries', value: 'TIMESERIES' },
        { label: '是否网关', value: 'IS_GATEWAY' },
        { label: '————凭据————', disabled: true },
        { label: '访问令牌', value: 'ACCESS_TOKEN' },
        { label: 'X.509', value: 'X509' },
        { label: 'MQTT客户端ID', value: 'MQTT_CLIENT_ID' },
        { label: 'MQTT用户名', value: 'MQTT_USER_NAME' },
        { label: 'MQTT密码', value: 'MQTT_PASSWORD' },
        { label: '客户端终节点名称', value: 'LWM2M_CLIENT_ENDPOINT' },
        { label: '安全配置模式', value: 'LWM2M_CLIENT_SECURITY_CONFIG_MODE' },
        { label: '客户端标识', value: 'LWM2M_CLIENT_IDENTITY' },
        { label: '客户端公钥', value: 'LWM2M_CLIENT_KEY' },
        { label: '客户端证书', value: 'LWM2M_CLIENT_CERT' },
      ],
    },
    width: 250,
  },
  {
    title: '属性/遥测键',
    dataIndex: 'attribute',
    edit: true,
    editComponent: 'Input',
    width: 250,
  },
];
