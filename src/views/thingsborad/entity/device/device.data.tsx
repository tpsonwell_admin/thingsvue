import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { FormSchema } from '/@/components/Form';
import { Tag } from 'ant-design-vue';
import { DescItem } from '/@/components/Description/index';
import { getDeviceProfileInfos } from '/@/api/thingsborad/entity/entity';
import dayjs from 'dayjs';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';
import { RoleEnum } from '/@/enums/roleEnum';
import { usePermission } from '/@/hooks/web/usePermission';
const { hasPermission } = usePermission();
// 表格列配置
export const deviceTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '设备配置	',
    dataIndex: 'deviceProfileName',
    sorter: true,
  },
  {
    title: '标签',
    dataIndex: 'label',
    sorter: true,
  },
  {
    title: '状态',
    dataIndex: 'active',
    customRender: ({ record }) => {
      const state = record.active ? '活动' : '非活动';
      const color = record.active ? 'green' : 'red';
      return <Tag color={color}>{state}</Tag>;
    },
    sorter: true,
  },
  {
    title: '客户',
    dataIndex: 'customerTitle',
    customRender: ({ record }) => {
      return record.customerIsPublic ? '公开 ' : record.customerTitle;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
    sorter: true,
  },
  {
    title: '公开',
    dataIndex: 'customerIsPublic',
    customRender: ({ record }) => {
      const icon = record.customerIsPublic
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
    ifShow: () => hasPermission(RoleEnum.TENANT_ADMIN),
    sorter: true,
  },
  {
    title: '是否网关',
    dataIndex: 'isGateway',
    customRender: ({ record }) => {
      const icon = record.isGateway
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return <Icon icon={icon} size={20}></Icon>;
    },
  },
];
// 表格搜索表单配置
export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    label: '',
    componentProps: {
      placeholder: '请输入关键字',
    },
    component: 'Input',
  },
  {
    field: 'deviceProfileId',
    label: '',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        allowClear: false,
        listHeight: 160,
        showSearch: true,
        placeholder: '请选择设备配置',
        api: getDeviceProfileInfos,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id.id',
        immediate: false,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
  },
  {
    field: 'state',
    label: '',
    componentProps: {
      allowClear: false,
      placeholder: '请选择设备状态',
      options: [
        { label: '活动', value: true },
        { label: '非活动', value: false },
      ],
    },
    component: 'Select',
  },
];
// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
    render: (val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'name',
    label: '名称',
  },
  {
    field: 'deviceConfiguration',
    label: '设备配置',
  },
  {
    field: 'label',
    label: '标签',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'state',
    label: '状态',
    render: (val: any) => {
      return <span>{val ? '活动' : '非活动'}</span>;
    },
  },
  {
    field: 'customer',
    label: '客户',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'isOpen',
    label: '公开',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
  {
    field: 'isGateway',
    label: '是否网关',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
];
