import { BasicColumn, FormSchema } from '/@/components/Table';
import { useMessage } from '/@/hooks/web/useMessage';
import ScrollContainer from '/@/components/Container/src/ScrollContainer.vue';
export interface navBar {
  icon: string;
  name: string;
}

// 导航栏数据列表
export const navBarList: navBar[] = [
  {
    icon: 'material-symbols:inbox-rounded',
    name: '收件箱',
  },
  {
    icon: 'mdi:account-multiple',
    name: '已发送',
  },
  {
    icon: 'mdi:account-multiple',
    name: '收件人',
  },
  {
    icon: 'mdi:sort-variant',
    name: '模板',
  },
  {
    icon: 'gg:loadbar-doc',
    name: '规则',
  },
];
//收件箱表头
export const inboxColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 100,
    sorter: true,
  },
  {
    title: '类型',
    dataIndex: 'type',
    width: 100,
  },
  {
    title: '主题',
    dataIndex: 'subject',
    width: 100,
  },
  {
    title: '信息',
    dataIndex: 'text',
    width: 100,
  },
];
//已发送表头
export const sentColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    sorter: true,
  },
  {
    title: '状态',
    dataIndex: 'status',
    customRender: ({ record }) => {
      // 消息通知框
      const { createInfoModal } = useMessage();
      let colors = '';
      let colorsBg = '';
      let text = '';
      if (record.status === 'SENT') {
        text = '已发送';
        colors = '#198038';
        colorsBg = '#EDF5EF';
      } else if (record.status === 'PROCESSING') {
        text = '处理中';
        colors = '#D47D18';
        colorsBg = '#EDF5EF';
      } else if (record.status === 'SCHEDULED') {
        text = '已安排';
        colors = '#305680';
        colorsBg = '#EDF5EF';
      }
      const sendStyle: any = {
        padding: '5px',
        backgroundColor: colorsBg,
        color: colors,
        borderRadius: '6px',
      };
      let sumError = 0;
      Object.keys(record.info.stats.errors).forEach((key) => {
        sumError += Object.keys(record.info.stats.errors[key]).length;
      });
      const errorStyle = {
        padding: '5px',
        backgroundColor: '#D12730',
        color: '#FFF',
        borderRadius: '5px',
        marginLeft: '5px',
        display: sumError === 0 ? 'none' : ' ',
        cursor: 'pointer',
      };
      const info = () => {
        createInfoModal({
          iconType: 'info',
          title: '发送失败',
          bodyStyle: {
            width: '580px',
          },
          maskClosable: true,
          style: {
            width: '580px',
            height: '800px',
          },
          content: () => (
            <ScrollContainer style={{ height: '600px' }}>
              {Object.entries(record.info.stats.errors).map(
                ([key, errorsObj]: [string, object]) => {
                  return (
                    <div>
                      <div style={{ width: '100%', padding: '16px' }}>{key}</div>
                      {Object.entries(errorsObj).map(([user, errorInfo]) => {
                        return (
                          <div
                            style={{
                              display: 'flex',
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}
                          >
                            <div
                              style={{
                                backgroundColor: '#E0E0E0',
                                borderRadius: '10px',
                                padding: '5px',
                              }}
                            >
                              {user}
                            </div>
                            <div style={{ width: '50%' }}>{errorInfo}</div>
                          </div>
                        );
                      })}
                    </div>
                  );
                },
              )}
            </ScrollContainer>
          ),
        });
      };
      return (
        <div>
          <span style={sendStyle}>{text}</span>
          <span onClick={info} style={errorStyle}>
            {sumError} 失败
          </span>
        </div>
      );
    },
  },
  {
    title: '推送方式',
    dataIndex: 'deliveryMethods',
  },
  {
    title: '模板',
    dataIndex: 'template',
  },
];
//收件人表头
export const recipientColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 100,
    sorter: true,
  },
  {
    title: '收件人组',
    dataIndex: 'name',
    width: 180,
  },
  {
    title: '类型',
    dataIndex: 'type',
    width: 180,
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 180,
  },
];
//模板表头
export const modelColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 100,
    sorter: true,
  },
  {
    title: '类型',
    dataIndex: 'notificationType',
    width: 180,
  },
  {
    title: '名称',
    dataIndex: 'name',
    width: 180,
  },
];
//规则表头
export const ruleColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 100,
    sorter: true,
  },
  {
    title: '规则名称',
    dataIndex: 'name',
    width: 180,
  },
  {
    title: '模板',
    dataIndex: 'templateName',
    width: 180,
  },
  {
    title: '触发器',
    dataIndex: 'triggerType',
    width: 180,
  },
  {
    title: '描述',
    dataIndex: 'description',
    width: 180,
  },
];
//表头数组
export const columnsList = new Array<BasicColumn[]>();
columnsList.push(inboxColumns);
columnsList.push(sentColumns);
columnsList.push(recipientColumns);
columnsList.push(modelColumns);
columnsList.push(ruleColumns);

//是否跳转到设备页面
export const goToDevices = {
  GENERAL: false,
  ALARM: false,
  DEVICE_ACTIVITY: true,
  ENTITY_ACTION: true,
  ALARM_COMMENT: false,
  RULE_ENGINE_COMPONENT_LIFECYCLE_EVENT: true,
  ALARM_ASSIGNMENT: false,
  NEW_PLATFORM_VERSION: true,
  ENTITIES_LIMIT: false,
};
// 收件箱类型类别
export const inboxType = {
  GENERAL: '通用',
  ALARM: '告警',
  DEVICE_ACTIVITY: '设备活动',
  ENTITY_ACTION: '实体操作',
  ALARM_COMMENT: '告警评论',
  RULE_ENGINE_COMPONENT_LIFECYCLE_EVENT: '规则引擎生命周期事件',
  ALARM_ASSIGNMENT: '告警分配',
  NEW_PLATFORM_VERSION: '新的平台版本',
  ENTITIES_LIMIT: '实体限制',
  API_USAGE_LIMIT: 'API使用限制',
  RULE_NODE: '规则节点',
  RATE_LIMITS: '速率限制',
};
//告警提示
export const tipsList = {
  CRITICAL: '危险',
  MAJOR: '重要',
  MINOR: '次要',
  WARNING: '警告',
  INDETERMINATE: '不确定',
};

//告警提示颜色
export const tipsColors = {
  CRITICAL: 'red',
  MAJOR: 'orange',
  MINOR: 'volcano',
  WARNING: 'error',
  INDETERMINATE: 'warning',
};
//反向查找收件箱类型类别键名
export function findInboxKeyByValue(value: string): string {
  let keys;
  for (const key of Object.keys(inboxType)) {
    if (inboxType[key] === value) {
      keys = key;
      break;
    }
  }
  return keys;
}
//收件人类型
export const recipientType = {
  PLATFORM_USERS: '平台用户',
  Slack: 'Slack',
  MICROSOFT_TEAMS: 'Microsoft Teams',
};
export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '',
    componentProps: () => {
      return {
        placeholder: '搜索通知',
      };
    },
    component: 'Input',
    colProps: {
      span: 18,
    },
  },
];

export const modelsearchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入搜索名称',
      };
    },
    component: 'Input',
    colProps: {
      span: 18,
    },
  },
];
export const inBoxsearchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入搜索通知',
      };
    },
    component: 'Input',
    colProps: {
      span: 18,
    },
  },
];

export const ruleSearchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入搜索规则名称',
      };
    },
    component: 'Input',
    colProps: {
      span: 18,
    },
  },
];

//下拉多选框 选项接口SelectModal和一个selectList数组来存储所有的选项。
export interface SelectModal {
  value: string;
  label: string;
}
//收件人下拉多选
export const selectList: SelectModal[] = [
  { value: '苹果', label: '苹果' },
  { value: '香蕉', label: '香蕉' },
  { value: '樱桃', label: '樱桃' },
];

export const ruleChainEventsSelectList: SelectModal[] = [
  { value: '已开始', label: '已开始' },
  { value: '已更新', label: '已更新' },
  { value: '已停止', label: '已停止' },
];
//警报严重性列表下拉列表
export const ruleChainLibrarySelectList: SelectModal[] = [
  { value: 'Root Rule Chain', label: 'Root Rule Chain' },
  { value: 'Thermostat', label: 'Thermostat' },
];
export const seriousAlarmSelectList: SelectModal[] = [
  { value: 'CRITICAL', label: '危险' },
  { value: 'MAJOR', label: '重要' },
  { value: 'MINOR', label: '次要' },
  { value: 'WARNING', label: '警告' },
  { value: 'INDETERMINATE', label: '不确定' },
];
//通知条件告警下拉列表
export const notificationAlarmSelectList: SelectModal[] = [
  { value: 'ACKNOWLEDGED', label: '告警已确认' },
  { value: 'SEVERITY_CHANGED', label: '告警级别已更改' },
  { value: 'CLEARED', label: '告警已清除' },
  { value: 'CREATED', label: '告警已创建' },
];

export const notificationActivitySelectList: SelectModal[] = [
  { value: '活动', label: '活动' },
  { value: '非活动', label: '非活动' },
];
//添加规则时间下拉列表
export const ruleTimeSelectList: SelectModal[] = [
  { label: '1分', value: '60' },
  { label: '2分', value: '120' },
  { label: '5分', value: '300' },
  { label: '10分', value: '600' },
  { label: '15分', value: '900' },
  { label: '30分', value: '1800' },
  { label: '1小时', value: '3600' },
  { label: '2小时', value: '7200' },
  { label: '5小时', value: '18000' },
  { label: '10小时', value: '36000' },
  { label: '12小时', value: '43200' },
  { label: '1天', value: '86400' },
  { label: '7天', value: '604800' },
];
//添加规则 告警下拉列表
export const ruleAlarmSelectList: SelectModal[] = [
  { value: 'ACTIVE', label: '激活' },
  { value: 'CLEARED', label: '已清除' },
  { value: 'ACK', label: '已确认' },
  { value: 'UNACK', label: '未确认' },
];

export const entityTypeSelectList: SelectModal[] = [
  { value: 'TYPE_TENANT', label: '租户' },
  { value: 'TYPE_CUSTOMER', label: '客户' },
  { value: 'TYPE_USER', label: '用户' },
  { value: 'TYPE_DASHBOARD', label: '仪表板' },
  { value: 'TYPE_ASSET', label: '资产' },
  { value: 'TYPE_DEVICE', label: '设备' },
  { value: 'TYPE_DEVICE_PROFILE', label: '设备配置' },
  { value: 'TYPE_ASSET_PROFILE', label: '资产配置' },
  { value: 'TYPE_RULECHAIN', label: '规则链' },
  { value: 'TYPE_RULENODE', label: '规则节点' },
  { value: 'TYPE_EDGE', label: '边缘' },
  { value: 'TYPE_ENTITY_VIEW', label: '实体视图' },
  { value: 'TYPE_WIDGETS_BUNDLE', label: '部件包' },
  { value: 'TYPE_TB_RESOURCE', label: '资源' },
  { value: 'TYPE_OTA_PACKAGE', label: 'OTA包' },
  { value: 'TYPE_NOTIFICATION_RULE', label: '通知规则' },
  { value: 'TYPE_NOTIFICATION_TARGET', label: '通知收件人' },
  { value: 'TYPE_NOTIFICATION_TEMPLATE', label: '通知模板' },
];
//推荐方式列表
export interface pushMethodModal {
  tooltip?: string;
  title: string;
  icon?: boolean;
  switchFlag: boolean;
  isShow?: boolean;
}

export const entityOperationSwitchList: pushMethodModal[] = [
  {
    switchFlag: false,
    title: '已创建',
  },
  {
    switchFlag: false,
    title: '已更新',
  },
  {
    switchFlag: false,
    title: '已删除',
  },
];

export const noticeSwitchList: pushMethodModal[] = [
  {
    switchFlag: false,
    title: '仅在用户评论时通知',
  },
  {
    switchFlag: false,
    title: '在评论更新时通知',
  },
];

export const pushMethodList: pushMethodModal[] = [
  {
    title: 'WEB',
    tooltip: '',
    icon: false,
    switchFlag: true,
    isShow: true,
  },
  {
    title: 'EMAIL',
    tooltip: '',
    icon: false,
    switchFlag: false,
    isShow: true,
  },
  {
    title: 'MICROSOFT_TEAMS',
    tooltip: '',
    icon: false,
    switchFlag: false,
    isShow: true,
  },
  {
    title: 'SMS',
    tooltip: '推送方式未配置，请与您的系统管理员联系。',
    icon: false,
    switchFlag: false,
    isShow: true,
  },
  {
    title: 'SLACK',
    tooltip: '推送方式未配置，点击进行设置。',
    icon: true,
    switchFlag: false,
    isShow: true,
  },
];

export interface stepModal {
  title: string;
  description?: string;
  subTitle?: string;
}

//创建收件人类型
export interface createOneTargets {
  name: string;
  configuration: {
    type: string;
    description: string;
    webhookUrl?: string;
    channelName?: string;
    usersFilter?: {
      type: string;
      usersIds?: any;
      customerId?: string;
    };
  };
}
