import { DescItem } from '/@/components/Description';
export const detailSchema: DescItem[] = [
  {
    field: 'name',
    label: '名称',
  },
  {
    field: 'debugMode',
    label: '调试模式',
    render: (value: boolean) => (value ? '开启' : '未开启'),
  },
  {
    field: 'description',
    label: '描述',
    render: (val: string) => {
      return <span>{Boolean(val) ? val : '— —'}</span>;
    },
  },
];
