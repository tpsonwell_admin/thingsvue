import { FormSchema } from '/@/components/Form';
import { isObject, cloneDeep } from 'lodash-es';
import { formObject } from './ruleFormprops';
export const getSchemas = (nodeCfg: any): FormSchema[] => {
  let list: Array<FormSchema> = [];
  console.log(nodeCfg.clazz);
  list = formObject[nodeCfg.clazz]
    ? [
        ...formObject[nodeCfg.clazz],
        {
          field: 'description',
          label: '描述',
          component: 'InputTextArea',
          componentProps: {
            placeholder: '请输入描述',
          },
          colProps: {
            span: 24,
          },
          defaultValue: '',
        },
      ]
    : [
        {
          field: 'description',
          label: '描述',
          component: 'InputTextArea',
          componentProps: {
            placeholder: '请输入描述',
          },
          colProps: {
            span: 24,
          },
          defaultValue: '',
        },
      ];
  return [
    {
      field: 'name',
      label: '名称',
      component: 'Input',
      componentProps: {
        placeholder: '请输入名称',
      },
      required: true,
    },
    {
      field: 'debugMode',
      label: '调试模式',
      component: 'Switch',
      defaultValue: false,
    },
    ...list,
  ];
};

export const getNodeData = (data, descriptor, EntityType, nodeId, ruleChainId, isNewNode) => {
  const obj = {
    ...data,
    singletonMode: false,
    type: descriptor.value?.clazz || '',
    configuration:
      descriptor.value?.configurationDescriptor.nodeDefinition.defaultConfiguration || {},
    additionalInfo: { description: '', layoutX: 0, layoutY: 0 },
    id: { entityType: EntityType.RULE_NODE, id: nodeId.value },
    ruleChainId: ruleChainId.value,
    configurationVersion: descriptor.value?.configurationVersion || 0,
    createdTime: descriptor.value?.createdTime || new Date().getTime(),
    isNewNode: isNewNode.value,
  };
  return obj;
};

export const recursion = (data) => {
  let obj = {};
  const res = cloneDeep(data);
  Object.keys(res).forEach((key) => {
    if (isObject(res[key])) {
      obj = { ...obj, ...res[key] };
      recursion(res[key]);
    } else {
      obj[key] = res[key];
    }
  });

  return obj;
};

export const processingData = (key: string, data: any, descriptor?: any) => {
  const { name } = descriptor;
  if (key == 'display') {
    switch (name) {
      case 'script':
        return {
          name: data.name,
          debugMode: data.debugMode,
          description: data.description,
          transformScript: {
            scriptLang: data.scriptLang,
            jsScript: data.jsScript,
            tbelScript: data.tbelScript,
          },
        };
      default:
        return data;
    }
  } else if (key == 'save') {
    switch (name) {
      case 'script':
        return recursion(data);
      default:
        return data;
    }
  }
};
