export const selectOptions1 = [
  {
    label: '创建时间',
    value: 'createdTime',
  },
  {
    label: '名称',
    value: 'name',
  },
  {
    label: 'Profile name',
    value: 'type',
  },
  {
    label: '名字',
    value: 'firstName',
  },
  {
    label: '姓氏',
    value: 'lastName',
  },
  {
    label: '电子邮件',
    value: 'email',
  },
  {
    label: '标题',
    value: 'title',
  },
  {
    label: '国家',
    value: 'country',
  },
  {
    label: '省/州',
    value: 'state',
  },
  {
    label: '城市',
    value: 'city',
  },
  {
    label: '地址',
    value: 'address',
  },
  {
    label: '地址二',
    value: 'address2',
  },
  {
    label: '邮政编码',
    value: 'zip',
  },
  {
    label: '电话',
    value: 'phone',
  },
  {
    label: '标签',
    value: 'label',
  },
];

export const selectOptions2 = [
  {
    label: 'Contains',
    value: 'Contains',
  },
  {
    label: 'Manages',
    value: 'Manages',
  },
];

export const entityTypeOptions = [
  { label: '设备', value: 'DEVICE' },
  { label: '资产', value: 'ASSET' },
  { label: '实体视图', value: 'ENTITY_VIEW' },
  { label: '租户', value: 'TENANT' },
  { label: '客户', value: 'CUSTOMER' },
  { label: '用户', value: 'USER' },
  { label: '仪表盘', value: 'DASHBOARD' },
  { label: '边缘', value: 'EDGE' },
];

export const options = {
  'org.thingsboard.rule.engine.metadata.TbGetOriginatorFieldsNode': selectOptions1,
  'org.thingsboard.rule.engine.metadata.TbGetRelatedAttributeNode': selectOptions2,
};
