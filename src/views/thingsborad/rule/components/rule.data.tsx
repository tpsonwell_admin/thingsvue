import { BasicColumn, FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import dayjs from 'dayjs';
import { DescItem } from '/@/components/Description';
export const columns: BasicColumn[] = [
  {
    title: '部门名称',
    dataIndex: 'deptName',
    width: 160,
    align: 'left',
  },
  {
    title: '排序',
    dataIndex: 'orderNo',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '名称',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '是否根链',
    dataIndex: 'deptName',
    width: 180,
  },
];

export const ruleColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
    customRender: ({ record }) => {
      return record.name === null ? '--' : record.name;
    },
  },
  {
    title: '是否根链',
    dataIndex: 'root',
    customRender: ({ record }) => {
      const enable = record.root;
      const color = enable ? 'green' : 'red';
      const text = enable ? '是' : '否';
      return h(Tag, { color: color }, () => text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入关键字',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];
// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    label: '名称',
    field: 'name',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },

  {
    label: '是否根链',
    field: 'root',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
  {
    label: '调试模式',
    field: 'debugMode',
    render: (val: any) => {
      return <span>{val == true ? '是' : '否'}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return (
        <span>
          {val == '' || val == null || val.description == undefined ? '— —' : val.description}
        </span>
      );
    },
  },
];
