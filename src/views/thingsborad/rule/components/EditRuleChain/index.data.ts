// 拖拽面板
export const configDefaultDndPanel = () => {
  return [
    {
      type: 'TBRect',
      text: 'device profile',
      label: 'device profile',
      properties: {
        // 存放表单数据
        type: 'action',
        schema: [
          {
            field: 'name',
            label: '名称',
            component: 'Input',
            componentProps: {
              placeholder: '请输入名称',
            },
            required: true,
          },
          {
            field: 'debugMode',
            label: '调试模式',
            component: 'Switch',
          },
          {
            field: 'persistAlarmRulesState',
            label: 'Persist state of alarm rules',
            component: 'Checkbox',
          },
          {
            field: 'fetchAlarmRulesStateOnStart',
            label: 'Fetch state of alarm rules',
            component: 'Checkbox',
          },
          {
            field: 'description',
            label: '描述',
            component: 'InputTextArea',
            componentProps: {
              placeholder: '请输入描述',
            },
            colProps: {
              span: 24,
            },
          },
        ],
        data: {},
      },
      className: 'py-2.5 px-14 border-2 border-solid border-red-500 rounded',
      callback: (lf) => {
        lf.updateEditConfig({
          textEdit: false,
        });
      },
    },
    {
      type: 'TBRect',
      text: 'message type switch',
      label: 'message type switch',
      properties: {
        // 存放表单数据
        type: 'attributes',
        schema: [
          {
            field: 'name',
            label: '名称',
            component: 'Input',
            componentProps: {
              placeholder: '请输入名称',
            },
            required: true,
          },
          {
            field: 'debugMode',
            label: '调试模式',
            component: 'Switch',
          },
          {
            field: 'description',
            label: '描述',
            component: 'InputTextArea',
            componentProps: {
              placeholder: '请输入描述',
            },
            colProps: {
              span: 24,
            },
          },
        ],
        data: {},
      },
      className: 'py-2.5 px-14 border-2 border-solid border-red-500 rounded',
      callback: (lf) => {
        lf.updateEditConfig({
          textEdit: false,
        });
      },
    },
    {
      type: 'TBRect',
      text: 'script',
      label: 'script',
      properties: {
        // 存放表单数据
        type: 'change',
        schema: [
          {
            field: 'name',
            label: '名称',
            component: 'Input',
            componentProps: {
              placeholder: '请输入名称',
            },
            required: true,
          },
          {
            field: 'debugMode',
            label: '调试模式',
            component: 'Switch',
          },
          {
            field: 'description',
            label: '描述',
            component: 'InputTextArea',
            componentProps: {
              placeholder: '请输入描述',
            },
            colProps: {
              span: 24,
            },
          },
        ],
        data: {},
      },
      className: 'py-2.5 px-14 border-2 border-solid border-red-500 rounded',
      callback: (lf) => {
        lf.updateEditConfig({
          textEdit: false,
        });
      },
    },
    {
      type: 'TBRect',
      text: 'save attributes',
      label: 'save attributes',
      properties: {
        // 存放表单数据
        type: 'action',
        schema: [
          {
            field: 'name',
            label: '名称',
            component: 'Input',
            componentProps: {
              placeholder: '请输入名称',
            },
            required: true,
          },
          {
            field: 'debugMode',
            label: '调试模式',
            component: 'Switch',
          },
          {
            field: 'scope',
            label: '设备属性范围',
            component: 'Select',
            componentProps: {
              allowClear: false,
              options: [
                { label: '客户端属性', value: 'CLIENT_SCOPE' },
                { label: '服务端属性', value: 'SERVER_SCOPE' },
                { label: '共享属性', value: 'SHARED_SCOPE' },
              ],
            },
            required: true,
            defaultValue: 'CLIENT_SCOPE',
          },
          {
            field: 'updateAttributesOnlyOnValueChange',
            label: 'Save attributes only if the value changes',
            component: 'Switch',
          },
        ],
        data: {},
      },
      className: 'py-2.5 px-14 border-2 border-solid border-red-500 rounded',
      callback: (lf) => {
        lf.updateEditConfig({
          textEdit: false,
        });
      },
    },
    {
      type: 'TBRect',
      text: 'save timeseries',
      label: 'save timeseries',
      properties: {
        // 存放表单数据
        type: 'action',
        schema: [
          {
            field: 'name',
            label: '名称',
            component: 'Input',
            componentProps: {
              placeholder: '请输入名称',
            },
            required: true,
          },
          {
            field: 'debugMode',
            label: '调试模式',
            component: 'Switch',
          },
          {
            field: 'defaultTTL',
            label: '默认TTL',
            component: 'InputNumber',
            componentProps: {
              placeholder: '请输入默认TTL',
            },
          },
          {
            field: 'skipLatestPersistence',
            label: 'Skip latest persistence',
            component: 'Checkbox',
          },
          {
            field: 'useServerTs',
            label: 'Use server ts',
            component: 'Checkbox',
          },
          {
            field: 'description',
            label: '描述',
            component: 'InputTextArea',
            componentProps: {
              placeholder: '请输入描述',
            },
            colProps: {
              span: 24,
            },
          },
        ],
        data: {},
      },
      className: 'py-2.5 px-14 border-2 border-solid border-red-500 rounded',
      callback: (lf) => {
        lf.updateEditConfig({
          textEdit: false,
        });
      },
    },
  ];
};

// 初始画布数据
export const initCanvasData = {
  flowElementList: [
    {
      incoming: [],
      outgoing: [],
      dockers: [],
      type: 3,
      properties: {
        type: 'input',
        x: 400,
        y: 100,
        text: {
          x: 400,
          y: 100,
          value: 'Input',
        },
        logicFlowType: 'TBRect',
      },
      key: 'Input_08p8i6q',
    },
  ],
};
