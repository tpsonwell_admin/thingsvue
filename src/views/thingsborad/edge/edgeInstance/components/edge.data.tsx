import { BasicColumn, FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { SelectModal } from '/@/views/thingsborad/notification/components/data/notification.data';
import dayjs from 'dayjs';
import { DescItem } from '/@/components/Description';
import { log } from 'console';

export const columns: BasicColumn[] = [
  {
    title: '部门名称',
    dataIndex: 'deptName',
    width: 160,
    align: 'left',
  },
  {
    title: '排序',
    dataIndex: 'orderNo',
    width: 50,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 80,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 0;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '边缘类型',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '标签',
    dataIndex: 'deptName',
    width: 180,
  },
  {
    title: '客户',
    dataIndex: 'deptName',
    width: 180,
  },
];
export const edgeColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    align: 'left',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    width: 160,
    sorter: true,
    customRender: ({ record }) => {
      return record.name === null ? '--' : record.name;
    },
  },

  {
    title: '边缘类型',
    dataIndex: 'type',
    width: 180,
    customRender: ({ record }) => {
      return record.type === '' || record.type === null ? '--' : record.type;
    },
    sorter: true,
  },
  {
    title: '标签',
    dataIndex: 'label',
    width: 180,
    sorter: true,
    customRender: ({ record }) => {
      return record.label === '' || record.label === null ? '--' : record.label;
    },
  },
  {
    title: '客户',
    dataIndex: 'customerTitle',
    width: 180,
    sorter: true,
    customRender: ({ record }) => {
      return record.customerTitle === null ? '--' : record.customerTitle;
    },
  },
];

export const customerEdgeColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    align: 'left',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
    width: 160,
    sorter: true,
  },

  {
    title: '边缘类型',
    dataIndex: 'type',
    width: 180,
    sorter: true,
  },
  {
    title: '标签',
    dataIndex: 'label',
    width: 180,
    sorter: true,
  },
];
export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入搜索关键字',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const edgeTypeSelectList: SelectModal[] = [
  { value: '全部', label: '全部' },
  { value: 'default', label: 'default' },
];

//导航栏类型定义
export interface navBar {
  icon: string;
  name: string;
}

// 导航栏数据列表
export const navBarList: navBar[] = [
  {
    icon: 'mdi:ubuntu',
    name: '操作系统',
  },
  {
    icon: 'mdi:centos',
    name: 'CentOS/RHEL',
  },
  {
    icon: 'ion:logo-docker',
    name: 'docker',
  },
];

// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    label: '名称',
    field: 'name',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    label: '边缘类型',
    field: 'type',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '边缘建',
    field: 'routingKey',
    render: (val) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '边缘密钥',
    field: 'secret',
    render: (val) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '标签',
    field: 'label',
    render: (val) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return (
        <span>
          {val == '' || val == null || val.description == undefined ? '— —' : val.description}
        </span>
      );
    },
  },
];

// 抽屉详情配置
export const customerDetailSchema: DescItem[] = [
  {
    label: '名称',
    field: 'name',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    label: '边缘类型',
    field: 'type',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '标签',
    field: 'label',
    render: (val) => {
      return <span>{val == '' || val == null || val === undefined ? '— —' : val}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return (
        <span>
          {val == '' || val == null || val.description == undefined ? '— —' : val.description}
        </span>
      );
    },
  },
];
