import { BasicColumn, FormSchema } from '/@/components/Table';

export const securityColumns: BasicColumn[] = [
  {
    title: '时间戳',
    dataIndex: 'createdTime',
    width: 150,
    sorter: true,
  },
  {
    title: '实体类型',
    dataIndex: 'entityType',
    width: 100,
    sorter: true,
  },
  {
    title: '实体名称',
    dataIndex: 'entityName',
    width: 100,
    sorter: true,
  },
  {
    title: '用户',
    dataIndex: 'userName',
    width: 100,
    sorter: true,
  },
  {
    title: '类型',
    dataIndex: 'actionType',
    width: 100,
    sorter: true,
  },
  {
    title: '状态',
    dataIndex: 'actionStatus',
    width: 100,
    sorter: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入检索条件',
    },
  },
];
