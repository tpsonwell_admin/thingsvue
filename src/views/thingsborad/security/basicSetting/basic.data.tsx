import { FormSchema } from '/@/components/Form/index';
import { ref, nextTick } from 'vue';
export const hasSettingEdit = ref<boolean>(false);
export const hasJWtEdit = ref<boolean>(false);
function setHasSettingEdit() {
  hasSettingEdit.value = true;
}
function setHasJWtEdit() {
  hasJWtEdit.value = true;
}
export const securitySettings: FormSchema[] = [
  {
    field: 'maxFailedLoginAttempts',
    component: 'InputNumber',
    label: '登录失败之前的最大登录尝试次数',
    colProps: {
      span: 24,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'userLockoutNotificationEmail',
    component: 'Input',
    label: '如果账户锁定，请发送通知到电子邮件',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'divider',
    label: '密码策略',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.minimumLength',
    component: 'InputNumber',
    label: '最小密码长度',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: '5',
      max: '50',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.minimumUppercaseLetters',
    component: 'InputNumber',
    label: '最少大写字母位数',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.minimumLowercaseLetters',
    component: 'InputNumber',
    label: '最少小写字母位数',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.minimumDigits',
    component: 'InputNumber',
    label: '最少数字位数',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.minimumSpecialCharacters',
    component: 'InputNumber',
    label: '最少特殊字符位数',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.passwordExpirationPeriodDays',
    component: 'InputNumber',
    label: '密码有效期(天)',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.passwordReuseFrequencyDays',
    component: 'InputNumber',
    label: '密码重用频率',
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '0',
      parser: formatNumber,
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
  {
    field: 'passwordPolicy.allowWhitespaces',
    component: 'Checkbox',
    label: '',
    renderComponentContent: '允许有空格',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setHasSettingEdit();
      },
    },
  },
];

export const JWTSettings: FormSchema[] = [
  {
    field: 'tokenIssuer',
    component: 'Input',
    label: '发行者名称',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      onChange: () => {
        setHasJWtEdit();
      },
    },
  },
  {
    field: 'tokenSigningKey',
    component: 'Input',
    label: '签名密钥',
    slot: 'tokenSigningKey',
    required: true,
    colProps: {
      span: 12,
    },
    rules: [
      {
        validator(_rule, value, _callback) {
          if (value === '' || value === 'thingsboardDefaultSigningKey') {
            return Promise.resolve();
          } else {
            try {
              const decodedValue = atob(value);
              if (decodedValue.length < 32) {
                return Promise.reject(new Error(' 签名密钥必须至少为256位的数据'));
              } else {
                return Promise.resolve();
              }
            } catch (e) {
              return Promise.reject(new Error('签名密钥必须是Base64格式'));
            }
          }
        },
      },
    ],
    componentProps: {
      onChange: () => {
        setHasJWtEdit();
      },
    },
  },
  {
    field: 'tokenExpirationTime',
    component: 'InputNumber',
    label: '令牌过期时间(秒)',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: ({ formActionType }) => {
      return {
        min: '60',
        parser: formatNumber,
        onChange: async () => {
          setHasJWtEdit();
          if (!formActionType) {
            return;
          }
          nextTick(async () => {
            await formActionType.validate();
          });
        },
      };
    },
  },
  {
    field: 'refreshTokenExpTime',
    component: 'InputNumber',
    label: '刷新令牌过期时间(秒)',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      parser: formatNumber,
      onChange: () => {
        setHasJWtEdit();
      },
    },
    dynamicRules: (form) => {
      return [
        {
          validator(_rule, value) {
            if (value <= form.model.tokenExpirationTime) {
              return Promise.reject(new Error('刷新令牌过期时间必须大于令牌过期时间'));
            } else {
              return Promise.resolve();
            }
          },
        },
      ];
    },
  },
];

function formatNumber(text) {
  const decimalIndex = text.indexOf('.');
  return /^\d+$/.test(text) ? text : text.slice(0, decimalIndex);
}
