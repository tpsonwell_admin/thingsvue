import { FormSchema } from '/@/components/Form/index';
import { ref } from 'vue';
export const isEdit = ref<boolean>(false);
function setIsEdit() {
  isEdit.value = true;
}
export const TOTPForm: FormSchema[] = [
  {
    field: 'issuerName',
    component: 'Input',
    label: '发行者名称',
    defaultValue: 'ThingsBoard',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsEdit();
      },
    },
  },
];
export const SMSForm: FormSchema[] = [
  {
    field: 'smsVerificationMessageTemplate',
    component: 'Input',
    label: '验证消息模板',
    defaultValue: 'Verification code: ${code}',
    required: true,
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /\${code}/, message: ' 验证消息需要包含模板：$ {code}' }],
    componentProps: {
      onChange: () => {
        setIsEdit();
      },
    },
  },
  {
    field: 'verificationCodeLifetime',
    component: 'InputNumber',
    label: '验证码生存期(秒)',
    defaultValue: 120,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '1',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
];

export const EMAILForm: FormSchema[] = [
  {
    field: 'verificationCodeLifetime',
    component: 'InputNumber',
    label: '验证码生存期(秒)',
    defaultValue: 120,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: '1',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
];

export const BackUPForm: FormSchema[] = [
  {
    field: 'codesQuantity',
    component: 'InputNumber',
    label: '验证码数量',
    defaultValue: 10,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: '1',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
];

export const JverifyForm: FormSchema[] = [
  {
    field: 'totalAllowedTimeForVerification',
    component: 'InputNumber',
    label: '总允许验证时间(秒)',
    required: true,
    colProps: {
      span: 8,
    },
    componentProps: {
      min: '60',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
  {
    field: 'minVerificationCodeSendPeriod',
    component: 'InputNumber',
    label: '重试验证码周期(秒)',
    required: true,
    colProps: {
      span: 8,
    },
    componentProps: {
      min: '5',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
  {
    field: 'maxVerificationFailuresBeforeUserLockout',
    component: 'InputNumber',
    label: '用户锁定前最大验证失败次数',
    colProps: {
      span: 8,
    },
    componentProps: {
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
];

export const CheckLimitForm: FormSchema[] = [
  {
    field: 'tryTimes',
    component: 'InputNumber',
    label: '检查尝试次数',
    required: true,
    defaultValue: 3,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '1',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
  {
    field: 'inTimes',
    component: 'InputNumber',
    label: '在时间内(秒)',
    defaultValue: 900,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: '1',
      parser: formatNumber,
      onChange: () => {
        setIsEdit();
      },
    },
  },
];
function formatNumber(text) {
  const decimalIndex = text.indexOf('.');
  return /^\d+$/.test(text) ? text : text.slice(0, decimalIndex);
}
