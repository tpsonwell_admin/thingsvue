import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { DescItem } from '/@/components/Description';
import dayjs from 'dayjs';

export const customersColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '租户配置',
    dataIndex: 'tenantProfileName',
    width: 160,
    sorter: true,
  },
  {
    title: '电子邮件',
    dataIndex: 'email',
    width: 160,
    sorter: true,
  },
  {
    title: '城市',
    dataIndex: 'city',
    width: 160,
    sorter: true,
  },
];

export const adminColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '名字',
    dataIndex: 'firstName',
    width: 160,
    sorter: true,
  },
  {
    title: '电子邮件',
    dataIndex: 'email',
    width: 160,
    sorter: true,
  },
];

export const tenantDetailSchema: DescItem[] = [
  {
    label: '标题',
    field: 'title',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: (val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    label: '电子邮件',
    field: 'email',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '手机号码',
    field: 'phone',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val.replace(/^\+86/, '')}</span>;
    },
  },
  {
    label: '国家',
    field: 'country',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '省份',
    field: 'state',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '城市',
    field: 'city',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '邮政编码',
    field: 'zip',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '地址1',
    field: 'address',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '地址2',
    field: 'address2',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val.description}</span>;
    },
  },
];

export const detailSchema: DescItem[] = [
  {
    label: '电子邮件',
    field: 'email',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: (val, { createdTime }) => {
      return <span>{dayjs(createdTime).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },

  {
    label: '手机号码',
    field: 'phone',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val.replace(/^\+86/, '')}</span>;
    },
  },
  {
    label: '说明',
    field: 'additionalInfo',
    render: (val: any) => {
      return (
        <span>{val.description === '' || val.description === null ? '— —' : val.description}</span>
      );
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  // {
  //   field: 'field6',
  //   component: 'Switch',
  //   label: '包括客户实体',
  //   colProps: {
  //     span: 8,
  //   },
  //   labelWidth: 200,
  // },
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入标题',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];
