import { FormSchema } from '/@/components/Form/index';
import { UseFormReturnType, useForm, FormProps } from '/@/components/Form';
import { buildUUID } from '/@/utils/uuid';
import { modalFormStyleSetting } from '/@/settings/thingsBoardDesign';

export const schemas: FormSchema[] = [
  {
    field: 'msgCount',
    component: 'InputNumber',
    label: '消息数量',
    componentProps: {
      placeholder: '请输入消息数量',
    },
    colProps: {
      span: 10,
    },
    required: true,
  },
  {
    field: 'perSecond',
    component: 'InputNumber',
    label: '每秒',
    componentProps: {
      placeholder: '请输入秒数',
    },
    colProps: {
      span: 10,
    },
    required: true,
  },
];
export const baseFormConfig: Partial<FormProps> = {
  layout: 'vertical',
  showActionButtonGroup: false,
  ...modalFormStyleSetting,
};
type ListType = {
  uuid: string;
  Form: UseFormReturnType;
};
export const formList: ListType[] = [
  {
    uuid: buildUUID(),
    Form: useForm(Object.assign({ schemas }, baseFormConfig) as FormProps),
  },
];
