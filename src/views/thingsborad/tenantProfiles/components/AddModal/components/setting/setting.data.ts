import { FormSchema } from '/@/components/Form/index';
export const schemas: FormSchema[] = [
  {
    field: 'field1',
    component: 'Divider',
    label: '实体（0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'maxDevices',
    component: 'InputNumber',
    label: '最大设备数',
    componentProps: {
      placeholder: '请输入最大设备数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxDashboards',
    component: 'InputNumber',
    label: '最大仪表盘数',
    componentProps: {
      placeholder: '请输入最大仪表盘数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxAssets',
    component: 'InputNumber',
    label: '最大资产数',
    componentProps: {
      placeholder: '请输入最大资产数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxUsers',
    component: 'InputNumber',
    label: '最大用户数',
    componentProps: {
      placeholder: '请输入最大用户数',
    },
    required: true,
    defaultValue: 0,
  },
  // 高级设置
  {
    field: 'maxCustomers',
    component: 'InputNumber',
    label: '最大客户数',
    componentProps: {
      placeholder: '请输入最大客户数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxRuleChains',
    component: 'InputNumber',
    label: '最大规则链数',
    componentProps: {
      placeholder: '请输入最大规则链数',
    },
    required: true,
    defaultValue: 0,
  },
  // 规则引擎
  {
    field: 'field12',
    component: 'Divider',
    label: '规则引擎（0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'maxREExecutions',
    component: 'InputNumber',
    label: '最大规则引擎执行数',
    componentProps: {
      placeholder: '请输入最大规则引擎执行数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxTransportMessages',
    component: 'InputNumber',
    label: '最大传输消息数',
    componentProps: {
      placeholder: '请输入最大传输消息数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxJSExecutions',
    component: 'InputNumber',
    label: '最大Javascript执行数',
    componentProps: {
      placeholder: '请输入最大Javascript执行数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxTbelExecutions',
    component: 'InputNumber',
    label: '最大TBEL执行数',
    componentProps: {
      placeholder: '请输入最大TBEL执行数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxRuleNodeExecutionsPerMessage',
    component: 'InputNumber',
    label: '每条消息的最大规则节点执行数',
    componentProps: {
      placeholder: '请输入每条消息的最大规则节点执行数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxTransportDataPoints',
    component: 'InputNumber',
    label: '最大传输数据点数',
    componentProps: {
      placeholder: '请输入最大传输数据点数',
    },
    required: true,
    defaultValue: 0,
  },
  // TTL
  {
    field: 'field2',
    component: 'Divider',
    label: 'TTL（0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'maxDPStorageDays',
    component: 'InputNumber',
    label: '最大存储点天数',
    componentProps: {
      placeholder: '请输入最大存储点天数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'alarmsTtlDays',
    component: 'InputNumber',
    label: '告警TTL天数',
    componentProps: {
      placeholder: '请输入告警TTL天数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'defaultStorageTtlDays',
    component: 'InputNumber',
    label: '默认存储TTL天数',
    componentProps: {
      placeholder: '请输入默认存储TTL天数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'rpcTtlDays',
    component: 'InputNumber',
    label: 'RPCTTL天数',
    componentProps: {
      placeholder: '请输入RPCTTL天数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'queueStatsTtlDays',
    component: 'InputNumber',
    label: '队列统计TTL天数',
    componentProps: {
      placeholder: '请输入队列统计TTL天数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'ruleEngineExceptionsTtlDays',
    component: 'InputNumber',
    label: '规则引擎异常TTL天',
    componentProps: {
      placeholder: '请输入规则引擎异常TTL天数',
    },
    required: true,
    defaultValue: 0,
  },
  // 告警与通知
  {
    field: 'field3',
    component: 'Divider',
    label: '告警与通知（0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'smsEnabled',
    component: 'Switch',
    label: 'SMS enabled',
    defaultValue: true,
  },
  {
    field: 'maxSms',
    component: 'InputNumber',
    label: '最大短信发送数',
    componentProps: {
      placeholder: '请输入最大短信发送数',
    },
    defaultValue: 0,
    ifShow: ({ model }) => model.smsEnabled,
  },
  {
    field: 'maxEmails',
    component: 'InputNumber',
    label: '最大邮件发送数',
    componentProps: {
      placeholder: '请输入最大邮件发送数',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxCreatedAlarms',
    component: 'InputNumber',
    label: '最大创建告警数',
    componentProps: {
      placeholder: '请输入最大创建告警数',
    },
    required: true,
    defaultValue: 0,
  },
  // OTA文件
  {
    field: 'field4',
    component: 'Divider',
    label: 'OTA文件（字节） （0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'maxResourcesInBytes',
    component: 'InputNumber',
    label: '资源文件总大小',
    componentProps: {
      placeholder: '请输入资源文件总大小',
    },
    required: true,
    defaultValue: 0,
  },
  {
    field: 'maxOtaPackagesInBytes',
    component: 'InputNumber',
    label: 'OTA包文件总大小',
    componentProps: {
      placeholder: '请输入OTA包文件总大小',
    },
    required: true,
    defaultValue: 0,
  },
  // WS
  {
    field: 'field5',
    component: 'Divider',
    label: 'WS （0 - 无限制）',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'maxWsSessionsPerTenant',
    component: 'InputNumber',
    label: '租户最大会话数',
    componentProps: {
      placeholder: '请输入租户最大会话数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSubscriptionsPerTenant',
    component: 'InputNumber',
    label: '租户最大订阅数',
    componentProps: {
      placeholder: '请输入租户最大订阅数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSessionsPerCustomer',
    component: 'InputNumber',
    label: '客户最大会话数',
    componentProps: {
      placeholder: '请输入客户最大会话数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSubscriptionsPerCustomer',
    component: 'InputNumber',
    label: '客户最大订阅数',
    componentProps: {
      placeholder: '请输入客户最大订阅数',
    },
    defaultValue: 0,
  },
  // 高级设置
  {
    field: 'maxWsSessionsPerPublicUser',
    component: 'InputNumber',
    label: '公共用户最大会话数',
    componentProps: {
      placeholder: '请输入公共用户最大会话数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSubscriptionsPerPublicUser',
    component: 'InputNumber',
    label: '公共用户最大订阅数',
    componentProps: {
      placeholder: '请输入公共用户最大订阅数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSessionsPerRegularUser',
    component: 'InputNumber',
    label: '普通用户最大会话数',
    componentProps: {
      placeholder: '请输入普通用户最大会话数',
    },
    defaultValue: 0,
  },
  {
    field: 'maxWsSubscriptionsPerRegularUser',
    component: 'InputNumber',
    label: '公共用户最大订阅数',
    componentProps: {
      placeholder: '请输入公共用户最大订阅数',
    },
    defaultValue: 0,
  },
  {
    field: 'wsMsgQueueLimitPerSession',
    component: 'InputNumber',
    label: '会话最大消息队列大小',
    componentProps: {
      placeholder: '请输入会话最大消息队列大小',
    },
    defaultValue: 0,
  },
  {
    field: 'divider5',
    component: 'Divider',
    label: '速率限制',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'transportTenantMsgRateLimit',
    component: 'Input',
    label: '传输租户消息',
    slot: 'transportTenantMsgRateLimit',
  },
  {
    field: 'transportDeviceMsgRateLimit',
    component: 'Input',
    label: '传输设备消息',
    slot: 'transportDeviceMsgRateLimit',
  },
  {
    field: 'transportTenantTelemetryMsgRateLimit',
    component: 'Input',
    label: '传输租户遥测消息',
    slot: 'transportTenantTelemetryMsgRateLimit',
  },
  {
    field: 'transportDeviceTelemetryMsgRateLimit',
    component: 'Input',
    label: '传输设备遥测消息',
    slot: 'transportDeviceTelemetryMsgRateLimit',
  },
  {
    field: 'transportTenantTelemetryDataPointsRateLimit',
    component: 'Input',
    label: '传输租户遥测数据点',
    slot: 'transportTenantTelemetryDataPointsRateLimit',
  },
  {
    field: 'transportDeviceTelemetryDataPointsRateLimit',
    component: 'Input',
    label: '传输设备遥测数据点',
    slot: 'transportDeviceTelemetryDataPointsRateLimit',
  },
  {
    field: 'tenantServerRestLimitsConfiguration',
    component: 'Input',
    label: '租户REST请求',
    slot: 'tenantServerRestLimitsConfiguration',
  },
  {
    field: 'customerServerRestLimitsConfiguration',
    component: 'Input',
    label: '客户REST请求',
    slot: 'customerServerRestLimitsConfiguration',
  },
  {
    field: 'tenantEntityExportRateLimit',
    component: 'Input',
    label: '实体版本创建',
    slot: 'tenantEntityExportRateLimit',
  },
  {
    field: 'tenantEntityImportRateLimit',
    component: 'Input',
    label: '实体版本加载',
    slot: 'tenantEntityImportRateLimit',
  },
  {
    field: 'wsUpdatesPerSessionRateLimit',
    component: 'Input',
    label: '会话WS更新',
    slot: 'wsUpdatesPerSessionRateLimit',
  },
  {
    field: 'cassandraQueryTenantRateLimitsConfiguration',
    component: 'Input',
    label: '租户Cassandra查询',
    slot: 'cassandraQueryTenantRateLimitsConfiguration',
  },
  {
    field: 'tenantNotificationRequestsRateLimit',
    component: 'Input',
    label: '通知请求',
    slot: 'tenantNotificationRequestsRateLimit',
  },
  {
    field: 'tenantNotificationRequestsPerRuleRateLimit',
    component: 'Input',
    label: '每个通知规则的通知请求',
    slot: 'tenantNotificationRequestsPerRuleRateLimit',
  },
];

export const limitNameList = [
  'transportTenantMsgRateLimit',
  'transportDeviceMsgRateLimit',
  'transportTenantTelemetryMsgRateLimit',
  'transportDeviceTelemetryMsgRateLimit',
  'transportTenantTelemetryDataPointsRateLimit',
  'transportDeviceTelemetryDataPointsRateLimit',
  'tenantServerRestLimitsConfiguration',
  'customerServerRestLimitsConfiguration',
  'tenantEntityExportRateLimit',
  'tenantEntityImportRateLimit',
  'wsUpdatesPerSessionRateLimit',
  'cassandraQueryTenantRateLimitsConfiguration',
  'tenantNotificationRequestsRateLimit',
  'tenantNotificationRequestsPerRuleRateLimit',
];
