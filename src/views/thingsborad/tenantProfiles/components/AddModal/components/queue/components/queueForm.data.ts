import { FormSchema } from '/@/components/Form/index';
export const schemas: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    slot: 'name',
    colProps: {
      span: 24,
    },
    required: true,
  },
  {
    field: 'submitSetting',
    component: 'Input',
    label: '',
    slot: 'submitSetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'retrySetting',
    component: 'Input',
    label: '',
    slot: 'retrySetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'pollingSetting',
    component: 'Input',
    label: '',
    slot: 'pollingSetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'customProperties',
    component: 'Input',
    label: '自定义属性',
    componentProps: {
      placeholder: '请输入自定义属性',
    },
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    componentProps: {
      placeholder: '请输入说明',
    },
    colProps: {
      span: 24,
    },
  },
];
export const editSchemas: FormSchema[] = [
  {
    field: 'submitSetting',
    component: 'Input',
    label: '',
    slot: 'submitSetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'retrySetting',
    component: 'Input',
    label: '',
    slot: 'retrySetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'pollingSetting',
    component: 'Input',
    label: '',
    slot: 'pollingSetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'customProperties',
    component: 'Input',
    label: '自定义属性',
    componentProps: {
      placeholder: '请输入自定义属性',
    },
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    componentProps: {
      placeholder: '请输入说明',
    },
    colProps: {
      span: 24,
    },
  },
];
export const submitSettingSchemas: FormSchema[] = [
  {
    field: 'type',
    component: 'RadioGroup',
    label: '策略类型',
    componentProps: {
      options: [
        {
          label: '按发起者顺序处理',
          value: 'SEQUENTIAL_BY_ORIGINATOR',
        },
        {
          label: '按租户顺序处理',
          value: 'SEQUENTIAL_BY_TENANT',
        },
        {
          label: '顺序处理',
          value: 'SEQUENTIAL',
        },
        {
          label: '突发处理',
          value: 'BURST',
        },
        {
          label: '批量处理',
          value: 'BATCH',
        },
      ],
    },
    colProps: {
      span: 24,
    },
    required: true,
  },
  {
    field: 'batchSize',
    component: 'InputNumber',
    label: '分组参数',
    componentProps: {
      placeholder: '请输入批量处理大小',
    },
    required: true,
    ifShow: ({ model }) => model.type === 'BATCH',
  },
];
export const retrySettingSchemas: FormSchema[] = [
  {
    field: 'processType',
    component: 'RadioGroup',
    label: '处理类型',
    componentProps: {
      options: [
        {
          label: '失败与超时重试',
          value: 'RETRY_FAILED_AND_TIMED_OUT',
        },
        {
          label: '跳过所有失败',
          value: 'SKIP_ALL_FAILURES',
        },
        {
          label: '跳过所有失败和超时',
          value: 'SKIP_ALL_FAILURES_AND_TIMED_OUT',
        },
        {
          label: '全部重试',
          value: 'RETRY_ALL',
        },
        {
          label: '失败重试',
          value: 'RETRY_FAILED',
        },
        {
          label: '超时重试',
          value: 'RETRY_TIMED_OUT',
        },
      ],
    },
    colProps: {
      span: 24,
    },
    required: true,
  },
  {
    field: 'retries',
    component: 'InputNumber',
    label: '重试次数',
    componentProps: {
      placeholder: '请输入重试次数',
    },
    required: true,
  },
  {
    field: 'failurePercentage',
    component: 'InputNumber',
    label: '跳过重试的失败消息百分比',
    componentProps: {
      placeholder: '请输入跳过重试的失败消息百分比',
    },
    required: true,
  },
  {
    field: 'pauseBetweenRetries',
    component: 'InputNumber',
    label: '重试间隔',
    componentProps: {
      placeholder: '请输入重试间隔',
    },
    required: true,
  },
  {
    field: 'maxPauseBetweenRetries',
    component: 'InputNumber',
    label: '最大重试间隔',
    componentProps: {
      placeholder: '请输入最大重试间隔',
    },
    required: true,
  },
];
export const pollingSettingSchemas: FormSchema[] = [
  {
    field: 'field1',
    component: 'Divider',
    label: '批量处理',
    colProps: {
      span: 24,
    },
    required: true,
  },
  {
    field: 'pollInterval',
    component: 'InputNumber',
    label: '轮询间隔',
    componentProps: {
      placeholder: '请输入轮询间隔',
    },
    required: true,
  },
  {
    field: 'partitions',
    component: 'InputNumber',
    label: '分区',
    componentProps: {
      placeholder: '请输入分区',
    },
    required: true,
  },
  {
    field: 'field1',
    component: 'Divider',
    label: '即时处理',
    colProps: {
      span: 24,
    },
    required: true,
  },
  {
    field: 'consumerPerPartition',
    component: 'Checkbox',
    label: '每个分区消费者单独轮询消息',
  },
  {
    field: 'packProcessingTimeout',
    component: 'InputNumber',
    label: '处理超时',
    componentProps: {
      placeholder: '请输入处理超时',
    },
    required: true,
  },
];
