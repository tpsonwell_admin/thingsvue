import { UseFormReturnType, useForm, FormProps } from '/@/components/Form';
import { modalFormStyleSetting } from '/@/settings/thingsBoardDesign';
import { editSchemas, schemas } from './components/queueForm.data';
import {
  submitSettingSchemas,
  retrySettingSchemas,
  pollingSettingSchemas,
} from './components/queueForm.data';

export function guid(): string {
  function s4(): string {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

interface slotFormInstance {
  submitSettingForm: UseFormReturnType;
  retrySettingForm: UseFormReturnType;
  pollingSettingForm: UseFormReturnType;
}
export interface ListType {
  id: string;
  Form: UseFormReturnType;
  slotFormInstance: slotFormInstance;
  formValue: any;
}
export const baseFormConfig: Partial<FormProps> = {
  layout: 'vertical',
  showActionButtonGroup: false,
  ...modalFormStyleSetting,
};

export const generateFormInsFun = (): slotFormInstance => {
  return {
    submitSettingForm: useForm(
      Object.assign({ schemas: submitSettingSchemas }, baseFormConfig) as FormProps,
    ),
    retrySettingForm: useForm(
      Object.assign({ schemas: retrySettingSchemas }, baseFormConfig) as FormProps,
    ),
    pollingSettingForm: useForm(
      Object.assign({ schemas: pollingSettingSchemas }, baseFormConfig) as FormProps,
    ),
  };
};

export const formList: ListType[] = [
  {
    id: guid(),
    Form: useForm(
      Object.assign(
        {
          schemas: [
            {
              field: 'name',
              component: 'Input',
              label: '名称',
              colProps: {
                span: 24,
              },
              slot: 'name',
              required: true,
              defaultValue: 'Main',
            },
            ...editSchemas,
          ],
        },
        baseFormConfig,
      ) as FormProps,
    ),
    slotFormInstance: generateFormInsFun(),
    formValue: {
      name: 'Main',
      submitSetting: {
        type: 'BURST',
      },
      retrySetting: {
        processType: 'SKIP_ALL_FAILURES',
        retries: 3,
        failurePercentage: 0,
        pauseBetweenRetries: 3,
        maxPauseBetweenRetries: 3,
      },
      pollingSetting: {
        pollInterval: 2000,
        partitions: 1,
        consumerPerPartition: false,
        packProcessingTimeout: 10000,
      },
    },
  },
  {
    id: guid(),
    Form: useForm(
      Object.assign(
        {
          schemas: [
            {
              field: 'name',
              component: 'Input',
              label: '名称',
              colProps: {
                span: 24,
              },
              slot: 'name',
              required: true,
              defaultValue: 'HighPriority',
            },
            ...editSchemas,
          ],
        },
        baseFormConfig,
      ) as FormProps,
    ),
    slotFormInstance: generateFormInsFun(),
    formValue: {
      name: 'HighPriority',
      submitSetting: {
        type: 'BURST',
      },
      retrySetting: {
        processType: 'RETRY_FAILED_AND_TIMED_OUT',
        retries: 0,
        failurePercentage: 0,
        pauseBetweenRetries: 5,
        maxPauseBetweenRetries: 5,
      },
      pollingSetting: {
        pollInterval: 2000,
        partitions: 1,
        consumerPerPartition: false,
        packProcessingTimeout: 10000,
      },
    },
  },
  {
    id: guid(),
    Form: useForm(
      Object.assign(
        {
          schemas: [
            {
              field: 'name',
              component: 'Input',
              label: '名称',
              colProps: {
                span: 24,
              },
              slot: 'name',
              required: true,
              defaultValue: 'SequentialByOriginator',
            },
            ...editSchemas,
          ],
        },
        baseFormConfig,
      ) as FormProps,
    ),
    slotFormInstance: generateFormInsFun(),
    formValue: {
      name: 'SequentialByOriginator',
      submitSetting: {
        type: 'SEQUENTIAL_BY_ORIGINATOR',
      },
      retrySetting: {
        processType: 'RETRY_FAILED_AND_TIMED_OUT',
        retries: 3,
        failurePercentage: 0,
        pauseBetweenRetries: 5,
        maxPauseBetweenRetries: 5,
      },
      pollingSetting: {
        pollInterval: 2000,
        partitions: 1,
        consumerPerPartition: false,
        packProcessingTimeout: 10000,
      },
    },
  },
];
