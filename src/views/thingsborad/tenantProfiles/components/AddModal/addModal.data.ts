import { FormSchema } from '/@/components/Form/index';
export const schemas: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    componentProps: {
      placeholder: '请输入名称',
    },
    required: true,
  },
  {
    field: 'isolatedTbRuleEngine',
    component: 'Checkbox',
    label: '使用独立的规则引擎服务',
    // subLabel: '每个独立租户需要单独的规则引擎微服务',
  },
  {
    field: 'queue',
    component: 'Input',
    label: '',
    slot: 'queueSlot',
    colProps: {
      span: 24,
    },
    ifShow: ({ model }) => model.isolatedTbRuleEngine,
  },
  {
    field: 'profileSetting',
    component: 'Input',
    label: '',
    slot: 'settingSlot',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    componentProps: {
      placeholder: '请输入说明',
    },
    colProps: {
      span: 24,
    },
  },
];
