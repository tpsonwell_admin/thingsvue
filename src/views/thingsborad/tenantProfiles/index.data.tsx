import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { FormSchema } from '/@/components/Form';
import { DescItem } from '/@/components/Description/index';
import { h } from 'vue';

export const tableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '说明',
    dataIndex: 'remark',
  },
  {
    title: '默认',
    dataIndex: 'default',
    customRender: ({ record }) => {
      const icon = record.default
        ? 'ant-design:check-square-outlined'
        : 'ant-design:border-outlined';
      return h(Icon, { icon, size: 20 });
    },
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    component: 'Input',
    label: '',
    componentProps: {
      placeholder: '请输入关键字',
    },
  },
];

export const detailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
  },
  {
    field: 'name',
    label: '名称',
  },
  {
    field: 'default',
    label: '是否为默认配置',
    render: (value) => {
      return value ? '是' : '否';
    },
  },
  {
    field: 'description',
    label: '说明',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'isolatedTbRuleEngine',
    label: '是否使用独立的规则引擎服务',
    render: (value) => {
      return value ? '是' : '否';
    },
  },
];
