//
export const linkParams = {
  attrSubCmds: [],
  tsSubCmds: [],
  historyCmds: [],
  entityDataCmds: [
    {
      query: {
        entityFilter: {
          type: 'singleEntity',
          singleEntity: {
            entityType: 'DEVICE',
            id: '63e416f1-fb97-11ee-bf35-41a6f6398a5e',
          },
        },
        pageLink: {
          pageSize: 1024,
          page: 0,
          sortOrder: {
            key: {
              type: 'ENTITY_FIELD',
              key: 'createdTime',
            },
            direction: 'DESC',
          },
        },
        entityFields: [
          {
            type: 'ENTITY_FIELD',
            key: 'name',
          },
          {
            type: 'ENTITY_FIELD',
            key: 'label',
          },
          {
            type: 'ENTITY_FIELD',
            key: 'additionalInfo',
          },
        ],
        latestValues: [],
      },
      cmdId: 1,
    },
  ],
  entityDataUnsubscribeCmds: [],
  alarmDataCmds: [],
  alarmDataUnsubscribeCmds: [],
  entityCountCmds: [],
  entityCountUnsubscribeCmds: [],
  alarmCountCmds: [],
  alarmCountUnsubscribeCmds: [],
};

//
export const sendParams = {
  attrSubCmds: [],
  tsSubCmds: [],
  historyCmds: [],
  entityDataCmds: [
    {
      cmdId: 1,
      historyCmd: {
        keys: ['temperature', 'k17', 'k7', 'ke', 'ke1'],
        startTs: 1710919478824,
        endTs: 1713511478824,
        interval: 7200000,
        limit: 360,
        agg: 'AVG',
      },
    },
  ],
  entityDataUnsubscribeCmds: [],
  alarmDataCmds: [],
  alarmDataUnsubscribeCmds: [],
  entityCountCmds: [],
  entityCountUnsubscribeCmds: [],
  alarmCountCmds: [],
  alarmCountUnsubscribeCmds: [],
};
