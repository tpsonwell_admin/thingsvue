import { useWebsocketStoreWithOut } from '/@/store/modules/thingsboard/websocket';
import {
  processingChartPublicFormData,
  processingChartPrivateFormData,
  processingChartPrivateReceiveData,
  processingChartPublicReceiveData,
} from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/chart/index.data';
const ws = useWebsocketStoreWithOut();

export function getWidgetData(cmdId: number, type: string, formData: Recordable) {
  //发送第一次链接请求
  publicSend(cmdId, type, formData);
  //发送第二次查询请求 并返回数据
  return privateSend(cmdId, type, formData);
}
// public send
function publicSend(cmdId: number, type: string, formData: Recordable) {
  // 赋值第一次链接请求参数
  if (type === 'timeseries') {
    const sendMsg = processingChartPublicFormData(cmdId, formData);
    ws.send(cmdId, sendMsg, () => {});
  } else if (type == 'table') {
  }
}
// private send
function privateSend(cmdId: number, type: string, formData: Recordable) {
  if (type === 'timeseries') {
    const sendMsg = processingChartPrivateFormData(cmdId, formData);
    return new Promise((resolve) => {
      ws.send(cmdId, sendMsg, (data) => {
        if (data.update) {
          const partOptions = processingChartPrivateReceiveData(data, formData);
          resolve(partOptions);
        }
      });
    });
  } else if (type == 'table') {
    return new Promise((resolve) => {
      ws.send(1, formData, (data) => {
        resolve(data);
      });
    });
  }
}
