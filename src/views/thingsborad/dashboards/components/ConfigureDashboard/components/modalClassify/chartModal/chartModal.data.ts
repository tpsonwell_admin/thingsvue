import { FormSchema } from '/@/components/Form/index';
import type { EChartsOption } from 'echarts';
import { getEntityDeviceList } from '/@/api/thingsborad/entity/entity';
import { useDebounceFn } from '@vueuse/shared';
import { ref } from 'vue';
import { getEntityKeys } from '/@/api/thingsborad/dashboards/dashboard';
import { publicChartOptions } from '/@/components/thingsborad/sharedComponents/modules/widgetsComponents/chart/index.data';
// 设备options请求参数
const params = ref({
  pageSize: 50,
  page: 0,
  textSearch: '',
  sortProperty: 'name',
  sortOrder: 'ASC',
});
// series options请求参数
const seriesParams = ref<Recordable>({
  payload: {
    entityFilter: {
      type: 'singleEntity',
      singleEntity: null,
    },
    pageLink: {
      pageSize: 100,
      page: 0,
      sortOrder: {
        key: {
          type: 'ENTITY_FIELD',
          key: 'createdTime',
        },
        direction: 'DESC',
      },
    },
  },
  query: {
    attributes: true,
    timeseries: true,
  },
});

export function setDeviceOptionsParams(entityName: string) {
  params.value.textSearch = entityName;
}
function formatter(value: number) {
  return `${value}%`;
}
// 时间戳对应关联选项映射关系
export const reflectOptions = {
  day: {
    startTs: 24 * 60 * 60 * 1000,
    options: [
      {
        label: '5分钟',
        value: 5 * 60 * 1000,
      },
      {
        label: '10分钟',
        value: 10 * 60 * 1000,
      },
      {
        label: '15分钟',
        value: 15 * 60 * 1000,
      },
      {
        label: '30分钟',
        value: 30 * 60 * 1000,
      },
      {
        label: '1小时',
        value: 60 * 60 * 1000,
      },
      {
        label: '2小时',
        value: 2 * 60 * 60 * 1000,
      },
    ],
  },
  week: {
    startTs: 7 * 24 * 60 * 60 * 1000,
    options: [
      {
        label: '30分钟',
        value: 30 * 60 * 1000,
      },
      {
        label: '1小时',
        value: 60 * 60 * 1000,
      },
      {
        label: '2小时',
        value: 2 * 60 * 60 * 1000,
      },
      {
        label: '5小时',
        value: 5 * 60 * 60 * 1000,
      },
      {
        label: '10小时',
        value: 10 * 60 * 60 * 1000,
      },
      {
        label: '12小时',
        value: 12 * 60 * 60 * 1000,
      },
      {
        label: '1天',
        value: 24 * 60 * 60 * 1000,
      },
    ],
  },
  month: {
    startTs: 30 * 24 * 60 * 60 * 1000,
    options: [
      {
        label: '2小时',
        value: 2 * 60 * 60 * 1000,
      },
      {
        label: '5小时',
        value: 5 * 60 * 60 * 1000,
      },
      {
        label: '10小时',
        value: 10 * 60 * 60 * 1000,
      },
      {
        label: '12小时',
        value: 12 * 60 * 60 * 1000,
      },
      {
        label: '1天',
        value: 24 * 60 * 60 * 1000,
      },
    ],
  },
  year: {
    startTs: 365 * 24 * 60 * 60 * 1000,
    options: [
      {
        label: '1天',
        value: 24 * 60 * 60 * 1000,
      },
      {
        label: '7天',
        value: 7 * 24 * 60 * 60 * 1000,
      },
      {
        label: '30天',
        value: 30 * 24 * 60 * 60 * 1000,
      },
    ],
  },
};

// 仪表盘schemas配置
export const dashboardChartschemas: FormSchema[] = [
  {
    field: 'Divider34',
    label: '卡片配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'cardTitle',
    component: 'Input',
    label: '卡片标题',
    required: true,
  },
  {
    field: 'Divider1',
    label: '数据源',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'type',
    label: '类型',
    component: 'Select',
    componentProps: () => {
      return {
        placeholder: '请选择类型',
        allowClear: false,
        options: [
          {
            label: '设备',
            value: 'device',
          },
          {
            label: '告警',
            value: 'alarm',
          },
        ],
      };
    },
    defaultValue: 'device',
    required: true,
  },
  {
    field: 'deviceId',
    label: '设备',
    component: 'ApiSelect',
    componentProps: () => {
      return {
        placeholder: '请选择设备',
        listHeight: 160,
        api: getEntityDeviceList,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id',
        objectToJson: true,
        showSearch: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
        onChange: (e: string) => {
          if (e) {
            seriesParams.value.payload.entityFilter.singleEntity = JSON.parse(e);
          } else {
            params.value.textSearch = '';
          }
        },
        immediate: true,
      };
    },
    ifShow: ({ values }) => values.type === 'device',
    defaultValue: undefined,
    required: true,
  },
  {
    field: 'times',
    label: '时间区间',
    component: 'Select',
    componentProps: {
      allowClear: false,
      listHeight: 160,
      options: [
        {
          label: '最近1天',
          value: 'day',
        },
        {
          label: '最近1周',
          value: 'week',
        },
        {
          label: '最近1月',
          value: 'month',
        },
        {
          label: '最近1年',
          value: 'year',
        },
      ],
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'agg',
    label: '数据聚合功能',
    component: 'Select',
    componentProps: {
      allowClear: false,
      listHeight: 160,
      options: [
        {
          label: '最小值',
          value: 'MIN',
        },
        {
          label: '最大值',
          value: 'MAX',
        },
        {
          label: '平均值',
          value: 'AVG',
        },
        {
          label: '求和',
          value: 'SUM',
        },
        {
          label: '计数',
          value: 'COUNT',
        },
        {
          label: '无',
          value: 'NONE',
        },
      ],
    },
    defaultValue: 'NONE',
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'interval',
    label: '分组间隔',
    component: 'Select',
    componentProps: ({ formModel }) => {
      return {
        options: reflectOptions[formModel.times].options,
      };
    },
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') && values.agg !== 'NONE',
    required: true,
  },
  {
    field: 'limit',
    label: '限制数据量',
    component: 'InputNumber',
    componentProps: {
      min: 7,
      max: 50000,
      step: 1,
    },
    defaultValue: 25000,
    required: true,
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') && values.agg == 'NONE',
  },
  {
    field: 'Divider2',
    label: 'Series配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
];

export function getAddSchemas(n: number): FormSchema[] {
  return [
    {
      field: `series.${n}.key`,
      label: '',
      component: 'ApiSelect',
      componentProps: {
        placeholder: '请选择数据键',
        listHeight: 160,
        api: getEntityKeys,
        params: seriesParams.value,
        isDashBoard: true,
        showSearch: true,
        immediate: true,
      },
      colProps: {
        span: 6,
        style: {
          marginRight: '20px',
        },
      },
      required: true,
    },
    {
      field: `series.${n}.name`,
      label: '',
      component: 'Input',
      componentProps: {
        placeholder: '请输入图例名称',
      },
      colProps: {
        span: 6,
        style: {
          marginRight: '20px',
        },
      },
      required: true,
    },
    {
      field: `series.${n}.color`,
      label: '',
      componentProps: {
        placeholder: '请输入图例颜色',
      },
      component: 'Input',
      slot: 'colorPickerSlot',
      colProps: {
        span: 6,
      },
      required: true,
    },
    {
      field: `${n}`,
      label: '',
      component: 'Input',
      slot: 'deleteSlot',
      colProps: {
        span: 4,
        style: {
          textAlign: 'center',
        },
      },
    },
  ];
}

// 条形图柱状图样式配置
export const lineBarSchemas: FormSchema[] = [
  {
    field: 'grid.left',
    label: '图表至容器左侧距离(百分比)',
    component: 'InputNumber',
    componentProps: {
      min: '0',
      max: '100',
      formatter,
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'grid.top',
    label: '图表至容器顶部距离',
    component: 'InputNumber',
    componentProps: {
      min: '0',
      max: '100',
      formatter,
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'grid.right',
    label: '图表至容器右侧距离',
    component: 'InputNumber',
    componentProps: {
      min: '0',
      max: '100',
      formatter,
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'grid.bottom',
    label: '图表至容器底部距离',
    component: 'InputNumber',
    componentProps: {
      min: '0',
      max: '100',
      formatter,
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'Divider5',
    label: '图例配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    // ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'legend.orient',
    label: '图例方向',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '水平',
          value: 'horizontal',
        },
        {
          label: '垂直',
          value: 'vertical',
        },
      ],
    },
    // ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'usingLegendLocation',
    label: '配置图例位置',
    component: 'Switch',
    // ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'legend.left',
    label: '图例至容器左侧距离',
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 100,
      formatter,
    },
    defaultValue: 0,
    ifShow: ({ values }) =>
      // (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLegendLocation === true,
    required: true,
  },
  {
    field: 'legend.top',
    label: '图例至容器顶部距离',
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 100,
      formatter,
    },
    defaultValue: 0,
    ifShow: ({ values }) =>
      // (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLegendLocation === true,
    required: true,
  },
  {
    field: 'Divider',
    label: '缩放配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'usingDataZoom',
    component: 'Switch',
    label: '启用图表缩放',
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'dataZoom.type',
    label: '缩放类型',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '滑动条',
          value: 'slider',
        },
        {
          label: '内置型',
          value: 'inside',
        },
      ],
    },
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') && values.usingDataZoom === true,
  },
  {
    field: 'Divider',
    label: '提示设置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'usingLineBarToolTip',
    label: '启用提示框',
    component: 'Switch',
    ifShow: ({ values }) => values.chartType === 'line' || values.chartType === 'bar',
  },
  {
    field: 'tooltip.backgroundColor',
    label: '提示框背景色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLineBarToolTip === true,
  },
  {
    field: 'tooltip.borderColor',
    label: '提示框边框色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLineBarToolTip === true,
  },
  {
    field: 'tooltip.textStyle.color',
    label: '提示框文字颜色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLineBarToolTip === true,
  },
  {
    field: 'tooltip.textStyle.fontSize',
    label: '提示框文字大小',
    component: 'InputNumber',
    required: true,
    ifShow: ({ values }) =>
      (values.chartType === 'line' || values.chartType === 'bar') &&
      values.usingLineBarToolTip === true,
  },
];

// 饼图样式配置
export const pieSchemas: FormSchema[] = [
  {
    field: 'name',
    label: '图表名称',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'radius',
    label: '图表半径(相对可视区的百分比尺寸)',
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 100,
      formatter,
    },
    defaultValue: 0,
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'Divider8',
    label: '高亮配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'usingEmphasis',
    label: '启用扇形高亮',
    component: 'Switch',
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'emphasis.itemStyle.shadowBlur',
    label: '阴影模糊大小',
    component: 'InputNumber',
    componentProps: {
      min: 0,
      max: 100,
    },
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingEmphasis === true,
  },
  {
    field: 'emphasis.itemStyle.shadowColor',
    label: '阴影颜色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingEmphasis === true,
  },
  {
    field: 'Divider9',
    label: '提示框配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'usingPieToolTip',
    label: '启用提示框',
    component: 'Switch',
    ifShow: ({ values }) => values.chartType === 'pie',
  },
  {
    field: 'tooltip.backgroundColor',
    label: '提示框背景色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => {
      return values.chartType === 'pie' && values.usingPieToolTip === true;
    },
  },
  {
    field: 'tooltip.borderColor',
    label: '提示框边框色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingPieToolTip === true,
  },
  {
    field: 'tooltip.borderWidth',
    label: '提示框边框宽度',
    component: 'InputNumber',
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingPieToolTip === true,
  },
  {
    field: 'tooltip.textStyle.color',
    label: '提示框文字颜色',
    component: 'Input',
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingPieToolTip === true,
  },
  {
    field: 'tooltip.textStyle.fontSize',
    label: '提示框文字大小',
    component: 'InputNumber',
    componentProps: {
      min: 5,
      max: 24,
    },
    defaultValue: 14,
    required: true,
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingPieToolTip === true,
  },
  {
    field: 'tooltip.textStyle.fontWeight',
    label: '提示框文字粗细',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '更细',
          value: 'lighter',
        },
        {
          label: '正常',
          value: 'normal',
        },
        {
          label: '加粗',
          value: 'bold',
        },
        {
          label: '更粗',
          value: 'bolder',
        },
      ],
    },
    defaultValue: 'normal',
    ifShow: ({ values }) => values.chartType === 'pie' && values.usingPieToolTip === true,
  },
];

// 部件库schemas配置
export const widgetsChartSchemas: FormSchema[] = [
  {
    field: 'Divider7',
    label: '图表配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'theme',
    label: '图表主题',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        { label: '深色', value: 'dark' },
        { label: '亮色', value: 'light' },
      ],
    },
    required: true,
  },
  {
    field: 'chartType',
    label: '图表类型',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '折线图',
          value: 'line',
        },
        {
          label: '柱状图',
          value: 'bar',
        },
        {
          label: '饼图',
          value: 'pie',
        },
        {
          label: '地图',
          value: 'map',
        },
      ],
    },
    defaultValue: 'line',
    required: true,
  },
  ...pieSchemas,
  ...lineBarSchemas,
];
