export interface gridItem {
  x: number;
  y: number;
  w: number;
  h: number;
  i: string;
  widget?: any;
  weightInfo?: any;
}

export const generate = (length?: number): string => {
  if (Boolean(typeof length == 'undefined') || length == null) {
    length = 1;
  }
  const l = length > 10 ? 10 : length;
  const str = Math.random().toString(36).substr(2, l);
  if (str.length >= length) {
    return str;
  }
  return str.concat(generate(length - str.length));
};

export const query = {
  entityFilter: {
    type: 'singleEntity',
    singleEntity: {
      entityType: 'DEVICE',
      id: 'a8cfd922-a48a-11ee-99f6-535103f50c3f',
    },
  },
  pageLink: {
    pageSize: 1024,
    page: 0,
    sortOrder: {
      key: {
        type: 'ENTITY_FIELD',
        key: 'createdTime',
      },
      direction: 'DESC',
    },
  },
  entityFields: [
    {
      type: 'ENTITY_FIELD',
      key: 'name',
    },
    {
      type: 'ENTITY_FIELD',
      key: 'label',
    },
    {
      type: 'ENTITY_FIELD',
      key: 'additionalInfo',
    },
  ],
  latestValues: [],
};
