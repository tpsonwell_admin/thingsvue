import { BasicColumn, FormSchema } from '/@/components/Table';
import Icon from '/@/components/Icon';
import dayjs from 'dayjs';
import { DescItem } from '/@/components/Description';

export const echartsColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '产品ID',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '断路器开关',
    dataIndex: 'assignedCustomers',
    width: 160,
    sorter: true,
  },
  {
    title: '开关次数',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '设备ID',
    dataIndex: 'assignedCustomers',
    width: 160,
    sorter: true,
  },
  {
    title: '通信状态',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '设备时间戳',
    dataIndex: 'assignedCustomers',
    width: 160,
    sorter: true,
  },
  {
    title: '设备名称',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '设备编码',
    dataIndex: 'assignedCustomers',
    width: 160,
    sorter: true,
  },
  {
    title: '消息类型',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '消息ID',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
];

export const dashboardColumns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    width: 160,
    sorter: true,
  },
  {
    title: '标题',
    dataIndex: 'title',
    width: 160,
    sorter: true,
  },
  {
    title: '分配给客户',
    dataIndex: 'assignedCustomers',
    width: 160,
    sorter: true,
  },
  {
    title: '公开',
    width: 100,
    dataIndex: 'public',
    customRender: ({ record }) => {
      return record.public !== true ? (
        <Icon icon="ci:checkbox-unchecked" />
      ) : (
        <Icon icon="material-symbols:check-box-outline" />
      );
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '',
    componentProps: () => {
      return {
        placeholder: '输入关键字',
      };
    },
    component: 'Input',
    colProps: { span: 8 },
  },
];

// 抽屉详情配置
export const detailSchema: DescItem[] = [
  {
    label: '标题',
    field: 'title',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    label: '创建时间',
    field: 'createdTime',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },

  {
    label: '说明',
    field: 'description',
    render: (val: any) => {
      return (
        <span>
          {val == '' || val == null || val.description == undefined ? '— —' : val.description}
        </span>
      );
    },
  },
];
