import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Icon } from '/@/components/Icon';
import { useMessage } from '/@/hooks/web/useMessage';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import { Button } from 'ant-design-vue';
import { StyleValue } from 'vue';
function convertFileSize(size, fromUnit) {
  const units = {
    B: 1,
    KB: 1024,
    MB: 1024 * 1024,
    GB: 1024 * 1024 * 1024,
    TB: 1024 * 1024 * 1024 * 1024,
  };

  if (!units[fromUnit]) {
    return 'Invalid unit';
  }

  let toUnit = 'B';
  const result = size;

  for (const unit in units) {
    if (result / units[unit] < 1024) {
      toUnit = unit;
      break;
    }
  }

  return (result / units[toUnit]).toFixed(2) + ' ' + toUnit;
}

export const OTATableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '标题',
    width: 150,
    dataIndex: 'title',
  },
  {
    title: '版本	',
    width: 100,
    dataIndex: 'version',
  },
  {
    title: '版本标签',
    width: 100,
    dataIndex: 'versionLabel',
  },
  {
    title: '包类型',
    width: 100,
    dataIndex: 'packetType',
  },
  {
    title: '直接URL',
    width: 100,
    dataIndex: 'URL',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };
      return (
        <div v-show={options.value}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <p style={style}>{options.value}</p>
            {/* <span>{options.value}</span> */}
            <Button onClick={copy} type="text" size="small" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </Button>
          </div>
        </div>
      );
    },
  },
  {
    title: '文件名',
    width: 100,
    dataIndex: 'fileName',
  },
  {
    title: '文件大小',
    width: 100,
    dataIndex: 'fileSize',
    customRender(opt) {
      return <span v-show={opt.value != null}>{convertFileSize(opt.value, 'B')}</span>;
    },
  },
  {
    title: '校验和',
    width: 100,
    dataIndex: 'checkSum',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };
      return (
        <div v-show={options.value}>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <p style={style}>{options.value}</p>
            <Button onClick={copy} type="text" size="small" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </Button>
          </div>
        </div>
      );
    },
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    label: '',
    componentProps: () => {
      return {
        placeholder: '请输入包名',
      };
    },
    component: 'Input',
  },
];
