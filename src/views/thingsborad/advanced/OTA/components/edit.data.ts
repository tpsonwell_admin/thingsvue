import { FormSchema } from '/@/components/Form/index';
export const schemas: FormSchema[] = [
  {
    field: 'remark',
    component: 'InputTextArea',
    label: '说明',
    componentProps: {
      placeholder: '请输入说明',
    },
    colProps: { span: 24 },
  },
];
