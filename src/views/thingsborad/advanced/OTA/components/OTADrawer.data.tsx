import { DescItem } from '/@/components/Description/index';
import dayjs from 'dayjs';

function convertFileSize(size, fromUnit) {
  const units = {
    B: 1,
    KB: 1024,
    MB: 1024 * 1024,
    GB: 1024 * 1024 * 1024,
    TB: 1024 * 1024 * 1024 * 1024,
  };

  if (!units[fromUnit]) {
    return 'Invalid unit';
  }

  let toUnit = 'B';
  const result = size;

  for (const unit in units) {
    if (result / units[unit] < 1024) {
      toUnit = unit;
      break;
    }
  }

  return (result / units[toUnit]).toFixed(2) + ' ' + toUnit;
}
export const URLDetailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
  },
  {
    field: 'title',
    label: '标题',
  },
  {
    field: 'version',
    label: '版本',
  },
  {
    field: 'versionLabel',
    label: '版本标签',
    render: (val: any) => {
      return <span>{val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'deviceConfiguration',
    label: '设备配置',
  },
  {
    field: 'packetType',
    label: '包类型',
  },
  {
    field: 'URL',
    label: '直接URL',
  },
  {
    field: 'remark',
    label: '说明',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
];

export const localPacketDetailSchema: DescItem[] = [
  {
    field: 'createdTime',
    label: '创建时间',
    render: ({ val }) => {
      return <span>{dayjs(val).format('YYYY-MM-DD HH:mm:ss')}</span>;
    },
  },
  {
    field: 'title',
    label: '标题',
  },
  {
    field: 'version',
    label: '版本',
  },
  {
    field: 'versionLabel',
    label: '版本标签',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'deviceConfiguration',
    label: '设备配置',
  },
  {
    field: 'packetType',
    label: '包类型',
    render: (val: any) => {
      let text = '固件';
      if (val == 'SOFTWARE') text = '软件';
      return text;
    },
  },
  {
    field: 'algorithm',
    label: '校验和算法',
  },
  {
    field: 'checkSum',
    label: '校验和',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'fileName',
    label: '文件名',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'fileSize',
    label: '文件大小',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : convertFileSize(val, 'B')}</span>;
    },
  },
  {
    field: 'contentType',
    label: '内容类型',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'remark',
    label: '说明',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
];
