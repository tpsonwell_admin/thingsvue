import { FormSchema } from '/@/components/Form/index';
import { getDeviceProfileInfos } from '/@/api/thingsborad/entity/entity';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const schemas: FormSchema[] = [
  {
    field: 'title',
    label: '标题',
    component: 'Input',
    componentProps: { placeholder: '请输入标题' },
    required: true,
  },
  {
    field: 'version',
    label: '版本',
    component: 'Input',
    componentProps: { placeholder: '请输入版本' },
    required: true,
  },
  {
    field: 'versionLabel',
    label: '版本标签',
    component: 'Input',
    componentProps: { placeholder: '请输入版本标签' },
  },
  {
    field: 'deviceConfiguration',
    label: '设备配置',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 10,
        textSearch: '',
      });
      return {
        placeholder: '请选择设备配置',
        listHeight: 160,
        showSearch: true,
        api: getDeviceProfileInfos,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'value',
        immediate: true,
        onOptionsChange: async (res) => {
          res.forEach((item) => {
            item.value = JSON.stringify(item.id);
          });
        },
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
    required: true,
  },
  {
    field: 'packetType',
    label: '包类型',
    component: 'Select',
    componentProps: {
      placeholder: '请选择包类型',
      allowClear: false,
      options: [
        { label: '固件', value: 'FIRMWARE' },
        { label: '软件', value: 'SOFTWARE' },
      ],
    },
    defaultValue: 'FIRMWARE',
    required: true,
  },
  {
    field: 'uploadMethod',
    label: '上传包方式',
    component: 'RadioGroup',
    componentProps: ({ formActionType }) => {
      return {
        options: [
          { label: '上传二进制文件', value: 'binary' },
          { label: '使用外部URL', value: 'URL' },
        ],
        onChange: ({ target }) => {
          const { updateSchema } = formActionType;
          if (target.value === 'URL') {
            updateSchema({
              field: 'remark',
            });
          } else {
            updateSchema({
              field: 'remark',
            });
          }
        },
      };
    },
    defaultValue: 'binary',
    required: true,
  },
  {
    field: 'localPacket',
    label: '选择本地包文件',
    component: 'Upload',
    slot: 'localPacket',
    required: true,
    ifShow: (value) => {
      return value?.model?.uploadMethod == 'binary';
    },
  },
  {
    field: 'autoGenerateChecksum',
    label: '自动生成校验和',
    component: 'Checkbox',
    defaultValue: true,
    ifShow: (value) => {
      return value?.model?.uploadMethod == 'binary';
    },
  },
  {
    field: 'algorithm',
    label: '校验和算法',
    component: 'Select',
    componentProps: {
      placeholder: '请选择校验和算法',
      listHeight: 150,
      allowClear: false,
      options: [
        { label: 'MD5', value: 'MD5' },
        { label: 'SHA-256', value: 'SHA256' },
        { label: 'SHA-384', value: 'SHA384' },
        { label: 'SHA-512', value: 'SHA512' },
        { label: 'CRC-32', value: 'CRC32' },
        { label: 'MURMUR3-32', value: 'MURMUR3_32' },
        { label: 'MURMUR3-128', value: 'MURMUR3_128' },
      ],
    },
    defaultValue: 'SHA256',
    required: true,
    show: (value) => {
      return value?.model?.uploadMethod == 'binary' && !value?.model?.autoGenerateChecksum;
    },
  },
  {
    field: 'checkSum',
    label: '校验和',
    component: 'Input',
    componentProps: { placeholder: '请输入校验和' },
    helpMessage: '如果校验和为空，会自动生成',
    ifShow: (value) => {
      return value?.model?.uploadMethod == 'binary' && !value?.model?.autoGenerateChecksum;
    },
  },
  {
    field: 'URL',
    label: '外部URL',
    component: 'Input',
    componentProps: { placeholder: '请输入直接URL' },
    required: true,
    ifShow: (value) => {
      return value?.model?.uploadMethod == 'URL';
    },
  },
  {
    field: 'remark',
    label: '说明',
    componentProps: { placeholder: '请输入说明' },
    component: 'InputTextArea',
    colProps: {
      span: 24,
    },
  },
];
