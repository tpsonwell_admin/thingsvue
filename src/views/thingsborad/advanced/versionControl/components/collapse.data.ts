export interface List {
  label: string;
  value: string;
  ifShow?: Boolean;
}
export const typeList: List[] = [
  { label: '资产', value: 'asset', ifShow: true },
  { label: '设备', value: 'device', ifShow: true },
  { label: '实体视图', value: 'entityView', ifShow: true },
  { label: '仪表盘', value: 'dashboard', ifShow: true },
  { label: '客户', value: 'customer', ifShow: true },
  { label: '设备配置', value: 'deviceConfiguration', ifShow: true },
  { label: '资产配置', value: 'assetConfiguration', ifShow: true },
  { label: '规则链库', value: 'ruleChainLibrary', ifShow: true },
  { label: 'Widgets', value: 'Widgets', ifShow: true },
  { label: '部件包', value: 'componentPackage', ifShow: true },
  { label: 'Notification templates', value: 'notificationTemplates', ifShow: true },
  { label: 'Notification recipients', value: 'notificationRecipients', ifShow: true },
  { label: 'Notification rules', value: 'notificationRules', ifShow: true },
];
