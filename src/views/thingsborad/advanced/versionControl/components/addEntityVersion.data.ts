import { FormSchema } from '/@/components/Form/index';

export const schemas: FormSchema[] = [
  {
    field: 'branch',
    label: '分支',
    component: 'Input',
    colProps: { span: 11, style: { marginRight: '1vw' } },
    componentProps: { placeholder: '分支*' },
    required: true,
  },
  {
    field: 'versionName',
    label: '版本名称',
    component: 'Input',
    colProps: { span: 11 },
    componentProps: { placeholder: '版本名称*' },
    required: true,
  },
  {
    field: 'synchronizationPolicy',
    label: '同步策略',
    component: 'Select',
    colProps: { span: 24 },
    componentProps: {
      allowClear: false,
      placeholder: '默认同步策略*',
      options: [
        { label: '合并', value: 'merge' },
        { label: '覆盖', value: 'cover' },
      ],
    },
    required: true,
  },
  {
    field: 'slot',
    component: 'Input',
    label: '导出的实体',
    slot: 'CollapseSlot',
    colProps: { span: 24 },
  },
];
