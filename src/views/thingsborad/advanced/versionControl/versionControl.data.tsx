import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Icon } from '/@/components/Icon';
import { useMessage } from '/@/hooks/web/useMessage';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import { Button } from 'ant-design-vue';
import { StyleValue } from 'vue';

export const versionControlTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createTime',
  },

  {
    title: '版本Id',
    width: 100,
    dataIndex: 'versionId',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };
      return (
        <div v-show={options.value != ''} style={{ display: 'flex', justifyContent: 'center' }}>
          <p style={style}>{options.value}</p>
          {/* <span>{options.value}</span> */}
          <Button onClick={copy} type="text" size="small" shape="circle">
            <Icon icon="ant-design:copy-outlined"></Icon>
          </Button>
        </div>
      );
    },
  },
  {
    title: '版本名称',
    width: 200,
    dataIndex: 'versionName',
  },
  {
    title: '作者',
    width: 300,
    dataIndex: 'author',
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'branch',
    label: '',
    component: 'Select',
    componentProps: {
      allowClear: false,
      placeholder: '选择分支*',
      options: [
        { label: 'master', value: 'master' },
        { label: 'main', value: 'value' },
      ],
    },
    colProps: {
      span: 8,
    },
    defaultValue: 'main',
  },
  {
    field: 'version',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '搜索版本',
    },
    colProps: {
      span: 8,
    },
  },
];
