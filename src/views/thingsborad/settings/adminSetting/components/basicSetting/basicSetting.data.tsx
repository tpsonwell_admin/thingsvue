import { FormSchema } from '/@/components/Form/index';
import { ref } from 'vue';
export const isConnectEdit = ref<boolean>(false);
function setIsConnectEdit() {
  isConnectEdit.value = true;
}
export const httpForm: FormSchema[] = [
  {
    field: 'http.enabled',
    component: 'Switch',
    label: '',
    slot: 'http',
    defaultValue: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'http.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['http.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'http.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['http.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'https.enabled',
    component: 'Switch',
    label: '',
    slot: 'https',
    defaultValue: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'https.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['https.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'https.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['https.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
];

export const mpttForm: FormSchema[] = [
  {
    field: 'mqtt.enabled',
    component: 'Switch',
    label: '',
    slot: 'mqtt',
    defaultValue: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'mqtt.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['mqtt.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'mqtt.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['mqtt.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'mqtts.enabled',
    component: 'Switch',
    label: '',
    slot: 'mqtts',
    defaultValue: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'mqtts.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['mqtts.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'mqtts.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['mqtts.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
];

export const coapForm: FormSchema[] = [
  {
    field: 'coap.enabled',
    component: 'Switch',
    label: '',
    slot: 'coap',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'coap.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['coap.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'coap.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['coap.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'coaps.enabled',
    component: 'Switch',
    label: '',
    slot: 'coaps',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'coaps.host',
    component: 'Input',
    label: 'Host',
    colProps: {
      span: 12,
    },
    dynamicDisabled: (value) => {
      return !value.model['coaps.enabled'];
    },
    componentProps: {
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
  {
    field: 'coaps.port',
    component: 'InputNumber',
    label: 'Port',
    colProps: {
      span: 12,
    },
    rules: [{ pattern: /^[1-9]\d{0,4}$/, message: '端口必须为正整数' }],
    dynamicDisabled: (value) => {
      return !value.model['coaps.enabled'];
    },
    componentProps: {
      max: 65535,
      min: 1,
      onChange: () => {
        setIsConnectEdit();
      },
    },
  },
];
