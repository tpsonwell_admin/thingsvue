import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { DescItem } from '/@/components/Description/index';
export const getBasicColumns: BasicColumn[] = [
  {
    title: '名称',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: '分区',
    dataIndex: 'partitions',
    sorter: true,
  },
  {
    title: '提交策略',
    dataIndex: 'submitStrategy',
    sorter: true,
  },
  {
    title: '处理策略',
    dataIndex: 'processingStrategy',
    sorter: true,
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'textSearch',
    label: '',
    componentProps: {
      placeholder: '队列名称',
    },
    component: 'Input',
  },
];

export const queuesForm: FormSchema[] = [
  {
    field: 'name',
    label: '名称',
    component: 'Input',
    required: true,
    slot: 'name',
    colProps: {
      span: 24,
    },
    rules: [
      {
        pattern: /^[a-zA-Z0-9_.\-]+$/,
        message: '队列名称不能包含ASCII字母数字以外的字符, `.`, `_` 和 `-`等。',
      },
    ],
  },
  {
    field: 'submitStrategy',
    label: '',
    component: 'Input',
    slot: 'submitStrategy',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'processingStrategy',
    label: '',
    component: 'Input',
    slot: 'processingStrategy',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'pollingSetting',
    label: '',
    component: 'Input',
    slot: 'pollingSetting',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'additionalInfo',
    label: '',
    component: 'Input',
    slot: 'additionalInfo',
    colProps: {
      span: 24,
    },
  },
];

export const submitStrategyForm: FormSchema[] = [
  {
    field: 'type',
    label: '策略类型',
    required: true,
    component: 'RadioGroup',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        {
          label: '按发起者顺序处理',
          value: 'SEQUENTIAL_BY_ORIGINATOR',
        },
        {
          label: '按租户顺序处理',
          value: 'SEQUENTIAL_BY_TENANT',
        },
        {
          label: '顺序处理',
          value: 'SEQUENTIAL',
        },
        {
          label: '突发处理',
          value: 'BURST',
        },
        {
          label: '批量处理',
          value: 'BATCH',
        },
      ],
      style: {
        display: 'flex',
        flexDirection: 'column',
      },
    },
  },
  {
    field: 'batchSize',
    label: '批量处理大小',
    component: 'InputNumber',
    required: true,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (value.model.type === 'BATCH') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      min: 1,
    },
  },
];

export const processingStrategyForm: FormSchema[] = [
  {
    field: 'type',
    label: '处理类型',
    required: true,
    component: 'RadioGroup',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        {
          label: '失败与超时重试',
          value: 'RETRY_FAILED_AND_TIMED_OUT',
        },
        {
          label: '跳过所有失败',
          value: 'SKIP_ALL_FAILURES',
        },
        {
          label: '跳过所有失败和超时',
          value: 'SKIP_ALL_FAILURES_AND_TIMED_OUT',
        },
        {
          label: '全部重试',
          value: 'RETRY_ALL',
        },
        {
          label: '失败重试',
          value: 'RETRY_FAILED',
        },
        {
          label: '超时重试',
          value: 'RETRY_TIMED_OUT',
        },
      ],
      style: {
        display: 'flex',
        flexDirection: 'column',
      },
    },
  },
  {
    field: 'retries',
    label: '重试次数(0-无限制)',
    component: 'InputNumber',
    defaultValue: 3,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: 0,
    },
  },
  {
    field: 'failurePercentage',
    label: '跳过重试的失败消息百分比',
    component: 'InputNumber',
    defaultValue: 0,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: 0,
      max: 100,
    },
  },
  {
    field: 'pauseBetweenRetries',
    label: '重试间隔(秒)',
    component: 'InputNumber',
    defaultValue: 3,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: 0,
    },
  },
  {
    field: 'maxPauseBetweenRetries',
    label: '最大重试间隔(秒)',
    component: 'InputNumber',
    defaultValue: 3,
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      min: 0,
    },
  },
];

export const pollingSettingForm: FormSchema[] = [
  {
    field: 'pollInterval',
    label: '轮询间隔',
    component: 'InputNumber',
    defaultValue: 25,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
    },
  },
  {
    field: 'partitions',
    label: '分区',
    component: 'InputNumber',
    defaultValue: 10,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
    },
  },
  {
    field: 'consumerPerPartition',
    label: ' ',
    component: 'Checkbox',
    required: true,
    defaultValue: false,
    slot: 'consumerPerPartition',
    colProps: {
      span: 12,
    },
  },
  {
    field: 'packProcessingTimeout',
    label: '处理超时(毫秒)',
    component: 'InputNumber',
    defaultValue: 2000,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 1,
    },
  },
];
export const additionalInfoForm: FormSchema[] = [
  {
    field: 'customProperties',
    label: 'Custom properties',
    component: 'Input',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    label: '说明',
    component: 'InputTextArea',
    colProps: {
      span: 24,
    },
  },
];
export const detailSchema: DescItem[] = [
  {
    field: 'name',
    label: '名称',
  },
  {
    field: 'submit',
    label: '策略类型',
  },
  {
    field: 'batchSize',
    label: '批量处理大小',
  },
  {
    field: 'processing',
    label: '处理类型',
  },
  {
    field: 'retries',
    label: '重试次数',
  },
  {
    field: 'failurePercentage',
    label: '跳过重试的失败消息消息的百分比',
  },
  {
    field: 'pauseBetweenRetries',
    label: '重试间隔',
  },
  {
    field: 'maxPauseBetweenRetries',
    label: '最大重试间隔',
  },
  {
    field: 'pollInterval',
    label: '轮询间隔',
  },
  {
    field: 'partitions',
    label: '分区',
  },
  {
    field: 'consumerPerPartition',
    label: '每个分区消费者单独轮询消息',
    render: (value) => {
      return value ? '是' : '否';
    },
  },
  {
    field: 'packProcessingTimeout',
    label: '处理超时',
  },
  {
    field: 'customProperties',
    label: 'Custom properties',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
  {
    field: 'description',
    label: '说明',
    render: (val: any) => {
      return <span>{val == '' || val == null ? '— —' : val}</span>;
    },
  },
];
export const keyWordsTranslation = {
  RETRY_TIMED_OUT: '超时重试',
  RETRY_FAILED: '失败重试',
  RETRY_ALL: '全部重试',
  SKIP_ALL_FAILURES_AND_TIMED_OUT: '跳过所有失败和超时',
  SKIP_ALL_FAILURES: '跳过所有失败',
  RETRY_FAILED_AND_TIMED_OUT: '失败与超时重试',
  SEQUENTIAL_BY_ORIGINATOR: '按发起者顺序处理',
  SEQUENTIAL_BY_TENANT: '按租户顺序处理',
  SEQUENTIAL: '顺序处理',
  BURST: '突发处理',
  BATCH: '批量处理',
};
