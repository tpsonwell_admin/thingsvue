import { FormSchema } from '/@/components/Form/index';
import { ref } from 'vue';
export const isSettingEdit = ref<boolean>(false);
function setIsSettingEdit() {
  isSettingEdit.value = true;
}
export const SMSForm: FormSchema[] = [
  {
    field: 'type',
    component: 'Select',
    label: 'SMS服务商类型',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        { value: 'AWS_SNS', label: '亚马逊社交网站' },
        { value: 'TWILIO', label: 'Twilio' },
        { value: 'SMPP', label: 'SMPP' },
      ],
      allowClear: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'accessKeyId',
    component: 'Input',
    label: 'AWS访问密钥ID',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'AWS_SNS') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'secretAccessKey',
    component: 'InputPassword',
    label: 'AWS秘密访问密钥',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'AWS_SNS') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'region',
    component: 'Input',
    label: 'AWS地区',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'AWS_SNS') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'numberFrom',
    component: 'Input',
    label: '发送方电话号码',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'TWILIO') {
        return true;
      } else {
        return false;
      }
    },
    rules: [
      {
        pattern: /^\+[1-9]\d{1,14}$|^(MG|PN).*$/,
        message: '无效的电话号码，应该使用E.164格式电话号码/电话号码的SID/消息服务的SID',
      },
    ],
  },
  {
    field: 'accountSid',
    component: 'Input',
    label: 'TWilio账户SID',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'TWILIO') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'accountToken',
    component: 'InputPassword',
    label: 'TWilio账户令牌',
    required: true,
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'TWILIO') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'protocolVersion',
    component: 'Select',
    label: 'SMPP版本',
    colProps: {
      span: 6,
    },
    defaultValue: '3.3',
    componentProps: {
      options: [
        { value: '3.3', label: '3.3' },
        { value: '3.4', label: '3.4' },
      ],
      allowClear: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'host',
    component: 'Input',
    label: 'SMPP主机',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'port',
    component: 'InputNumber',
    label: 'SMPP端口',
    required: true,
    colProps: {
      span: 6,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'systemId',
    component: 'Input',
    label: '系统ID',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'password',
    component: 'InputPassword',
    label: '密码',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    ifShow: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'TypeSettings',
    component: 'Input',
    label: '',
    slot: 'TypeSettings',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
    show: (value) => {
      if (value.model.type === 'SMPP') {
        return true;
      } else {
        return false;
      }
    },
  },
];

export const typeSetting: FormSchema[] = [
  {
    field: 'systemType',
    component: 'Input',
    label: '系统类型',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'bindType',
    component: 'Select',
    label: '绑定类型',
    colProps: {
      span: 24,
    },
    defaultValue: 'TX',
    componentProps: {
      options: [
        { value: 'TX', label: '发送端' },
        { value: 'RX', label: '接收端' },
        { value: 'TRX', label: '发送/接收' },
      ],
      allowClear: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'serviceType',
    component: 'Input',
    label: '服务类型',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
];
export const SourceSettings: FormSchema[] = [
  {
    field: 'sourceAddress',
    component: 'Input',
    label: '源地址',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'sourceTon',
    component: 'Select',
    label: '源TON(号码类型)',
    colProps: {
      span: 24,
    },
    defaultValue: 5,
    componentProps: {
      options: [
        { value: 0, label: '未知' },
        { value: 1, label: '国际' },
        { value: 2, label: '国内' },
        { value: 3, label: '网络特定' },
        { value: 4, label: '用户号码' },
        { value: 5, label: '字母数字' },
        { value: 6, label: '简写' },
      ],
      allowClear: false,
      listHeight: 160,
      virtual: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'sourceNpi',
    component: 'Select',
    label: '源NPI(编号方案标识)',
    colProps: {
      span: 24,
    },
    defaultValue: 0,
    componentProps: {
      options: [
        { value: 0, label: '0-未知' },
        { value: 1, label: '1-ISDN/电话编号方案(E163/E164)' },
        { value: 3, label: '3-数据编号方案(X.121)' },
        { value: 4, label: '4-电报编号方案(F.69)' },
        { value: 6, label: '6-移动设备编号方案(E.212)' },
        { value: 8, label: '国家编号方案' },
        { value: 9, label: '私有编号方案' },
        { value: 10, label: 'ERMES编号方案(ETSI DE/PS 3 01-3)' },
        { value: 13, label: '互联网(IP)' },
        { value: 18, label: '-WAP 客户端标识(由WAP论坛定义)' },
      ],
      allowClear: false,
      listHeight: 160,
      virtual: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
];
export const GoalSetting: FormSchema[] = [
  {
    field: 'destinationTon',
    component: 'Select',
    label: '目标TON(号码类型)',
    colProps: {
      span: 24,
    },
    defaultValue: 5,
    componentProps: {
      options: [
        { value: 0, label: '未知' },
        { value: 1, label: '国际' },
        { value: 2, label: '国内' },
        { value: 3, label: '网络特定' },
        { value: 4, label: '用户号码' },
        { value: 5, label: '字母数字' },
        { value: 6, label: '简写' },
      ],
      allowClear: false,
      listHeight: 160,
      virtual: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'destinationNpi',
    component: 'Select',
    label: '目标NPI(编号方案标识)',
    colProps: {
      span: 24,
    },
    defaultValue: 0,
    componentProps: {
      options: [
        { value: 0, label: '0-未知' },
        { value: 1, label: '1-ISDN/电话编号方案(E163/E164)' },
        { value: 3, label: '3-数据编号方案(X.121)' },
        { value: 4, label: '4-电报编号方案(F.69)' },
        { value: 6, label: '6-移动设备编号方案(E.212)' },
        { value: 8, label: '国家编号方案' },
        { value: 9, label: '私有编号方案' },
        { value: 10, label: 'ERMES编号方案(ETSI DE/PS 3 01-3)' },
        { value: 13, label: '互联网(IP)' },
        { value: 18, label: '-WAP 客户端标识(由WAP论坛定义)' },
      ],
      allowClear: false,
      listHeight: 160,
      virtual: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
];
export const OtherSettings: FormSchema[] = [
  {
    field: 'addressRange',
    component: 'Input',
    label: '地址范围',
    colProps: {
      span: 24,
    },
    componentProps: {
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
  {
    field: 'codingScheme',
    component: 'Select',
    label: '编码方案',
    colProps: {
      span: 24,
    },
    defaultValue: 0,
    componentProps: {
      options: [
        { value: 0, label: '0 - SMSC 默认字母表 (ASCII 码用于短码和长码，GSM 编码用于免费电话)' },
        {
          value: 1,
          label: '1 - IA5 码 (ASCII 码用于短码和长码，Latin 9 用于免费电话 (ISO-8859-9))',
        },
        { value: 2, label: '2 - 八进制未指定 (8 位二进制)' },
        { value: 3, label: '3 - Latin 1 (ISO-8859-1)' },
        { value: 4, label: '4 - 八进制未指定 (8 位二进制)' },
        { value: 5, label: '5 - JIS (X 0208-1990)' },
        { value: 6, label: '6 - Cyrillic (ISO-8859-5)' },
        { value: 7, label: '7 - Latin/Hebrew (ISO-8859-8)' },
        { value: 8, label: '8 - UCS2/UTF-16 (ISO/IEC-10646)' },
        { value: 9, label: '9 - 图标编码' },
        { value: 10, label: '10 - 音乐编码 (ISO-2022-JP)' },
        { value: 13, label: '13 - 扩展汉字 JIS (X 0212-1990)' },
        { value: 14, label: '14 - 韩文图形字符集 (KS C 5601/KS X 1001)' },
      ],
      allowClear: false,
      listHeight: 160,
      virtual: false,
      onChange: () => {
        setIsSettingEdit();
      },
    },
  },
];

export const MessageForm: FormSchema[] = [
  {
    field: 'numberTo',
    component: 'Input',
    label: '电话号码至',
    required: true,
    colProps: {
      span: 24,
    },
    rules: [{ pattern: /^\+[1-9]\d{1,14}$/, message: '手机号码无效或不存在' }],
  },
  {
    field: 'message',
    component: 'InputTextArea',
    label: '短信',
    required: true,
    colProps: {
      span: 24,
    },
  },
];
