import { FormSchema } from '/@/components/Form';

//导航栏类型定义
export interface navBar {
  icon: string;
  name: string;
  key: string;
}

// 导航栏数据列表
export const navBarList: navBar[] = [
  {
    icon: 'material-symbols:inbox-rounded',
    name: '首页',
    key: 'home',
  },
  {
    icon: 'mdi:account-multiple',
    name: '通知',
    key: 'notice',
  },
  {
    icon: 'mdi:account-multiple',
    name: '仓库',
    key: 'repository',
  },
  {
    icon: 'mdi:sort-variant',
    name: '自动提交',
    key: 'autoCommit',
  },
];
export const autoCommitSchemas: FormSchema[] = [
  {
    field: 'autoCommit',
    label: '自动提交设置',
    component: 'Input',
    slot: 'autoCommit',
    colProps: {
      style: {
        border: '1px solid black',
        borderRadius: '12px',
        padding: '10px',
      },
    },
  },
];

export const entityType = [
  { label: '设备', value: 'DEVICE' },
  { label: '资产', value: 'ASSET' },
  { label: '实体视图', value: 'ENTITY_VIEW' },
  { label: '租户', value: 'TENANT' },
  { label: '客户', value: 'CUSTOMER' },
  { label: '用户', value: 'USER' },
  { label: '仪表盘', value: 'DASHBOARD' },
  { label: '规则链', value: 'RULE_CHAIN' },
  { label: '规则节点', value: 'RULE_NODE' },
];
