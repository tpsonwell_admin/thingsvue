export function useFullScreen(dom: any, fn: any) {
  let fullDom: HTMLElement;
  let originDom: HTMLElement;
  let parentNode: HTMLElement;

  const enter = () => {
    // 进入全屏
    if (!fullDom) {
      originDom = dom.value;
      parentNode = originDom.parentNode as HTMLElement;
      fullDom = document.createElement('div');
      fullDom.style.width = '100vw';
      fullDom.style.height = '100vh';
      fullDom.style.position = 'fixed';
      fullDom.style.top = '0';
      fullDom.style.left = '0';
      fullDom.style.zIndex = '9999';
      fullDom.style.backgroundColor = '#fff';
      fullDom.style.padding = '16px';
      const closeBtn = document.createElement('button');
      closeBtn.className = 'closeBtn';
      closeBtn.innerText = '退出全屏';
      closeBtn.onclick = () => {
        exit(); // 退出全屏
      };
      fullDom.appendChild(closeBtn);
    }
    originDom.style.width = '100%';
    originDom.style.height = '95%';
    fullDom.appendChild(originDom);
    document.body.appendChild(fullDom);
    fn();
  };

  const exit = () => {
    fullDom = document.body.removeChild(fullDom);
    parentNode.appendChild(originDom);
    originDom.style.width = '100%';
    originDom.style.height = '';
    fn();
  };
  return {
    enter,
  };
}
