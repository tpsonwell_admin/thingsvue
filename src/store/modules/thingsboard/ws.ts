import { store } from '/@/store';
import { defineStore } from 'pinia';

interface SystemState {
  cmdId: number;
}

export const useWsStore = defineStore({
  id: 'app-ws',
  state: (): SystemState => ({
    cmdId: 0,
  }),
  getters: {
    getCmdId(state): number {
      return state.cmdId;
    },
  },
  actions: {
    setCmdId() {
      this.cmdId++;
    },
  },
});

export function useWsStoreWithOut() {
  return useWsStore(store);
}
