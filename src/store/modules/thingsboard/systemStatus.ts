import { store } from '/@/store';
import { defineStore } from 'pinia';

interface SystemState {
  isDataChanged: Boolean;
}

export const useSystemStatusStore = defineStore({
  id: 'app-status',
  state: (): SystemState => ({
    isDataChanged: false,
  }),
  getters: {
    getIsDataChanged(state): boolean {
      return !!state.isDataChanged;
    },
  },
  actions: {
    setIsDataChanged(value: boolean) {
      this.isDataChanged = value;
    },
  },
});

export function useSystemStatusStoreWithOut() {
  return useSystemStatusStore(store);
}
