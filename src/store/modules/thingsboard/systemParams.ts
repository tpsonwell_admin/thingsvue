import { store } from '/@/store';
import { defineStore } from 'pinia';
import { getSystemParams } from '/@/api/thingsborad/settings/settings';

interface SystemState {
  hasRepository: Boolean;
}

export const useSystemStore = defineStore({
  id: 'app-system',
  state: (): SystemState => ({
    hasRepository: Boolean(localStorage.getItem('hasRepository')),
  }),
  getters: {
    getHasRepository(state): boolean {
      return !!state.hasRepository;
    },
  },
  actions: {
    async setHasRepository() {
      const systemParams = await getSystemParams();
      localStorage.setItem('hasRepository', systemParams.hasRepository);
      this.hasRepository = systemParams.hasRepository;
    },
  },
});

export function useSystemStoreWithOut() {
  return useSystemStore(store);
}
