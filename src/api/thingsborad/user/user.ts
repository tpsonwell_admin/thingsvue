import { defHttp } from '/@/utils/http/axios';
enum Api {
  // 用户信息
  USER_INFO = '/api/user',
  // 退出登录
  LOGIN_OUT = '/api/auth/logout',
  NO_AUTHO = '/api/noauth/oauth2Clients',
}

export const getUserInfo = (params: string) =>
  defHttp.get<any>({ url: `${Api.USER_INFO}/${params}` });

export const loginOut = () => defHttp.post<any>({ url: Api.LOGIN_OUT });

export const NO_AUTHO = (params: string) =>
  defHttp.post<any>({ url: `${Api.NO_AUTHO}?platform=${params}` });
