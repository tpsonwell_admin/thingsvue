import { edgeParams } from './model/edgeModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  TENANT_EDGE_INFOS = '/api/tenant/edgeInfos',
  EDGE_TYPES = '/api/edge/types',
  ADD_EDGE = '/api/edge',
  EDGE_INSTRUCTIONS = '/api/edge/instructions',
  EDGE_INFO = '/api/edge/info',
  CUSTOMER_PUBLIC_EDGE = '/api/customer/public/edge',
  CUSTOMER_EDGE = '/api/customer/edge',
  CUSTOMERS_LIST = '/api/customers',
  TO_THE_CUSTOMER = '/api/customer',
}
export const getEdgeList = (params: edgeParams) =>
  defHttp.get<any>({ url: Api.TENANT_EDGE_INFOS, params });

export const getEdgeTypeList = () => defHttp.get<any>({ url: Api.EDGE_TYPES });

export const addTheEdge = (data: any) => defHttp.post<any>({ url: Api.ADD_EDGE, data });

export const getTheEdgeInfo = (id: any) =>
  defHttp.get<any>({ url: Api.EDGE_INSTRUCTIONS + `/${id}` });

export const deleteTheEdge = (id: any) => defHttp.delete<any>({ url: Api.ADD_EDGE + `/${id}` });

export const getTheEdgeData = (id: any) => defHttp.get<any>({ url: Api.EDGE_INFO + `/${id}` });

export const isPrivate = (id: string) =>
  defHttp.post<any>({ url: Api.CUSTOMER_PUBLIC_EDGE + `/${id}` });

export const isOpen = (id: string) => defHttp.delete<any>({ url: Api.CUSTOMER_EDGE + `/${id}` });

export const getCustomersList = (params: edgeParams) =>
  defHttp.get<any>({ url: Api.CUSTOMERS_LIST, params });

export const customerToEdge = (data: any) =>
  defHttp.post<any>({ url: Api.TO_THE_CUSTOMER + `/${data.id}`, data: data.data });

export const getCustomerEdge = (data: any) =>
  defHttp.get<any>({ url: Api.TO_THE_CUSTOMER + `/${data.id}/edgeInfos`, params: data.params });
