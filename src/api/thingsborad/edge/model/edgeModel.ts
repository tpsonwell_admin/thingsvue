import { BasicPageParams } from '/@/api/model/baseModel';

export type edgeParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  type?: string;
  textSearch?: string;
};
