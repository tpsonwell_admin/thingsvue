import { defHttp } from '/@/utils/http/axios';
enum Api {
  OAUTH2_SETTINGS = '/api/oauth2/config',
}
export const getOauth2Setting = () => defHttp.get<any>({ url: Api.OAUTH2_SETTINGS });
export const postOauth2Setting = (params: any) =>
  defHttp.post<any>({ url: Api.OAUTH2_SETTINGS, params });
