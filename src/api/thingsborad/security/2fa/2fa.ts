import { defHttp } from '/@/utils/http/axios';
enum Api {
  TWOFA_SETTINGS = '/api/2fa/settings',
}
export const get2faSetting = () => defHttp.get<any>({ url: Api.TWOFA_SETTINGS });
export const post2faSetting = (params: any) =>
  defHttp.post<any>({ url: Api.TWOFA_SETTINGS, params });
