import { BasicPageParams } from '/@/api/model/baseModel';

export type securityParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  startTime?: any;
  endTime?: any;
  textSearch?: string;
};
