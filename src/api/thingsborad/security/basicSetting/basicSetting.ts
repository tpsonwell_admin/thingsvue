import { defHttp } from '/@/utils/http/axios';
enum Api {
  ADMIN_SECURITYSETTINGS = '/api/admin/securitySettings',
  ADMIN_JWTSETTINGS = '/api/admin/jwtSettings',
}
export const getSecuritySetting = () => defHttp.get<any>({ url: Api.ADMIN_SECURITYSETTINGS });
export const postSecuritySetting = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_SECURITYSETTINGS, params });
export const getJWTSetting = () => defHttp.get<any>({ url: Api.ADMIN_JWTSETTINGS });
export const postJWTSetting = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_JWTSETTINGS, params });
