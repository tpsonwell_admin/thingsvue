import { defHttp } from '/@/utils/http/axios';
import { securityParams } from './model/securityModel';
enum Api {
  AUDIT_LOGS = '/api/audit/logs',
}
//
export const getSecurityList = (params: securityParams) =>
  defHttp.get<any>({ url: Api.AUDIT_LOGS, params });
export const getSecurityListById = (params: any) =>
  defHttp.get<any>({
    url: Api.AUDIT_LOGS + `/${params.route}/${params.id.entityType}/${params.id.id}`,
    params: params.params,
  });
