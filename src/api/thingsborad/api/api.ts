import { defHttp } from '/@/utils/http/axios';
enum Api {
  FIND = '/api/entitiesQuery/find',
}

export const find = (params: any) => defHttp.post<any>({ url: Api.FIND, params });
