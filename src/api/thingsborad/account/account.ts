import { defHttp } from '/@/utils/http/axios';
enum Api {
  TENANTDASHBOARD = '/api/tenant/dashboards',
  CUSTOMERDASHBOARD = '/api/customer/',
  POSTPROFILE = '/api/user?sendActivationMail=false',
  PASSWORDPOLICY = '/api/noauth/userPasswordPolicy',
  CHANGEPASSWORD = '/api/auth/changePassword',
  NOTIFICATION = '/api/notification/settings/user',
}

export const findDashboard = (params: any) =>
  defHttp.get<any>({ url: Api.TENANTDASHBOARD, params });

export const findCusDashboard = (id: string, params: any) =>
  defHttp.get<any>({ url: Api.CUSTOMERDASHBOARD + id + '/dashboards', params });

export const postProfile = (params: any) => defHttp.post<any>({ url: Api.POSTPROFILE, params });

export const getPasswordPolicy = () => defHttp.get<any>({ url: Api.PASSWORDPOLICY });

export const changePassword = (params: any) =>
  defHttp.post<any>({ url: Api.CHANGEPASSWORD, params });

export const getNotification = () => defHttp.get<any>({ url: Api.NOTIFICATION });
export const postNotification = (params: any) =>
  defHttp.post<any>({ url: Api.NOTIFICATION, params });
