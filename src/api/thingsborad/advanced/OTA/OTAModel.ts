import { BasicPageParams, BasicResponse } from '/@/api/model/baseModel';

export interface BasicOptions {
  label: string;
  value: string;
}
// OTA表格请求参数
export type OTATableParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: any;
};
// OTA包响应参数
export interface OTAPackageItem {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  deviceProfileId: {
    entityType: string;
    id: string;
  };
  type: string;
  title: string;
  version: string;
  tag: string;
  url: string;
  hasData: boolean;
  fileName: string;
  contentType: string;
  checksumAlgorithm: string;
  checksum: string;
  dataSize: number;
}

// 添加包请求参数
export interface OTAPackageAddParams {
  title: string;
  version: string;
  tag: string;
  type: string;
  deviceProfileId: {
    entityType: string;
    id: string;
  };
  checksumAlgorithm?: string;
  checksum?: string | null;
  url?: string;
  isURL: boolean;
  additionalInfo: {
    description: string;
  };
}
export type OTAPackageList = BasicResponse<OTAPackageItem>;
