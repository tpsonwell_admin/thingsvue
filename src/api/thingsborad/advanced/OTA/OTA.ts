import { OTATableParams, OTAPackageList, OTAPackageAddParams, OTAPackageItem } from './OTAModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  // ota包
  otaPackages = '/api/otaPackages',
  // 包相关操作
  otaPackage = '/api/otaPackage',
}
// 获取OTA包
export const otaPackages = (params: OTATableParams) =>
  defHttp.get<OTAPackageList>({ url: Api.otaPackages, params });
// 添加包
export const addPackage = (params: OTAPackageAddParams) =>
  defHttp.post<OTAPackageItem>({ url: Api.otaPackage, params });
// 上传包文件
export const uploadFile = (params: any) =>
  defHttp.post<OTAPackageList>(
    {
      url: `${Api.otaPackage}/${params.id}?checksumAlgorithm=${params.checksumAlgorithm}`,
      data: params.file,
    },
    { formatDate: true },
  );
// ota包info
export const otaPackageInfo = (params: string) =>
  defHttp.get<OTAPackageItem>({ url: `${Api.otaPackage}/${params}` });
// 更新包信息
export const updatePackage = (params: OTAPackageItem) =>
  defHttp.post<OTAPackageItem>({ url: Api.otaPackage, params });
// 下载包
export const downloadPackage = (params: string) =>
  defHttp.get<any>({ url: `${Api.otaPackage}/${params}/download`, responseType: 'blob' });
// 删除包
export const deletePackage = (params: string) =>
  defHttp.delete<any>({ url: `${Api.otaPackage}/${params}` });
