import { BasicPageParams, BasicResponse } from '/@/api/model/baseModel';
export interface BasicOptions {
  label: string;
  value: string;
}
// 请求参数
export type versionTableParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
  branch: string;
};

// 版本控制表格数据data项
export interface versionTableListDataItem {
  timestamp: number;
  id: string;
  name: string;
  author: string;
}

// 创建实体版本请求参数
export interface versionTableCreateParams {
  branch: string;
  versionName: string;
  syncStrategy: string;
  entityTypes: object;
  type: string;
}
// 还原版本
export interface versionTableRollbackParams {
  versionId: string;
  entityTypes: object;
  type: string;
}
export type versionTableList = BasicResponse<versionTableListDataItem>;
