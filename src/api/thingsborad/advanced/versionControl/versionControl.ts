import { defHttp } from '/@/utils/http/axios';
import {
  versionTableParams,
  versionTableList,
  versionTableCreateParams,
  versionTableRollbackParams,
} from './versionControlModel';
enum Api {
  // 版本控制表格数据
  TABLE_LIST = '/api/entities/vc/version',
  // 创建实体版本
  createEntityVersion = '/api/entities/vc/version',
  // 版本创建请求状态
  getVersionCreateRequestStatus = '/api/entities/vc/version',
  // 还原版本
  restoreVersion = '/api/entities/vc/entity',
  // 还原版本请求状态
  getRestoreVersionStatus = '/api/entities/vc/entity',
  // branch分支选择
  branchSelect = '/api/entities/vc/branches',
}
// 获取表格数据
export const versionTable = (params: versionTableParams) =>
  defHttp.get<versionTableList>({ url: Api.TABLE_LIST, params });
// 获取单实体数据
export const entityVersionTable = (params: any) =>
  defHttp.get<versionTableList>({
    url: `${Api.TABLE_LIST}/${params.entityType}/${params.entityId}`,
    params: params.pagination,
  });
// 创建实体版本
export const createEntityVersion = (params: versionTableCreateParams) =>
  defHttp.post<string>({ url: Api.createEntityVersion, params });
// 版本创建请求状态
export const getVersionCreateRequestStatus = (params: string) =>
  defHttp.get<any>({ url: `${Api.getVersionCreateRequestStatus}/${params}/status` });
// 还原版本
export const restoreVersion = (params: versionTableRollbackParams) =>
  defHttp.post<any>({ url: Api.restoreVersion, params });
// 还原版本请求状态
export const getRestoreVersionStatus = (params: string) =>
  defHttp.get<any>({ url: `${Api.getRestoreVersionStatus}/${params}/status` });
// 获取分支
export const getBranches = () => defHttp.get<any>({ url: Api.branchSelect });
