import { BasicPageParams } from '/@/api/model/baseModel';

export type ruleParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  type: string;
  textSearch?: string;
};
