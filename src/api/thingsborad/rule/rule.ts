import { ruleParams } from './model/ruleModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  RULECHAINS_LIST = '/api/ruleChains',
  DELETE_RULECHAIN = '/api/ruleChain',
  AUTO_ASSIGN_TO_EDGE_RULE_CHAINS = '/api/ruleChain/autoAssignToEdgeRuleChains',
  EDGE_LIST = '/api/edge',
}

export const getRuleChainsList = (params: ruleParams) =>
  defHttp.get<any>({ url: Api.RULECHAINS_LIST, params });

export const deleteTheRule = (id: any) =>
  defHttp.delete<any>({ url: Api.DELETE_RULECHAIN + `/${id}` });

export const addTheRule = (data: any) => defHttp.post<any>({ url: Api.DELETE_RULECHAIN, data });

export const falgTheRule = (data: any) =>
  defHttp.post<any>({ url: Api.DELETE_RULECHAIN + `/${data.id.id}/root`, data });

export const getTheRuleInfo = (id: string) =>
  defHttp.get<any>({ url: Api.DELETE_RULECHAIN + `/${id}` });

export const editTheRule = (data: any) => defHttp.post<any>({ url: Api.DELETE_RULECHAIN, data });

export const getToEdgeRuleChains = () =>
  defHttp.get<any>({ url: Api.AUTO_ASSIGN_TO_EDGE_RULE_CHAINS });

export const downloadTheRule = (id: string) =>
  defHttp.get<any>({ url: Api.DELETE_RULECHAIN + `/${id}` });

export const edgeTemplateRoot = (data: any) =>
  defHttp.post<any>({
    url: Api.DELETE_RULECHAIN + `/${data.id}/edgeTemplateRoot`,
    data: data.data,
  });

export const autoAssignToEdge = (id: any) =>
  defHttp.delete<any>({ url: Api.DELETE_RULECHAIN + `/${id}/autoAssignToEdge` });

export const isAutoAssignToEdge = (id: any) =>
  defHttp.post<any>({ url: Api.DELETE_RULECHAIN + `/${id}/autoAssignToEdge` });

export const getEdgeList = (data: any) =>
  defHttp.get<any>({ url: Api.EDGE_LIST + `/${data.id}/ruleChains`, params: data.params });

