import { BasicPageParams } from '/@/api/model/baseModel';

export type dashboardParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  type: string;
  textSearch?: string;
};
