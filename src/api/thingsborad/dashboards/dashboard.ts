import { defHttp } from '/@/utils/http/axios';
import { dashboardParams } from './model/dashboardModel';

enum Api {
  TENANT_DASHBOARDS = '/api/tenant/dashboards',
  API_DASHBOARD = '/api/dashboard',
  PUBLIC_DASHBOARD = '/api/customer/public/dashboard',
  CUSTOMER = '/api/customer',
  ENTITY_KEYS = '/api/entitiesQuery/find/keys',
  CUSTOMERS_LIST = '/api/customers',
}

export const getDashboard = (params: dashboardParams) =>
  defHttp.get<any>({ url: Api.TENANT_DASHBOARDS, params });

export const apiDashboard = (id: string) => defHttp.get<any>({ url: Api.API_DASHBOARD + `/${id}` });

export const deleteDashboard = (id: string) =>
  defHttp.delete<any>({ url: Api.API_DASHBOARD + `/${id}` });

export const publicDashboard = (id: string) =>
  defHttp.post<any>({ url: Api.PUBLIC_DASHBOARD + `/${id}` });

export const privateDashboard = (id: string) =>
  defHttp.delete<any>({ url: Api.PUBLIC_DASHBOARD + `/${id}` });

export const addDashboard = (data: any) => defHttp.post<any>({ url: Api.API_DASHBOARD, data });

export const getCustomerDashboard = (data: any) =>
  defHttp.get<any>({ url: Api.CUSTOMER + `/${data.id}/dashboards`, params: data.params });

export const getEntityKeys = (params: any) =>
  defHttp.post<any>({ url: `${Api.ENTITY_KEYS}`, params: params.query, data: params.payload });

export const getTheDashboardInfo = (id: string) =>
  defHttp.get({ url: Api.API_DASHBOARD + `/${id}` });

export const editDashBoard = (data: any) => defHttp.post({ url: Api.API_DASHBOARD, data });

export const getCustomersList = (params: any) => defHttp.get({ url: Api.CUSTOMERS_LIST, params });

export const shareCustomers = (data: any) =>
  defHttp.post({ url: Api.API_DASHBOARD + `/${data.id}/customers`, data: data.data });

export const noShareCustomers = (id: any) =>
  defHttp.post({ url: Api.API_DASHBOARD + `/${id}/customers` });

export const downloadDashboard = (id: string) => defHttp.get({ url: Api.API_DASHBOARD + `/${id}` });
