import { defHttp } from '/@/utils/http/axios';
enum Api {
  USER_DASHBOARDS = '/api/user/dashboards',
  USAGE = '/api/usage',
  QUICK_LINKS = '/api/user/settings/QUICK_LINKS',
  DOC_LINKS = '/api/user/settings/DOC_LINKS',
  featuresInfo = '/api/admin/featuresInfo',
}
export const getUserDashboards = () => defHttp.get<any>({ url: Api.USER_DASHBOARDS });

export const getUsage = () => defHttp.get<any>({ url: Api.USAGE });

export const getQuickLinks = () => defHttp.get<any>({ url: Api.QUICK_LINKS });

export const saveQuikLinks = (data: any) => defHttp.put<any>({ url: Api.QUICK_LINKS, data });

export const getDocLinks = () => defHttp.get<any>({ url: Api.DOC_LINKS });

export const saveDocLinks = (data: any) => defHttp.put<any>({ url: Api.DOC_LINKS, data });

export const getFeaturesInfo = () => defHttp.get<any>({ url: Api.featuresInfo });
