import { BasicPageParams } from '/@/api/model/baseModel';

export interface saveWarehouseItem {
  repositoryUri: string;
  defaultBranch: string;
  readOnly: boolean;
  showMergeCommits: boolean;
  authMethod: string;
  username?: string;
  password?: string;
  privateKeyFileName?: string;
  privateKey?: string;
  privateKeyPassword?: string;
}
// pageSize=10&page=0&textSearch=Gateways&sortProperty=title&sortOrder=ASC
export type homeSettingParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  textSearch?: string;
};
