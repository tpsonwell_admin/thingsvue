import { defHttp } from '/@/utils/http/axios';
enum Api {
  ADMIN_GENERAL = '/api/admin/settings/general',
  ADMIN_CONNECT = '/api/admin/settings/connectivity',
  ADMIN_SETTINGS = '/api/admin/settings',
  ADMIN_SMS = '/api/admin/settings/sms',
  ADMIN_SMS_TEST = '/api/admin/settings/testSms',
  ADMIN_EMAIL = '/api/admin/settings/mail',
  ADMIN_EMAIL_TEST = '/api/admin/settings/testMail',
  ADMIN_EMAIL_OAUTH2 = '/api/admin/mail/oauth2/authorize',
  NOTICE_SETTING = '/api/notification/settings',
  QUEUES = '/api/queues',
  QUEUES_POST = '/api/queues?serviceType=TB_RULE_ENGINE',
}
export const getBasicSetting = () => defHttp.get<any>({ url: Api.ADMIN_GENERAL });
export const getConnectSetting = () => defHttp.get<any>({ url: Api.ADMIN_CONNECT });
export const postBasicSetting = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_SETTINGS, params });
export const getSlackSetting = () => defHttp.get<any>({ url: Api.NOTICE_SETTING });
export const postSlackSetting = (params: any) =>
  defHttp.post<any>({ url: Api.NOTICE_SETTING, params });
export const getSMSSetting = () => defHttp.get<any>({ url: Api.ADMIN_SMS });
export const postSMSSetting = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_SETTINGS, params });
export const sendTestSms = (params: any) => defHttp.post<any>({ url: Api.ADMIN_SMS_TEST, params });
export const getEmailSetting = () => defHttp.get<any>({ url: Api.ADMIN_EMAIL });
export const postEmailSetting = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_SETTINGS, params });
export const sendTestEmail = (params: any) =>
  defHttp.post<any>({ url: Api.ADMIN_EMAIL_TEST, params });
export const generateToken = () => defHttp.get<any>({ url: Api.ADMIN_EMAIL_OAUTH2 });
export const getQueuesList = (params: any) => defHttp.get<any>({ url: Api.QUEUES, params });
export const postQueues = (params: any) => defHttp.post<any>({ url: Api.QUEUES_POST, params });
export const getQueues = (params: any) => defHttp.get<any>({ url: Api.QUEUES, params });
export const deleteQueues = (params: any) => defHttp.delete<any>({ url: Api.QUEUES, params });
