import { defHttp } from '/@/utils/http/axios';
import { saveWarehouseItem } from './model/settingsModel';
enum Api {
  ADMIN_REPOSITORY_SETTINGS = '/api/admin/repositorySettings',
  ADMIN_REPOSITORY_SETTINGS_CHECK_ACCESS = '/api/admin/repositorySettings/checkAccess',
  AUTH_TOKEN = '/api/auth/token',
  SYSTEM_PARAMS = '/api/system/params',
  TENANT_DASHBOARDS = '/api/tenant/dashboards',
  HOME_INFO = '/api/tenant/dashboard/home/info',
  DASHBOARD_INFO = '/api/dashboard/info',
  NOTIFICATION_SETTINGS = '/api/notification/settings',
  autoCommitSettings = '/api/admin/autoCommitSettings',
  repositoryInfo = '/api/admin/repositorySettings/info',
}
export const getRepositorySettings = () => defHttp.get<any>({ url: Api.ADMIN_REPOSITORY_SETTINGS });

export const getCheckAccessSettings = (data: saveWarehouseItem) =>
  defHttp.post<any>({ url: Api.ADMIN_REPOSITORY_SETTINGS_CHECK_ACCESS, data });

export const editRepositorySettings = (data: saveWarehouseItem) =>
  defHttp.post<any>({ url: Api.ADMIN_REPOSITORY_SETTINGS, data });

export const deleteRepositorySettings = () =>
  defHttp.delete<any>({ url: Api.ADMIN_REPOSITORY_SETTINGS });

export const getSystemParams = () => defHttp.get<any>({ url: Api.SYSTEM_PARAMS });

export const getHomePageSettings = (params: any) =>
  defHttp.get<any>({ url: Api.TENANT_DASHBOARDS, params });

export const getInfoData = () => defHttp.get<any>({ url: Api.HOME_INFO });

export const saveInfoData = (data: any) => defHttp.post<any>({ url: Api.HOME_INFO, data });

export const saveTheHomeSetting = (id: any) =>
  defHttp.get<any>({ url: Api.DASHBOARD_INFO + `/${id}` });

export const getNotificationSettings = () => defHttp.get<any>({ url: Api.NOTIFICATION_SETTINGS });

export const editNotificationSettings = (data: any) =>
  defHttp.post<any>({ url: Api.NOTIFICATION_SETTINGS, data });

// get自动提交设置
export const getAutoCommitSettings = () => defHttp.get<any>({ url: Api.autoCommitSettings });

// 保存自动提交设置
export const saveAutoCommitSettings = (data) =>
  defHttp.post<any>({ url: Api.autoCommitSettings, data });

// 删除自动提交设置
export const deleteAutoCommitSettings = () => defHttp.delete<any>({ url: Api.autoCommitSettings });

// 自动提交是否存在
export const exists = () => defHttp.get<any>({ url: Api.autoCommitSettings + '/exists' });

// 仓库信息
export const getRepositoryInfo = () => defHttp.get<any>({ url: Api.repositoryInfo });
