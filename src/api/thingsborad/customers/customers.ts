import { defHttp } from '/@/utils/http/axios';
import { customersParams } from './model/customersModel';
enum Api {
  CUSTOMERS_LIST = '/api/customers',
  ADD_CUSTOMER = '/api/customer',
  DELETE_USER = '/api/user',
  NOAUTH_ACTIVATE = '/api/noauth/activate',
}

export const getCustomersList = (params: customersParams) =>
  defHttp.get<any>({ url: Api.CUSTOMERS_LIST, params });

export const addTheCustomer = (data: any) => defHttp.post<any>({ url: Api.ADD_CUSTOMER, data });

export const deleteTheCustomer = (id: any) =>
  defHttp.delete<any>({ url: Api.ADD_CUSTOMER + `/${id}` });

export const getOtherCustomerList = (params: any) =>
  defHttp.get<any>({
    url: Api.ADD_CUSTOMER + '/' + params.id + '/' + params.type,
    params: params.params,
  });

export const deleteTheUser = (id: any) => defHttp.delete<any>({ url: Api.DELETE_USER + '/' + id });

export const addTheUser = (params: any) =>
  defHttp.post<any>({ url: Api.DELETE_USER, data: params.data, params: params.params });

export const getTheUserLink = (id: any) =>
  defHttp.get<any>({ url: Api.DELETE_USER + `/${id}` + '/activationLink' });

// 获取指定用户的信息
export const getTheCustomersInfo = (id: any) =>
  defHttp.get<any>({ url: Api.ADD_CUSTOMER + `/${id}` });

export const searchTheCustomer = (params: customersParams) =>
  defHttp.get<any>({ url: Api.ADD_CUSTOMER, params });

export const getTanentUserList = (data: any) =>
  defHttp.get({ url: Api.ADD_CUSTOMER + `/${data.id}/users`, params: data.params });

//设置密码
export const setUserPassword = (data: any) =>
  defHttp.post({ url: Api.NOAUTH_ACTIVATE, params: data.params, data: data.data, timeout: 20000 });
