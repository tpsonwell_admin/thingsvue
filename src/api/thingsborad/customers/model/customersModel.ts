import { BasicPageParams } from '/@/api/model/baseModel';

export type customersParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  textSearch?: string;
};
