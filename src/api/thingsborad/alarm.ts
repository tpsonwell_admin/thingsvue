import { defHttp } from '/@/utils/http/axios';
enum Api {
  // 所有实体告警
  OPTIONS_LIST = '/api/v2/alarms',
  // 实体类告警
  ENTITY_ALARM = '/api/v2/alarm',
  // 用户列表
  USER_LIST = '/api/users/assign',
  // 分配用户
  assignUsers = '/api/alarm',
  // 取消分配用户
  unassignedUsers = '/api/alarm',
  // 告警类型列表
  ALRAM_TYPE_LIST = '/api/alarm/types',
  // 所有受托人列表
  ALL_TRUSTED_LIST = '/api/users/info',
  // 获取告警信息
  ALARM_COMMENT = '/api/alarm',
  // 发送评论
  sendComment = '/api/alarm',
  // 应答
  ackAlarm = '/api/alarm',
  // 清除
  clearAlarm = '/api/alarm',
  // 删除告警
  deleteAlarm = '/api/alarm',
  // 删除评论
  deleteComment = '/api/alarm',
}

/**
 * @description: Get sample options value
 */
// 报警分页
export const alarmsPage = (params: any) =>
  defHttp.get<any>({ url: Api.OPTIONS_LIST, params: params.pagination });
// 用户列表
export const getUsers = (params: any) =>
  defHttp.get<any>({ url: `${Api.USER_LIST}/${params.alarmId}`, params: params.pagination });
// 分配用户
export const assignUsers = (params: any) =>
  defHttp.post<any>({
    url: `${Api.assignUsers}/${params.alarmId}/assign/${params.assigneeId}`,
  });
// 取消分配
export const unassignedUsers = (params: any) =>
  defHttp.delete<any>({ url: `${Api.unassignedUsers}/${params.alarmId}/assign` });
// 告警类型列表
export const getAlarmTypeList = (params: any) =>
  defHttp.get<any>({ url: Api.ALRAM_TYPE_LIST, params });
// 所有受托人列表
export const getTrustedList = (params: any) =>
  defHttp.get<any>({ url: Api.ALL_TRUSTED_LIST, params });
// 获取评论
export const getAlarmComment = (params: any) =>
  defHttp.get<any>({
    url: `${Api.ALARM_COMMENT}/${params.alarmId}/comment`,
    params: params.pagination,
  });
// 发送评论
export const sendComment = (params: any) =>
  defHttp.post<any>({ url: `${Api.sendComment}/${params.alarmId.id}/comment`, params });
// ack
export const ackAlarm = (params: string) =>
  defHttp.post<any>({ url: `${Api.ackAlarm}/${params}/ack` });
// 清除
export const clearAlarm = (params: string) =>
  defHttp.post<any>({ url: `${Api.clearAlarm}/${params}/clear` });

// 实体类告警
export const entityAlarmsPage = (params: any) =>
  defHttp.get<any>({
    url: `${Api.ENTITY_ALARM}/${params.entityType}/${params.entityId}`,
    params: params.pagination,
  });

// 删除告警
export const deleteAlarm = (params: string) =>
  defHttp.delete<any>({ url: `${Api.deleteAlarm}/${params}` });
// 删除评论
export const deleteComment = (params: any) =>
  defHttp.delete<any>({
    url: `${Api.deleteComment}/${params.alarmId}/comment/${params.commentId}`,
  });
