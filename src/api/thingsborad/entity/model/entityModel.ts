import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
export interface BasicOptions {
  label: string;
  value: string;
}
// 设备页外层表格请求参数
export type entityTableParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: any;
};
// 设备页外层表格响应数据格式
export interface deviceInfoItem {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo?: {
    gateway: boolean;
    overwriteActivityTime: boolean;
    description: string;
  };
  tenantId?: {
    entityType: string;
    id: string;
  };
  customerId?: {
    entityType: string;
    id: string;
  };
  name: string;
  type?: string;
  label?: string;
  deviceProfileId?: {
    entityType: string;
    id: string;
  };
  deviceData?: {
    configuration: {
      type: string;
    };
    transportConfiguration: {
      type: string;
    };
  };
  firmwareId: string;
  softwareId: string;
  externalId?: {
    entityType: string;
    id: string;
  };
  customerTitle?: string;
  customerIsPublic?: boolean;
  deviceProfileName?: string;
  active?: boolean;
}
// 设备页外层表格清洗后数据格式
export interface deviceTableItem {
  createdTime: number;
  name: string;
  deviceProfileName: string;
  label: string;
  active: boolean;
  customerTitle: string;
  customerIsPublic: boolean;
  isGateway: boolean;
  id: object;
  deviceProfileId: object;
  additionalInfo: object;
}

// 设备配置列表请求参数
export type deviceProfileInfosParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
};
// 设备配置响应数据格式
export interface deviceProfileInfo {
  id: {
    entityType: string;
    id: string;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  name: string;
  image: string;
  defaultDashboardId: {
    entityType: string;
    id: string;
  };
  type: string;
  transportType: string;
}

// 软固件列表请求参数
export interface profileURLParams {
  id: string;
  obj?: {
    pageSize: number;
    page: number;
    sortProperty?: string;
    sortOrder?: string;
  };
}

// 添加设备请求基础格式
export interface addBasicDeviceParams {
  name: string;
  label?: string;
  deviceProfileId: {
    entityType: string;
    id: string;
  };
  additionalInfo: {
    gateway: boolean;
    overwriteActivityTime: boolean;
    description: string;
  };
  customerId: {
    entityType: string;
    id: string;
  } | null;
}

// 添加设备请求带凭证格式
export interface addDeviceWithCredentialsParams {
  device: addBasicDeviceParams;
  credentials: {
    credentialsType: string;
    credentialsId: string;
    credentialsValue: string;
  };
}

// 对应设备凭证响应数据格式
export interface deviceCredentialsInfo {
  id: {
    id: string;
  };
  createdTime: number;
  deviceId: {
    entityType: string;
    id: string;
  };
  credentialsType: string;
  credentialsId: string;
  credentialsValue: string;
}

// 设备属性表格请求参数
export type attributesParams = {
  entityType: string;
  entityId?: string;
  scope?: string;
  key?: string;
  responseDataType?: string;
  sortProperty?: string;
  sortOrder?: string;
};
// 属性响应数据
export interface attributesResponse {
  id: string;
  type?: string;
  lastUpdateTs: number;
  key: string;
  value: string | number | boolean | JSON;
}

// 保存属性请求参数
export interface saveAttributesParams {
  entityType: string;
  entityId?: string;
  scope: string;
  key: string;
  value?: any;
}
// 保存遥测请求参数
export interface saveTelemetryParams {
  entityType: string;
  entityId?: string;
  key: string;
  value?: any;
}
// 设备告警请求参数
export interface alarmsParams {
  entityType: string;
  entityId: string;
  pageParameters: BasicPageParams & {
    statusList?: string;
    severityList?: string;
    typeList?: string;
    assigneeId?: string;
    textSearch?: string;
    startTime?: number;
    endTime?: number;
    sortProperty?: string;
    sortOrder?: string;
  };
}
// 保存关联请求参数
export interface saveRelationParams {
  type: string;
  additionalInfo: any;
  typeGroup: string;
  from: {
    entityType: string;
    id?: string;
  };
  to: {
    entityType: string;
    id?: string;
  };
}
// 删除关联请求参数
export interface deleteRelationParams {
  fromId?: string;
  fromType: string;
  relationType: string;
  toId?: string;
  toType: string;
}
// 获取关联from请求参数
export interface relationByFromParams {
  fromId?: string;
  fromType: string;
  relationTypeGroup?: string;
}
// 获取关联to请求参数
export interface relationByToParams {
  toId?: string;
  toType: string;
  relationTypeGroup?: string;
}
// 获取关联响应数据
export interface relationResponse {
  from: {
    entityType: string;
    id: string;
  };
  to: {
    entityType: string;
    id: string;
  };
  type: string;
  typeGroup: string;
  additionalInfo: string;
  fromName: string;
  toName: string;
}
// 获取关联表格清洗后数据
export interface relationFromTableItem {
  type: string;
  fromEntityType: string;
  fromEntityName: string;
  fromId: string;
  jsonValue: any;
  id: string;
}
export interface relationToTableItem {
  type: string;
  toEntityType: string;
  toEntityName: string;
  toId: string;
  jsonValue: any;
  id: string;
}
// 审计日志请求参数
export interface auditLogParams {
  entityType: string;
  entityId?: string;
  pageParameters: entityTableParams & {
    startTime?: number;
    endTime?: number;
  };
}
// 审计日志响应数据
export interface auditLogResponse {
  id: {
    id: string;
  };
  createdTime: number;
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  entityId: {
    entityType: string;
    id: string;
  };
  entityName: string;
  userId: {
    entityType: string;
    id: string;
  };
  userName: string;
  actionType: string;
  actionData: {
    relation: {
      from: {
        entityType: string;
        id: string;
      };
      to: {
        entityType: string;
        id: string;
      };
      type: string;
      typeGroup: string;
      additionalInfo: string;
    };
  };
  actionStatus: string;
  actionFailureDetails: string;
}

// 获取事件请求参数
export interface eventParams {
  pageParameters: entityTableParams & {
    startTime?: number;
    endTime?: number;
  };
  startTime?: number;
  endTime?: number;
  tenantId?: string;
  entityType: string;
  entityId?: string;
  eventType: string;
}

// 仓库配置请求参数
export interface repositorySettingsParams {
  repositoryUri: string;
  defaultBranch: string;
  readOnly: boolean;
  showMergeCommits: boolean;
  authMethod: string;
  username?: string;
  password?: string;
  privateKeyFileName?: string;
  privateKey?: string;
  privateKeyPassword?: string;
}

// 导入设备请求参数
export interface importParams {
  file: string;
  mapping: {
    columns: Array<object>;
    delimiter: string;
    header: boolean;
    update: boolean;
  };
}

// 资产页外层表格清洗后数据格式
export interface assetTableItem {
  createdTime: number;
  name: string;
  assetProfileName: string;
  label: string;
  customerTitle: string;
  customerIsPublic: boolean;
  id: object;
  assetProfileId: object;
  additionalInfo: object;
}

// 资产配置列表请求参数
export type assetProfileInfosParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
};

// 保存资产请求参数
export interface saveAssetParams {
  name: string;
  assetProfileId: {
    entityType: string;
    id: string;
  };
  label?: string;
  customerId: {
    entityType: string;
    id: string;
  } | null;
  additionalInfo?: {
    description: string;
  };
}

// 资产更新数据
export interface assetUpdateParams {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  name: string;
  type: string;
  label: string;
  assetProfileId: {
    entityType: string;
    id: string;
  };
  externalId: null;
  customerTitle: string;
  customerIsPublic: boolean;
  assetProfileName: string;
}

// 资产响应数据
export interface assetResponse {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  name: string;
  type: string;
  label: string;
  assetProfileId: {
    entityType: string;
    id: string;
  };
  externalId: string;
  customerTitle: string;
  customerIsPublic: boolean;
  assetProfileName: string;
}

// 实体视图data项
export interface entityViewDataItem {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
  };
  entityId: {
    entityType: string;
    id: string;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  name: string;
  type: string;
  keys: {
    timeseries: null;
    attributes: {
      cs: Array<string>;
      ss: Array<string>;
      sh: Array<string>;
    };
  };
  startTimeMs: number;
  endTimeMs: number;
  externalId: string;
  customerTitle: string;
  customerIsPublic: boolean;
}
// 实体视图表格项
export interface entityViewTableItem {
  createdTime: number;
  entityViewName: string;
  entityViewType: string;
  customer: string;
  isOpen: boolean;
  id: object;
}
// 实体视图响应数据
export interface entityViewResponse {
  data: Array<entityViewDataItem>;
  hasNext: boolean;
  totalElements: number;
  totalPages: number;
}

// 实体视图类型
export interface entityViewType {
  tenantId: {
    entityType: string;
    id: string;
  };
  entityType: string;
  type: string;
}

// 添加实体视图请求参数
export interface addEntityViewParams {
  name: string;
  type: string;
  entityId: {
    entityType: string;
    id: string;
  };
  startTimeMs: number | null;
  endTimeMs: number | null;
  keys: {
    attributes: {
      cs: Array<string> | null;
      sh: Array<string> | null;
      ss: Array<string> | null;
    };
    timeseries: Array<string> | null;
  };
  additionalInfo: {
    description: string | null;
  };
}

export type deviceListGetResultModel = BasicFetchResult<deviceTableItem>;
export type assetListGetResultModel = BasicFetchResult<assetTableItem>;
export type customerListGetResultModel = BasicFetchResult<BasicOptions>;
