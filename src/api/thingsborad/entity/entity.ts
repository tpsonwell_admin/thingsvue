import {
  entityTableParams,
  deviceListGetResultModel,
  customerListGetResultModel,
  profileURLParams,
  deviceInfoItem,
  deviceProfileInfo,
  addBasicDeviceParams,
  addDeviceWithCredentialsParams,
  deviceProfileInfosParams,
  attributesParams,
  deviceCredentialsInfo,
  alarmsParams,
  attributesResponse,
  saveAttributesParams,
  relationByFromParams,
  relationByToParams,
  relationResponse,
  auditLogParams,
  saveRelationParams,
  deleteRelationParams,
  eventParams,
  repositorySettingsParams,
  importParams,
  assetListGetResultModel,
  assetProfileInfosParams,
  saveAssetParams,
  assetUpdateParams,
  assetResponse,
  saveTelemetryParams,
  entityViewResponse,
  entityViewType,
  addEntityViewParams,
  entityViewDataItem,
} from '/@/api/thingsborad/entity/model/entityModel';
import { BasicPageParams } from '/@/api/model/baseModel';
import { defHttp } from '/@/utils/http/axios';
enum Api {
  // 设备列表请求
  DEVICE_TABLE = '/api/tenant/deviceInfos',
  // 实体关于客户
  ABOUT_CUSTOMER = '/api/customer',
  // 根据边缘实例筛选实体
  DEVICE_LIST_BY_EDGE = '/api/edge',
  // 客户列表请求
  CUSTOMER_LIST = '/api/customers',

  // 公开
  setPublic = '/api/customer/public/device/',
  // 删除实体
  deleteEntity = '/api',
  // 获取设备配置信息
  getDeviceProfileInfo = '/api/deviceProfileInfo/',
  // 固件列表请求
  ABOUT_OTAPACKAGES = '/api/otaPackages/',
  // 创建或更新设备请求
  ABOUT_DEVICE = '/api/device',
  // 创建设备带上凭证
  saveDeviceWithCredentials = '/api/device-with-credentials',
  // 单行设备信息请求
  getDeviceInfo = '/api/device/info/',
  // 获取默认设备配置
  getDefaultDeviceConfig = '/api/deviceProfileInfo/default',
  // 获取设备配置列表
  getDeviceProfileInfos = '/api/deviceProfileInfos',
  // 获取属性
  ABOUT_TELEMETRY = '/api/plugins/telemetry',
  // 获取设备凭证
  updateDeviceCredentials = '/api/device/credentials',
  // 获取告警V2
  getAlarmV2 = '/api/v2/alarm/',
  // 获取关联
  getRelation = '/api/relations/info',
  // 保存关联
  saveRelation = '/api/relation',
  // 获取租户
  getTenant = '/api/tenant/',
  // 获取用户列表
  getUsers = '/api/users',
  // 获取仪表盘列表
  getDashboards = '/api/tenant/dashboards',
  // 获取边缘列表
  getEdge = '/api/tenant/edgeInfos',
  // 获取审计日志
  getAuditLogs = '/api/audit/logs/entity',
  // 获取事件
  getEvents = '/api/events',
  // 仓库配置
  saveRepositorySettings = '/api/admin/repositorySettings',
  // 设备导入
  processDevicesBulkImport = '/api/device/bulk_import',
  // 资产列表
  ABOUT_ASSET = '/api/tenant/assetInfos',
  // 资产配置列表
  getAssetProfileInfos = '/api/assetProfileInfos',
  // 创建或更新设备请求
  saveAsset = '/api/asset',
  // 资产导入
  processAssetBulkImport = '/api/asset/bulk_import',
  // 资产详情
  getAssetInfo = '/api/asset/info/',
  // 资产公开
  setAssetPublic = '/api/customer/public/asset/',

  // 实体视图列表
  ABOUT_VIEW = '/api/tenant/entityViewInfos',
  // 实体视图类型
  VIEW_TYPE = '/api/entityView/types',
  // 添加实体视图
  VIEW_ACTION = '/api/entityView',
  // 获取实体视图ById
  getViewInfo = '/api/entityView/info',
  // 实体视图公开
  setViewPublic = '/api/customer/public/entityView/',
  // 实体视图分配客户
  assignCustomerToView = '/api/customer',

  //仪表盘表头
  QUERY_FIND_KEYS = '/api/entitiesQuery/find/keys',

  // 设备类型
  DEVICE_PROFILE_TYPES = '/api/device/types',
}
export const getEntityDeviceList = (
  params: entityTableParams & { deviceId?: string; deviceProfileId?: string; active?: boolean },
) => defHttp.get<deviceListGetResultModel>({ url: Api.DEVICE_TABLE, params });
// 客户端
export const getCustomerEntityDeviceList = (params: any) =>
  defHttp.get<deviceListGetResultModel>({
    url: `${Api.ABOUT_CUSTOMER}/${params.customerId}/deviceInfos`,
    params: params.pagination,
  });

export const getEntityListByCustomerId = (params: entityTableParams & { urlParams: any }) =>
  defHttp.get<deviceListGetResultModel>({
    url: `${Api.ABOUT_CUSTOMER}/${params.urlParams.customerId}/${params.urlParams.infosType}`,
    params,
  });

export const getEntityListByEdgeId = (params: entityTableParams & { urlParams: any }) =>
  defHttp.get<deviceListGetResultModel>({
    url: `${Api.DEVICE_LIST_BY_EDGE}/${params.urlParams.edgeId}/${params.urlParams.infosType}`,
    params,
  });

export const unassignCustomer = (params: any) =>
  defHttp.delete<any>({ url: `${Api.ABOUT_CUSTOMER}/${params.entityType}/`, params: params.id });

export function assignCustomer(params: any): Promise<any> {
  return new Promise((resolve, reject) => {
    defHttp
      .post<any>({
        url: `${Api.ABOUT_CUSTOMER}/${params.customerId}/${params.entityType}/${params.entityId}`,
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const getCustomerList = (params?: BasicPageParams) =>
  defHttp.get<customerListGetResultModel>({ url: Api.CUSTOMER_LIST, params });

export const setPublic = (params: string) => defHttp.post<any>({ url: Api.setPublic, params });

export const deleteEntity = (params: any) =>
  defHttp.delete<any>({
    url: `${Api.deleteEntity}/${params.entityType}/${params.entityId}`,
  });

export const getDeviceProfileInfo = (params?: string) =>
  defHttp.get<any>({ url: Api.getDeviceProfileInfo, params });

export const getFirmwareList = (params: profileURLParams) =>
  defHttp.get<any>({
    url: Api.ABOUT_OTAPACKAGES + `${params.id}` + '/FIRMWARE',
    params: params.obj,
  });

export const getSoftwareList = (params: profileURLParams) =>
  defHttp.get<any>({
    url: Api.ABOUT_OTAPACKAGES + `${params.id}` + '/SOFTWARE',
    params: params.obj,
  });

export const updateDevice = (params: deviceInfoItem) =>
  defHttp.post<any>({ url: Api.ABOUT_DEVICE, params });

export const getDeviceInfo = (params: string) =>
  defHttp.get<deviceInfoItem>({ url: Api.getDeviceInfo, params });

export const getCredentials = (params: string) =>
  defHttp.get<deviceCredentialsInfo>({ url: `${Api.ABOUT_DEVICE}/${params}` });

export const getDefaultDeviceConfig = () =>
  defHttp.get<deviceProfileInfo>({ url: Api.getDefaultDeviceConfig });

export const createDevice = (params: addBasicDeviceParams) =>
  defHttp.post<any>({ url: Api.ABOUT_DEVICE, params });

export const saveDeviceWithCredentials = (params: addDeviceWithCredentialsParams) =>
  defHttp.post<any>({ url: Api.saveDeviceWithCredentials, params });

export const getDeviceProfileInfos = (params: deviceProfileInfosParams) =>
  defHttp.get<any>({ url: Api.getDeviceProfileInfos, params });
// 保存实体属性
export const saveEntityAttributes = (params: saveAttributesParams) =>
  defHttp.post<any>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/${params.scope}`,
    params: { [params.key]: params.value },
  });

export const getClientScope = (params: attributesParams) =>
  defHttp.get<attributesResponse[]>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/${params.responseDataType}/attributes/${params.scope}`,
  });

export const getServeScope = (params: attributesParams) =>
  defHttp.get<attributesResponse[]>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/${params.responseDataType}/attributes/${params.scope}`,
  });

export const getSharedScope = (params: attributesParams) =>
  defHttp.get<attributesResponse[]>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/${params.responseDataType}/attributes/${params.scope}`,
  });

export const getAttributeKeys = (params: attributesParams) =>
  defHttp.get<attributesResponse[]>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/keys/attributes`,
  });

export const getDeviceCredentialsByDeviceId = (params: string) =>
  defHttp.get<any>({ url: `${Api.ABOUT_DEVICE}/${params}` });

export const updateDeviceCredentials = (params: deviceCredentialsInfo) =>
  defHttp.post<any>({ url: Api.updateDeviceCredentials, params });
// 告警
export const getAlarmV2 = (params: alarmsParams) =>
  defHttp.get<any>({
    url: Api.getAlarmV2 + `${params.entityType}/` + `${params.entityId}`,
    params: params.pageParameters,
  });
// 设备关联from-to
export const getRelationByFrom = (params: relationByFromParams) =>
  defHttp.get<relationResponse>({ url: Api.getRelation, params });

export const getRelationByTo = (params: relationByToParams) =>
  defHttp.get<relationResponse>({ url: Api.getRelation, params });
// 获取资产表格数据
export const getTenantAssetInfos = (
  params: entityTableParams & { assetProfileId: string; type: string },
) => defHttp.get<any>({ url: Api.ABOUT_ASSET, params });
// 获取实体视图
export const getTenantEntityViews = (params: entityTableParams & { type: string }) =>
  defHttp.get<any>({ url: Api.ABOUT_VIEW, params });
// 获取租户
export const getTenant = (params: string) => defHttp.get<any>({ url: Api.getTenant + `${params}` });
// 获取用户列表
export const getUsers = (params: entityTableParams) =>
  defHttp.get<any>({ url: Api.getUsers, params });
// 仪表盘列表
export const getDashboards = (params: entityTableParams) =>
  defHttp.get<any>({ url: Api.getDashboards, params });
// 边缘列表
export const getEdge = (params: entityTableParams) =>
  defHttp.get<any>({ url: Api.getEdge, params });
// 审计日志
export const getAuditLogs = (params: auditLogParams) =>
  defHttp.get<any>({
    url: `${Api.getAuditLogs}/${params.entityType}/${params.entityId}`,
    params: params.pageParameters,
  });
// 保存关联
export const saveRelation = (params: saveRelationParams) =>
  defHttp.post<any>({ url: Api.saveRelation, params });
// 删除关联
export const deleteRelation = (params: deleteRelationParams) =>
  defHttp.delete<any>({
    url:
      `${Api.saveRelation}?fromId=${params.fromId}&fromType=${params.fromType}` +
      `&relationType=${params.relationType}&toId=${params.toId}&toType=${params.toType}`,
  });
// 事件列表
export const getEvents = (params: eventParams) =>
  defHttp.post<any>({
    url: `${Api.getEvents}/${params.entityType}/${params.entityId}`,
    params: {
      ...params.pageParameters,
      tenantId: params.tenantId,
    },
    data: {
      eventType: params.eventType,
    },
  });
// 仓库配置
export const saveRepositorySettings = (params: repositorySettingsParams) =>
  defHttp.post<any>({ url: Api.saveRepositorySettings, params });
// 导入设备
export const processDevicesBulkImport = (params: importParams) =>
  defHttp.post<any>({ url: Api.processDevicesBulkImport, params });
// 删除属性数据
export const deleteEntityAttribute = (params: any) =>
  defHttp.delete<any>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/${params.scope}`,
    params: params.obj,
    data: { test: 'test' },
  });
// 删除遥测
export const deleteEntityTelemetry = (params: any) =>
  defHttp.delete<any>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/timeseries/delete`,
    params: params.obj,
    data: { test: 'test' },
  });
// 保存遥测
export const saveEntityTelemetry = (params: saveTelemetryParams) =>
  defHttp.post<any>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/timeseries/LATEST_TELEMETRY`,
    params: { [params.key]: params.value },
  });
// 获取时间序列
export const getTimeseries = (params: any) =>
  defHttp.get<any>({
    url: `${Api.ABOUT_TELEMETRY}/${params.entityType}/${params.entityId}/keys/timeseries`,
  });

// 资产列表
export const getEntityAssetList = (params: entityTableParams) =>
  defHttp.get<assetListGetResultModel>({ url: Api.ABOUT_ASSET, params });
// 客户端资产
export const getCustomerEntityAssetList = (params: any) =>
  defHttp.get<assetListGetResultModel>({
    url: `${Api.ABOUT_CUSTOMER}/${params.customerId}/assetInfos`,
    params: params.pagination,
  });
// 资产配置列表
export const getAssetProfileInfos = (params: assetProfileInfosParams) =>
  defHttp.get<any>({ url: Api.getAssetProfileInfos, params });
// 添加资产
export const saveAsset = (params: saveAssetParams) =>
  defHttp.post<any>({ url: Api.saveAsset, params });
// 导入资产
export const processAssetsBulkImport = (params: importParams) =>
  defHttp.post<any>({ url: Api.processAssetBulkImport, params });
// 资产详情
export const getAssetInfo = (params: string) =>
  defHttp.get<assetResponse>({ url: Api.getAssetInfo, params });
// 更新资产
export const updateAsset = (params: assetUpdateParams) =>
  defHttp.post<any>({ url: Api.saveAsset, params });
// 资产公开
export const setAssetPublic = (params: string) =>
  defHttp.post<any>({ url: Api.setAssetPublic, params });

// 实体视图列表
export const getEntityViewList = (params: entityTableParams) =>
  defHttp.get<entityViewResponse>({ url: Api.ABOUT_VIEW, params });
// 客户端
export const getCustomerEntityViewList = (params) =>
  defHttp.get<entityViewResponse>({
    url: `${Api.ABOUT_CUSTOMER}/${params.customerId}/entityViewInfos`,
    params: params.pagination,
  });
// 实体视图类型
export const getEntityViewTypeList = () => defHttp.get<entityViewType[]>({ url: Api.VIEW_TYPE });
// 添加实体视图
export const addEntityView = (params: addEntityViewParams) =>
  defHttp.post<any>({ url: Api.VIEW_ACTION, params });
// 获取实体视图信息
export const getViewInfo = (params: string) =>
  defHttp.get<entityViewDataItem>({ url: `${Api.getViewInfo}/${params}` });
// 实体视图公开
export const setViewPublic = (params: string) =>
  defHttp.post<any>({ url: Api.setViewPublic, params });
// 实体视图分配客户
export const assignCustomerToView = (params: any) =>
  defHttp.post<any>({
    url: `${Api.assignCustomerToView}/${params.customerId}/entityView/${params.entityViewId}`,
  });
// 删除实体视图
export const deleteEntityView = (params: string) =>
  defHttp.delete<any>({ url: `${Api.VIEW_ACTION}/${params}` });

// 获取实体信息
export const getEntityInfo = (params: any) =>
  defHttp.get<any>({
    url: `/api/${params.entityType}${
      ['customer', 'ruleChain'].includes(params.entityType) ? '' : '/info'
    }/${params.entityId}`,
  });

// 仪表盘表头
export const getColumnsList = (data: any) =>
  defHttp.post<any>({ url: Api.QUERY_FIND_KEYS, params: data.params, data: data.data });

// 获取设备类型
export const getDeviceTypes = () => defHttp.get<any>({ url: Api.DEVICE_PROFILE_TYPES });
