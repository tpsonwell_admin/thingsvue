import { tenantParams } from './model/tenantModel';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  TENANT_INFOS = '/api/tenantInfos',
  DELETE_TENANT = '/api/tenant',
  TENANT_INFO = '/api/tenant/info',
  DELETE_USER = '/api/user',
  TENANT_PROFILE_INFO = '/api/tenantProfileInfo/default',
  TENANT_PROFILE_INFO_LSIT = '/api/tenantProfileInfos',
  TENANT_PROFILE = '/api/tenantProfile',
}

export const getTenantInfos = (params: tenantParams) =>
  defHttp.get<any>({ url: Api.TENANT_INFOS, params });

export const deleteTheTenant = (id: string) =>
  defHttp.delete<any>({ url: Api.DELETE_TENANT + `/${id}` });

export const addTheTenant = (data: any) => defHttp.post<any>({ url: Api.DELETE_TENANT, data });

export const getTheTenantById = (id: string) =>
  defHttp.get<any>({ url: Api.TENANT_INFO + `/${id}` });

export const getOneTenantById = (id: any) =>
  defHttp.get<any>({ url: Api.DELETE_TENANT + `/${id}` });

export const getAdminInfo = (data: any) =>
  defHttp.get<any>({ url: Api.DELETE_TENANT + `/${data.id}/users`, params: data.params });

export const deleteTheUser = (id: string) =>
  defHttp.delete<any>({ url: Api.DELETE_USER + `/${id}` });

// 获取已经选择的租户配置
export const getTenantProfileInfo = () => defHttp.get<any>({ url: Api.TENANT_PROFILE_INFO });

// 获取租户列表
export const getTenantProfileInfoList = (params: any) =>
  defHttp.get<any>({ url: Api.TENANT_PROFILE_INFO_LSIT, params });

// 获取租户配置的详细数据
export const getTenantProfile = (id: string) => defHttp.get({ url: Api.TENANT_PROFILE + `/${id}` });

// 添加租户用户
export const addTheUser = (data: any) =>
  defHttp.post({ url: Api.DELETE_USER, params: data.params, data: data.data });

//获取配置更改密码
export const activationLink = (id: string) =>
  defHttp.get({ url: Api.DELETE_USER + `/${id}/activationLink` });

// 获取用户的详细信息
export const getTheUserInfo = (id: string) => defHttp.get({ url: Api.DELETE_USER + `/${id}` });

// 以租户管理员身份登录
export const loginAsTenantAdmin = (userId: string) =>
  defHttp.get({ url: Api.DELETE_USER + `/${userId}/token` });
