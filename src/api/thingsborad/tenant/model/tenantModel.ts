import { BasicPageParams } from '/@/api/model/baseModel';

export type tenantParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  type?: string;
  textSearch?: string;
};
