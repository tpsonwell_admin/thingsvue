import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type notificationsParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  unreadOnly?: boolean;
  notificationType?: string;
  notificationTypes?: string;
  textSearch?: string;
};
// 收件箱类
export interface notificationsItem {
  createdTime: string;
  type: string;
  text: string;
  subject: string;
  id: string;
  status: string;
  actionButtonConfig: any;
  info: any;
}
//已发送
export interface requestsItem {
  createdTime: string;
  status: string;
  deliveryMethods: string;
  template: string;
  id: string;
}
//收件人
export interface targetsItem {
  createdTime: string;
  name: string;
  type: string;
  description: string;
  id: string;
}
//模板
export interface templatesItem {
  createdTime: string;
  name: string;
  notificationType: string;
  id: string;
}
//规则
//模板
export interface rulesItem {
  createdTime: string;
  name: string;
  templateName: string;
  triggerType: string;
  description: string;
  id: string;
  enabled: boolean;
}
//收件箱类
export type notificationListGetResultModel = BasicFetchResult<notificationsItem>;
//已发送
export type requestsListGetResultModel = BasicFetchResult<requestsItem>;
//收件人
export type targetsListGetResultModel = BasicFetchResult<targetsItem>;
//模板
export type templatesListGetResultModel = BasicFetchResult<templatesItem>;
//规则
export type rulesListGetResultModel = BasicFetchResult<rulesItem>;
