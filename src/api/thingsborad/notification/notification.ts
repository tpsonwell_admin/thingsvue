import {
  notificationsParams,
  notificationListGetResultModel,
  requestsListGetResultModel,
  targetsListGetResultModel,
  templatesListGetResultModel,
  rulesListGetResultModel,
} from '/@/api/thingsborad/notification/model/notificationsModel';
import { defHttp } from '/@/utils/http/axios';
enum Api {
  //收件箱
  NOTIFICATION_LIST = '/api/notifications',
  //收件箱标记为已读
  NOTIFICATION_OPERATE_LIST = '/api/notification',
  //已发送
  REQUESTS_LIST = '/api/notification/requests',
  //已发送删除
  REQUEST_DELETE = '/api/notification/request',
  //收件人
  TARGETS_LIST = '/api/notification/targets',
  //收件人删除
  TARGET_DELETE = '/api/notification/target',
  //模板
  TEMPLATES_LIST = '/api/notification/templates',

  TEMPLATE_DELETE = 'api/notification/template',
  //规则
  RULES_LIST = '/api/notification/rules',
  //规则删除
  RULES_DELETE = '/api/notification/rule',
  //用户列表
  USERS_LIST = '/api/users',
  //客户列表
  CUSTOMERS_LIST = '/api/customers',
  TENANT_SELECT_LIST = '/api/tenant/deviceInfos',
  // 设备配置
  DEVICE_PROFILE_INFOS_LIST = '/api/deviceProfileInfos',

  NOTIFICATION_DELIVERY_METHODS = '/api/notification/deliveryMethods',

  NOTIFICATION_REQUEST_PREVIEW = '/api/notification/request/preview',

  TENANT_DASHBOARDS = '/api/tenant/dashboards',

  DASHBOARD = '/api/dashboard',

  DASHBOARD_INFO = '/api/dashboard/info',

  ONE_CUSTOMER = '/api/customer',

  ONE_USER = '/api/user',

  NOTIFICATION_TEMPLATE = '/api/notification/template',

  NOTIFICATIONS_READ = 'api/notifications/read',

  //租户列表
  TENANTS_LIST = '/api/tenants',

  //租户配置列表
  TENANT_PROFILES = '/api/tenantProfiles',
}

//收件箱数据列表
export const getNotificationListList = (params: notificationsParams) =>
  defHttp.get<notificationListGetResultModel>({ url: Api.NOTIFICATION_LIST, params });
//收件箱标记已读
export const readTheInbox = (params: string) =>
  defHttp.put<any>({ url: Api.NOTIFICATION_OPERATE_LIST, params });
//删除收件箱信息
export const deleteTheInbox = (params: string) =>
  defHttp.delete<any>({ url: Api.NOTIFICATION_OPERATE_LIST, params });
// 已发送
export const getRequestsList = (params: notificationsParams) =>
  defHttp.get<requestsListGetResultModel>({ url: Api.REQUESTS_LIST, params });
// 已发送 删除
export const deleteTheRequests = (params: string) =>
  defHttp.delete<any>({ url: Api.REQUEST_DELETE, params });
//收件人
export const getTargetsList = (params: notificationsParams) =>
  defHttp.get<targetsListGetResultModel>({ url: Api.TARGETS_LIST, params });
// 已发送 删除
export const deleteTheTargets = (params: string) =>
  defHttp.delete<any>({ url: Api.TARGET_DELETE, params });
//模板
export const getTemplatesList = (params: notificationsParams) =>
  defHttp.get<templatesListGetResultModel>({ url: Api.TEMPLATES_LIST, params });
// 已发送 删除
export const deleteTheTemplates = (params: string) =>
  defHttp.delete<any>({ url: Api.TEMPLATE_DELETE, params });
//规则
export const getRulesList = (params: notificationsParams) =>
  defHttp.get<rulesListGetResultModel>({ url: Api.RULES_LIST, params });
// 规则 删除
export const deleteTheRules = (params: string) =>
  defHttp.delete<any>({ url: Api.RULES_DELETE, params });
//规则switch开关
export const switchTheRules = (data: any) => defHttp.post<any>({ url: Api.RULES_DELETE, data });
//用户列表
export const getUserList = (params: notificationsParams) =>
  defHttp.get<any>({ url: Api.USERS_LIST, params });
//发送通知按钮 创建收件人
export const createTarget = (data: any) => defHttp.post<any>({ url: Api.TARGET_DELETE, data });
//发送通知按钮 创建收件人ids
export const createTargetIds = (params: string) =>
  defHttp.post<any>({ url: Api.TARGETS_LIST, params });

export const getCustomersList = (params: notificationsParams) =>
  defHttp.get<any>({ url: Api.CUSTOMERS_LIST, params });

export const getTenantSelectList = (params: notificationsParams) =>
  defHttp.get<any>({ url: Api.TENANT_SELECT_LIST, params });

export const getDeviceProfileInfosSelectList = (params: notificationsParams) =>
  defHttp.get<any>({ url: Api.DEVICE_PROFILE_INFOS_LIST, params });

export const getDeliveryMethods = () =>
  defHttp.get<any>({ url: Api.NOTIFICATION_DELIVERY_METHODS });

export const createNotice = (data: any) =>
  defHttp.post<any>({ url: Api.NOTIFICATION_REQUEST_PREVIEW, data });

export const requestNotice = (data: any) => defHttp.post<any>({ url: Api.REQUEST_DELETE, data });

export const getDashboardsList = (params: notificationsParams) =>
  defHttp.get<any>({ url: Api.TENANT_DASHBOARDS, params });

export const getDashboardsStatus = (id: string) =>
  defHttp.get<any>({ url: Api.DASHBOARD + `/${id}` });

export const getOneRecipient = (id: string) =>
  defHttp.get<any>({ url: Api.TARGETS_LIST, params: { ids: id } });

export const getDashboardInfo = (id: string) =>
  defHttp.get<any>({ url: Api.DASHBOARD_INFO + `/${id}` });

export const getOneCustomerInfo = (id: string) =>
  defHttp.get<any>({ url: Api.ONE_CUSTOMER + `/${id}` });

export const getOneUserInfo = (id: string) => defHttp.get<any>({ url: Api.ONE_USER + `/${id}` });

export const getNotificationTemplate = (data: any) =>
  defHttp.post<any>({ url: Api.NOTIFICATION_TEMPLATE, data });

//添加规则
export const createRule = (data: any) => defHttp.post<any>({ url: Api.RULES_DELETE, data });

export const setRead = (data: any) => defHttp.put<any>({ url: Api.NOTIFICATIONS_READ, data });

//租户列表
export const getTenantsList = (params: any) => defHttp.get({ url: Api.TENANTS_LIST, params });

//租户配置列表
export const gettenantProfiles = (params: any) => defHttp.get({ url: Api.TENANT_PROFILES, params });

// 系统端创建收件人
export const createTenant = (data: any) => defHttp.post({ url: Api.TARGET_DELETE, data });
