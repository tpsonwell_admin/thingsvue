import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type deviceProfileParam = BasicPageParams & {
  textSearch?: string;
  sortOrder?: string;
  sortProperty?: string;
};
//lwm2m
export type lwm2mParams = BasicPageParams & {
  textSearch?: string;
  sortOrder?: string;
  sortProperty?: string;
};
//规则链
export type ruleChainParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
  type?: string;
};
//队列
export type queueParams = BasicPageParams & {
  serviceType: string;
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
};
//移动端仪表盘
export type dashboardsParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
};
//ota
export type otaParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
};
export interface lwm2mItem {
  id: number;
  keyId: string;
  name: string;
  multiple: boolean;
  mandatory: boolean;
  instances: lwm2mInstance[];
}
export interface ruleChainItem {
  id: {
    id: string;
    entityType: string;
  };
  name: string;
}
export interface dashboardItem {
  id: {
    id: string;
    entityType: string;
  };
  name: string;
}
export interface queueItem {
  name: string;
}
export interface lwm2mInstance {
  id: number;
  resources: lwm2mInstanceResources[];
}

export interface lwm2mInstanceResources {
  id: number;
  name: string;
  observe: boolean;
  attribute: boolean;
  telemetry: boolean;
  keyName: string;
}

export type lwm2mItemModel = BasicFetchResult<lwm2mItem>;
//获取规则链
export type ruleChainItemModel = BasicFetchResult<ruleChainItem>;
//获取移动端仪表盘
export type dashboardItemModel = BasicFetchResult<dashboardItem>;

//获取队列
export type queueItemModel = BasicFetchResult<queueItem>;
