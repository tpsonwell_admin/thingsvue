import { defHttp } from '/@/utils/http/axios';
import {
  lwm2mParams,
  lwm2mItemModel,
  ruleChainParams,
  ruleChainItemModel,
  dashboardItemModel,
  dashboardsParams,
  queueParams,
  queueItemModel,
  deviceProfileParam,
  otaParams,
} from '/@/api/thingsborad/profile/deviceProfile/model/deviceProfileModel';

enum Api {
  LWM2M_LIST = '/api/resource/lwm2m/page',
  ATTRIBUTE_LIST = '/api/deviceProfile/devices/keys/attributes',
  TIMESERIES_LIST = '/api/deviceProfile/devices/keys/timeseries',
  RULECHAIN_LIST = '/api/ruleChains',
  DASHBOARD_LIST = '/api/tenant/dashboards',
  QUEUE_LIST = '/api/queues',
  DEVICEPROFILE_LIST = '/api/deviceProfiles',
  DEVICEPROFILE_ITEM = '/api/deviceProfile',
  OTAPACKAGE_INFO = '/api/otaPackage/info',
  OTAPACKAGE_LIST = '/api/otaPackages',
}

//获取deviceProfiles
export const getDeviceProfiles = (params: deviceProfileParam) =>
  defHttp.get<Object>({ url: Api.DEVICEPROFILE_LIST, params });
//删除deviceProfile
export const deleteDeviceProfile = (params: string) =>
  defHttp.delete({ url: Api.DEVICEPROFILE_ITEM, params });
//将deviceProfile设置为默认
export const defaultDeviceProfile = (params: string) =>
  defHttp.post({ url: Api.DEVICEPROFILE_ITEM, params });
//获取单个deviceProfile的信息
export const getDeviceProfileById = (params: string) =>
  defHttp.get({ url: Api.DEVICEPROFILE_ITEM, params });
//添加修改deviceProfile
export const postDeviceProfile = (params: any) =>
  defHttp.post({ url: Api.DEVICEPROFILE_ITEM, params });
//获取软硬件信息
export const getOtaPackageInfo = (params: string) =>
  defHttp.get({ url: Api.OTAPACKAGE_INFO, params });

//获取软硬件列表
export const getOtaPackageList = (params: otaParams) =>
  defHttp.get({ url: Api.OTAPACKAGE_LIST, params });

export const getLWM2MList = (params: lwm2mParams) =>
  defHttp.get<lwm2mItemModel>({ url: Api.LWM2M_LIST, params });

//获取attribute列表
export const getAttribute = (params?: any) => defHttp.get({ url: Api.ATTRIBUTE_LIST, params });

//获取timeseries列表
export const getTimeseries = (params?: any) => defHttp.get({ url: Api.TIMESERIES_LIST, params });

export const getRuleChainList = (params: ruleChainParams) =>
  defHttp.get<ruleChainItemModel>({ url: Api.RULECHAIN_LIST, params });

//获取移动端仪表盘数据
export const getDashboards = (params: dashboardsParams) =>
  defHttp.get<dashboardItemModel>({ url: Api.DASHBOARD_LIST, params });

//获取队列数据
export const getQueues = (params: queueParams) =>
  defHttp.get<queueItemModel>({ url: Api.QUEUE_LIST, params });
