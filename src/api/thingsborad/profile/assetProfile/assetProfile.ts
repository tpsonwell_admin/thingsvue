import { defHttp } from '/@/utils/http/axios';
import {
  assetProfileParams,
  assetProfileItemModel,
  assetProfileDefaultModel,
  ruleChainParams,
  ruleChainItemModel,
  dashboardsParams,
  dashboardItemModel,
  queueParams,
  queueItemModel,
} from '/@/api/thingsborad/profile/assetProfile/model/assetProfileModel';
enum Api {
  ASSETPROFILE_TABLELIST = '/api/assetProfiles',
  ASSETPROFILE_ITEM = '/api/assetProfile',
  RULECHAIN_LIST = '/api/ruleChains',
  RULECHAIN_ITEM = '/api/ruleChain',
  DASHBOARD_LIST = '/api/tenant/dashboards',
  QUEUE_LIST = '/api/queues',
  ASSETPROFILE_POST = '/api/assetProfile',
  DASHBOARD_ITEM = '/api/dashboard/info',
}
//资产配置数据列表
export const getAssetProfileTableList = (params: assetProfileParams) =>
  defHttp.get<assetProfileItemModel>({ url: Api.ASSETPROFILE_TABLELIST, params });
//添加修改资产配置
export const postAssetProfile = (params: Object) =>
  defHttp.post<Object>({ url: Api.ASSETPROFILE_POST, params });

//获取某个的资产配置详情
export const getAssetProfileById = (params: String) =>
  defHttp.get<Object>({ url: Api.ASSETPROFILE_ITEM, params });
//删除资产配置
export const deleteAssetProfile = (params: string) => {
  defHttp.delete({ url: Api.ASSETPROFILE_ITEM, params });
};
//将此资产配置设置为默认
export const setAssetProfileDefault = (params: string) =>
  defHttp.post<assetProfileDefaultModel>({ url: Api.ASSETPROFILE_ITEM, params });

//获取规则链数据
export const getRuleChains = (params: ruleChainParams) =>
  defHttp.get<ruleChainItemModel>({ url: Api.RULECHAIN_LIST, params });

//获取单个规则链数据
export const getRuleChainItem = (params: String) =>
  defHttp.get<Object>({ url: Api.RULECHAIN_ITEM, params });

//获取单个移动仪表盘
export const getDashBoardItem = (params: String) =>
  defHttp.get<Object>({ url: Api.DASHBOARD_ITEM, params });
//获取移动端仪表盘数据
export const getDashboards = (params: dashboardsParams) =>
  defHttp.get<dashboardItemModel>({ url: Api.DASHBOARD_LIST, params });

//获取队列数据
export const getQueues = (params: queueParams) =>
  defHttp.get<queueItemModel>({ url: Api.QUEUE_LIST, params });
