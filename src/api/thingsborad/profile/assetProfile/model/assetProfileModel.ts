import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

//资产配置
export type assetProfileParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
};
//规则链
export type ruleChainParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
  type?: string;
};
//单个规则链
export type ruleChainItemParams = {
  ruleChainId: string;
};

//移动端仪表盘
export type dashboardsParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
  mobile?: boolean;
};

//队列
export type queueParams = BasicPageParams & {
  serviceType: string;
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: string;
};
//资产配置data
export interface assetProfileItem {
  id: {
    id: string;
    entityType: string;
  };
  createdTime: number;
  tenantId: {
    id: string;
    entityType: string;
  };
  name: string;
  default: boolean;
  defaultDashboardId: {
    id: string;
    entityType: string;
  };
  defaultRuleChainId: {
    id: string;
    entityType: string;
  };
  defaultQueueName: string;
  description: string;
  image: string;
  defaultEdgeRuleChainId: {
    id: string;
    entityType: string;
  };
}

//规则链data
export interface ruleChainItem {
  additionalInfo: {};
  id: {
    id: string;
    entityType: string;
  };
  createdTime: number;
  tenantId: {
    id: string;
    entityType: string;
  };
  name: string;
  type: string;
  firstRuleNodeId: {
    id: string;
    entityType: string;
  };
  root: boolean;
  debugMode: boolean;
  configuration: {};
}

//移动端仪表盘data
export interface dashboardItem {
  id: {
    id: string;
    entityType: string;
  };
  createdTime: number;
  tenantId: {
    id: string;
    entityType: string;
  };
  name: string;
  title: string;
  assignedCustomers: [
    {
      public: boolean;
      customerId: {
        id: string;
        entityType: string;
      };
      title: string;
    },
  ];
  mobileHide: boolean;
  mobileOrder: number;
  image: string;
}

//队列data
export interface queueItem {
  additionalInfo: {};
  consumerPerPartition: boolean;
  createdTime: number;
  id: {
    id: string;
    entityType: string;
  };
  name: string;
  packProcessingTimeout: number;
  partitions: number;
  pollInterval: number;
  processingStrategy: {
    failurePercentage: number;
    maxPauseBetweenRetries: number;
    pauseBetweenRetries: number;
    retries: number;
    type: string;
  };
  submitStrategy: {
    batchSize: number;
    type: string;
  };
  tenantId: {
    id: string;
    entityType: string;
  };
  topic: string;
}

//资产配置表格数据
export type assetProfileItemModel = BasicFetchResult<assetProfileItem> & {
  totalPages: number;
  totalElements: number;
  hasNext: boolean;
};
//设置为默认
export type assetProfileDefaultModel = BasicFetchResult<assetProfileItem>;

//获取规则链
export type ruleChainItemModel = BasicFetchResult<ruleChainItem> & {
  totalPages: number;
  totalElements: number;
  hasNext: boolean;
};

//获取移动端仪表盘
export type dashboardItemModel = BasicFetchResult<dashboardItem> & {
  totalPages: number;
  totalElements: number;
  hasNext: boolean;
};

//获取队列
export type queueItemModel = BasicFetchResult<queueItem> & {
  totalPages: number;
  totalElements: number;
  hasNext: boolean;
};
