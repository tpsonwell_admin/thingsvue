import { defHttp } from '/@/utils/http/axios';
import { TableParams } from './model/tenantProfileModel';
enum Api {
  TABLE_LIST = '/api/tenantProfiles',
  ABOUT_PROFILE = '/api/tenantProfile',
}
//获取deviceProfiles
export const getTableList = (params: TableParams) =>
  defHttp.get<Object>({ url: Api.TABLE_LIST, params });
// 导出配置
export const download = (params: string) =>
  defHttp.get<Object>({ url: `${Api.ABOUT_PROFILE}/${params}` });
// 设为默认
export const setDefault = (params: any) =>
  defHttp.post<Object>({
    url: `${Api.ABOUT_PROFILE}/${params.profileId}/default`,
    params: params.payload,
  });
// 添加新配置
export const addNewProfiles = (params: any) =>
  defHttp.post<Object>({ url: Api.ABOUT_PROFILE, params });
// 删除配置
export const deleteProfiles = (params: string) =>
  defHttp.delete<Object>({ url: `${Api.ABOUT_PROFILE}/${params}` });
// id查找
export const getInfoById = (params: string) =>
  defHttp.get<Object>({ url: `${Api.ABOUT_PROFILE}/${params}` });
