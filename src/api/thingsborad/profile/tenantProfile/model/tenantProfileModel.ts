import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type TableParams = BasicPageParams & {
  textSearch?: string;
  sortOrder: string;
  sortProperty: string;
};
