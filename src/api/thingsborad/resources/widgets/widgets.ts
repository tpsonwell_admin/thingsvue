import { defHttp } from '/@/utils/http/axios';
import { widgetsTableParams, widgetsTableResponse, addWidgetParams } from './widgetsModel';
enum Api {
  // Widgets表格
  WIDGETS_TABLE = '/api/widgetTypes',
  // 导出部件
  exportWidgets = '/api/widgetType',
  // 部件包表格
  COMPONENT_TABLE = '/api/widgetsBundles',

  // 导出部分信息
  exportInfo = '/api/widgetsBundle',
  // 导出
  getBundleWidgetTypesDetails = '/api/widgetTypesDetails',
  // 删除
  getBundleWidgetTypeFqns = '/api/widgetTypeFqns',

  // 添加部件
  addWidgetsBundle = '/api/widgetsBundle',
  // 获取部件包部件
  getBundleWidgetTypeInfos = '/api/widgetTypesInfos',
}
// widgets表格
export const getWidgets = (params: widgetsTableParams) =>
  defHttp.get<widgetsTableResponse>({ url: `${Api.WIDGETS_TABLE}`, params });
// 导出widgets
export const exportWidgets = (params: string) =>
  defHttp.get<any>({ url: `${Api.exportWidgets}/${params}` });
// 删除widgets
export const deleteWidgets = (params: string) =>
  defHttp.delete<any>({ url: `${Api.exportWidgets}/${params}` });
// 编辑widgets
export const editWidgets = (params: object) =>
  defHttp.post<any>({ url: `${Api.exportWidgets}`, params });
// 导入widget
export const importWidget = (params: any) =>
  defHttp.post<any>({
    url: Api.exportWidgets,
    params: { updateExistingByFqn: params.updateExistingByFqn },
    data: params.jsonValue,
  });
// 部件包表格
export const getCommentTable = (params: widgetsTableParams) =>
  defHttp.get<any>({ url: Api.COMPONENT_TABLE, params });

// 导出部分信息
export const exportInfo = (params: string) =>
  defHttp.get<any>({ url: `${Api.exportInfo}/${params}` });

// 导出部件
export const getBundleWidgetTypesDetails = (params: any) =>
  defHttp.get<any>({ url: Api.getBundleWidgetTypesDetails, params });
// 删除部件
export const deleteBundleWidget = (params: string) =>
  defHttp.delete<any>({ url: `${Api.exportInfo}/${params}` });

export const getBundleWidgetTypeFqns = (params: any) =>
  defHttp.get<any>({ url: Api.getBundleWidgetTypeFqns, params });
// 详情
export const getWidgetsBundleInfo = (params: string) =>
  defHttp.get<any>({ url: `${Api.exportInfo}/${params}` });
// 添加部件
export const addWidgetsBundle = (params: addWidgetParams) =>
  defHttp.post<any>({ url: Api.exportInfo, params });

// 获取部件代码
export const getWidgetCode = (params: string) =>
  defHttp.get<any>({ url: `${Api.exportWidgets}/${params}` });
// 保存部件代码
export const saveWidgetCode = (params: any) =>
  defHttp.post<any>({ url: `${Api.exportWidgets}`, params });

// 部件包中添加部件
export const addWidgetsBundleWidget = (params: any) =>
  defHttp.post<any>({
    url: `${Api.exportInfo}/${params.packageId}/widgetTypes`,
    params: params.idList,
  });
// 获取部件包部件
export const getBundleWidgetTypeInfos = (params: any) =>
  defHttp.get<any>({ url: Api.getBundleWidgetTypeInfos, params });
// 获取对应fqn的部件信息
export const getWidgetByFqn = (params: Recordable) =>
  defHttp.get<any>({ url: Api.exportWidgets, params });
// 保存部件信息
export const saveWidgetInfo = (params: any) =>
  defHttp.post<any>({ url: Api.exportWidgets, params });
// 获取对应id的部件信息
export const getWidgetById = (params: string) =>
  defHttp.get<any>({ url: `${Api.exportWidgets}/${params}` });
