import { BasicPageParams, BasicResponse } from '/@/api/model/baseModel';

// 表格请求参数
export type entityTableParams = BasicPageParams & {
  sortProperty?: string;
  sortOrder?: string;
  textSearch?: any;
};

// 部件表格请求参数
export type widgetsTableParams = entityTableParams & {
  tenantOnly: boolean;
  fullSearch: boolean;
  deprecatedFilter?: string;
};

// 部件表格响应item
export interface widgetsTableItem {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  tenantId: {
    entityType: string;
    id: string;
  };
  fqn: string;
  name: string;
  deprecated: boolean;
  image: string;
  description: string;
  tags: Array<string>;
  widgetType: string;
}

// 添加部件
export interface addWidgetParams {
  title: string;
  image?: string;
  description?: string;
  order?: string;
}
// 部件表格响应数据
export type widgetsTableResponse = BasicResponse<widgetsTableItem>;
