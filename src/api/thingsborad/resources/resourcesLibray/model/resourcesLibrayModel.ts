import { BasicPageParams } from '/@/api/model/baseModel';

// 表格请求参数
export type resourcesParams = BasicPageParams & {
  sortProperty: string;
  sortOrder: string;
  resourceType?: string;
  textSearch?: string;
};
