import { defHttp } from '/@/utils/http/axios';
import { resourcesParams } from './model/resourcesLibrayModel';
enum Api {
  RESOURCE_LIST = '/api/resource',
}

export const getResourceList = (params: resourcesParams) =>
  defHttp.get<any>({ url: Api.RESOURCE_LIST, params });

export const addTheResource = (data: any) => defHttp.post<any>({ url: Api.RESOURCE_LIST, data });

export const deleteTheResource = (id: any) =>
  defHttp.delete<any>({ url: Api.RESOURCE_LIST + `/${id}` });

export const downloadTheResource = (id: any) =>
  defHttp.get<any>({ url: Api.RESOURCE_LIST + `/${id}/download` });

export const editTheResource = (id: any) => defHttp.get<any>({ url: Api.RESOURCE_LIST + `/${id}` });

export const getTheResourceInfo = (id: any) =>
  defHttp.get<any>({ url: Api.RESOURCE_LIST + `/${id}` });
