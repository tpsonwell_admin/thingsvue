/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  username: string;
  password: string;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  userId: string | number;
  token: string;
  role: RoleInfo;
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
    defaultDashboardId: string;
    defaultDashboardFullscreen: boolean;
    homeDashboardId: string;
    homeDashboardHideToolbar: boolean;
    userCredentialsEnabled: boolean;
    lang: string;
    failedLoginAttempts: number;
    lastLoginTs: number;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  email: string;
  authority: string;
  firstName: string;
  lastName: string;
  phone: string;
  name: string;
}
