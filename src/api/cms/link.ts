import { defHttp } from '/@/utils/http/axios';
enum Api {
  OPTIONS_LIST = '/api/auth/login',
  RESET_PASSWORD_BY_EMAIL = 'api/noauth/resetPasswordByEmail',
}

/**
 * @description: Get sample options value
 */
export const optionsListApi = (params: any) => defHttp.post<any>({ url: Api.OPTIONS_LIST, params });
export const resetPasswordByEmail = (params: any) =>
  defHttp.post<any>({ url: Api.RESET_PASSWORD_BY_EMAIL, params });
