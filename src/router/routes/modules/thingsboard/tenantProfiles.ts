import type { AppRouteModule } from '/@/router/types';
import { RoleEnum } from '/@/enums/roleEnum';

import { LAYOUT } from '/@/router/constant';

const tbTenantProfiles: AppRouteModule = {
  path: '/tb-tenantProfiles',
  name: 'tenantProfiles',
  component: LAYOUT,
  redirect: '/tb-tenantProfiles/index',
  meta: {
    orderNo: 6013,
    icon: 'ion:build-outline',
    title: '租户配置',
    hideChildrenInMenu: true,
    roles: [RoleEnum.SYS_ADMIN],
  },
  children: [
    {
      path: 'index',
      name: 'tbTenantProfiles',
      component: () => import('/@/views/thingsborad/tenantProfiles/index.vue'),
      meta: {
        title: '租户配置',
        icon: 'ion:build-outline',
        hideMenu: true,
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
  ],
};

export default tbTenantProfiles;
