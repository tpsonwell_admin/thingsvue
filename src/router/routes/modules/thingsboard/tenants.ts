import type { AppRouteModule } from '/@/router/types';
import { RoleEnum } from '/@/enums/roleEnum';

import { LAYOUT } from '/@/router/constant';

const tbTenant: AppRouteModule = {
  path: '/tb-tenant',
  name: 'tenant',
  component: LAYOUT,
  redirect: '/tb-tenant/index',
  meta: {
    orderNo: 6012,
    hideChildrenInMenu: true,
    icon: 'ant-design:user-outlined',
    title: '租户',
    roles: [RoleEnum.SYS_ADMIN],
  },
  children: [
    {
      path: 'index',
      name: 'Tenant',
      component: () => import('/@/views/thingsborad/tenant/index.vue'),
      meta: {
        title: '租户',
        icon: 'ant-design:user-outlined',
        hideMenu: true,
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
    {
      path: 'ManageTenant',
      name: 'ManageTenant',
      component: () => import('/@/views/thingsborad/tenant/components/ManageTenant.vue'),
      meta: {
        title: '管理租户管理员',
        // icon: 'ph:users-fill',
        hideMenu: true,
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
  ],
};

export default tbTenant;
