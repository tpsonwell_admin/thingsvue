import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbcustomers: AppRouteModule = {
  path: '/tb-customers',
  name: 'tbcustomers',
  component: LAYOUT,
  redirect: '/tb-customers/index',
  meta: {
    orderNo: 6008,
    hideChildrenInMenu: true,
    icon: 'ph:users-fill',
    title: '客户',
    roles: [RoleEnum.TENANT_ADMIN],
  },
  children: [
    {
      path: 'index',
      name: 'TbcustomersPage',
      component: () => import('/@/views/thingsborad/customers/index.vue'),
      meta: {
        title: '客户',
        icon: 'ph:users-fill',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
    {
      path: 'manageUser',
      name: 'ManageUser',
      component: () => import('/@/views/thingsborad/customers/components/ManageUser.vue'),
      meta: {
        title: '客户->信息',
        // icon: 'ph:users-fill',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
  ],
};

export default tbcustomers;
