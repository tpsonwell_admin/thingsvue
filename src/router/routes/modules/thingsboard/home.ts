import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbhome: AppRouteModule = {
  path: '/tb-home',
  name: 'tbhome',
  component: LAYOUT,
  redirect: '/tb-home/index',
  meta: {
    orderNo: 6000,
    hideChildrenInMenu: true,
    icon: 'ion:home-sharp',
    title: '首页',
  },
  children: [
    {
      path: 'index',
      name: 'TbhomePage',
      component: () => import('/@/views/thingsborad/home/index.vue'),
      meta: {
        title: '首页',
        icon: 'ion:home-sharp',
        hideMenu: true,
      },
    },
  ],
};

export default tbhome;
