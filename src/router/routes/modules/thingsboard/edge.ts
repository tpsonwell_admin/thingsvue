import type { AppRouteModule } from '/@/router/types';
import { RoleEnum } from '/@/enums/roleEnum';

import { LAYOUT } from '/@/router/constant';

const tbedge: AppRouteModule = {
  path: '/tb-edge',
  name: 'Tbedge',
  component: LAYOUT,
  meta: {
    orderNo: 6012,
    icon: 'eos-icons:edge-computing',
    title: '边缘管理',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
  },

  children: [
    {
      path: 'instance',
      name: 'Instance',
      component: () => import('/@/views/thingsborad/edge/edgeInstance/index.vue'),
      meta: {
        title: '边缘实例',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
    {
      path: 'rulechainTemplate',
      name: 'RulechainTemplate',
      component: () => import('/@/views/thingsborad/edge/ruleTemplate/index.vue'),
      meta: {
        title: '规则链模板',
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
  ],
};

export default tbedge;
