import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbadvanced: AppRouteModule = {
  path: '/tb-advanced',
  name: 'Tbadvanced',
  component: LAYOUT,
  meta: {
    orderNo: 6013,
    icon: 'pajamas:issue-type-maintenance',
    title: '高级功能',
    roles: [RoleEnum.TENANT_ADMIN],
  },

  children: [
    {
      path: 'ota',
      name: 'Ota',
      component: () => import('/@/views/thingsborad/advanced/ota/index.vue'),
      meta: {
        title: 'OTA升级',
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
    {
      path: 'version',
      name: 'Version',
      component: () => import('/@/views/thingsborad/advanced/versionControl/index.vue'),
      meta: {
        title: '版本控制',
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
  ],
};

export default tbadvanced;
