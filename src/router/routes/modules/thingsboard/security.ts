import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbsecurity: AppRouteModule = {
  path: '/tb-security',
  name: 'Tbsecurity',
  component: LAYOUT,
  meta: {
    orderNo: 6019,
    icon: 'ion:shield-half-outline',
    title: '安全',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.SYS_ADMIN],
  },

  children: [
    {
      path: 'audit',
      name: 'Audit',
      component: () => import('/@/views/thingsborad/security/index.vue'),
      meta: {
        title: '审计日志',
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
    {
      path: 'basicSetting',
      name: 'BasicSetting',
      component: () => import('/@/views/thingsborad/security/basicSetting/index.vue'),
      meta: {
        title: '基本设置',
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
    {
      path: 'twoFactorAuthentication',
      name: 'TwoFactorAuthentication',
      component: () => import('/@/views/thingsborad/security/twoFactorAuthentication/index.vue'),
      meta: {
        title: '双因素认证',
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
    {
      path: 'OAuth2',
      name: 'OAuth2',
      component: () => import('/@/views/thingsborad/security/oauth2/index.vue'),
      meta: {
        title: 'OAuth2',
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
  ],
};

export default tbsecurity;
