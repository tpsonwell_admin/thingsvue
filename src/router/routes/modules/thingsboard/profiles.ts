import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbprofiles: AppRouteModule = {
  path: '/tb-profiles',
  name: 'tbprofiles',
  component: LAYOUT,
  meta: {
    orderNo: 6007,
    icon: 'ion:build-outline',
    title: '配置文件',
    roles: [RoleEnum.TENANT_ADMIN],
  },
  children: [
    {
      path: 'deviceFiles',
      name: 'DeviceFiles',
      meta: {
        title: '设备配置文件',
        roles: [RoleEnum.TENANT_ADMIN],
      },
      component: () => import('/@/views/thingsborad/profiles/deviceFiles/index.vue'),
    },
    {
      path: 'assetFiles',
      name: 'AssetFiles',
      meta: {
        title: '资产配置文件',
        roles: [RoleEnum.TENANT_ADMIN],
      },
      component: () => import('/@/views/thingsborad/profiles/assetFiles/index.vue'),
    },
  ],
};

export default tbprofiles;
