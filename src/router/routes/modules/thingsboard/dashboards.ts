import type { AppRouteModule } from '/@/router/types';
import { RoleEnum } from '/@/enums/roleEnum';

import { LAYOUT } from '/@/router/constant';

const tbdashboards: AppRouteModule = {
  path: '/tb-dashboards',
  name: 'tbdashboards',
  component: LAYOUT,
  redirect: '/tb-dashboards/dashboards',
  meta: {
    orderNo: 6004,
    hideChildrenInMenu: true,
    icon: 'ant-design:appstore-filled',
    title: '仪表盘',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
  },
  children: [
    {
      path: 'dashboards',
      name: 'TbdashboardsPage',
      component: () => import('/@/views/thingsborad/dashboards/index.vue'),
      meta: {
        title: '仪表盘',
        icon: 'ant-design:appstore-filled',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
    {
      path: 'ConfigureDashboard',
      name: 'ConfigureDashboard',
      component: () =>
        import('/@/views/thingsborad/dashboards/components/ConfigureDashboard/index.vue'),
      meta: {
        title: '配置仪表盘',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
  ],
};

export default tbdashboards;
