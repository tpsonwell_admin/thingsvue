import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbentities: AppRouteModule = {
  path: '/tb-entities',
  name: 'Tbentities',
  component: LAYOUT,
  meta: {
    orderNo: 6006,
    icon: 'material-symbols:category',
    title: '实体',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
  },

  children: [
    {
      path: 'device',
      name: 'Devices',
      component: () => import('/@/views/thingsborad/entity/device/index.vue'),
      meta: {
        title: '设备',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
    {
      path: 'asset',
      name: 'Asset',
      component: () => import('/@/views/thingsborad/entity/asset/index.vue'),
      meta: {
        title: '资产',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
    {
      path: 'views',
      name: 'Views',
      component: () => import('/@/views/thingsborad/entity/view/index.vue'),
      meta: {
        title: '实体视图',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
  ],
};

export default tbentities;
