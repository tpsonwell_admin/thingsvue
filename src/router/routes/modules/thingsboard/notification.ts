import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbnotification: AppRouteModule = {
  path: '/tb-notification',
  name: 'tbnotification',
  component: LAYOUT,
  redirect: '/tb-notification/index',
  meta: {
    orderNo: 6015,
    hideChildrenInMenu: true,
    icon: 'mdi:message-badge',
    title: '通知中心',
  },
  children: [
    {
      path: 'index',
      name: 'TbnotificationPage',
      component: () => import('/@/views/thingsborad/notification/index.vue'),
      meta: {
        title: '通知中心',
        icon: 'mdi:message-badge',
        hideMenu: true,
      },
    },
  ],
};

export default tbnotification;
