import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbresources: AppRouteModule = {
  path: '/tb-resources',
  name: 'Tbresources',
  component: LAYOUT,
  meta: {
    orderNo: 6014,
    icon: 'ic:baseline-folder',
    title: '资源',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.SYS_ADMIN],
  },

  children: [
    {
      path: 'widgets',
      name: 'Widgets',
      component: () => import('/@/views/thingsborad/resources/widgets/index.vue'),
      meta: {
        title: '部件库',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.SYS_ADMIN],
      },
    },
    {
      path: 'widgets-coding/:widgetId?',
      name: 'widgetsCoding',
      component: () =>
        import(
          '/@/views/thingsborad/resources/widgets/components/widgetsTable/components/EditorWidgets/index.vue'
        ),
      meta: {
        title: '编写部件',
        hideMenu: true,
      },
    },
    {
      path: 'resourses',
      name: 'Resourses',
      component: () => import('/@/views/thingsborad/resources/resourceLibrary/index.vue'),
      meta: {
        title: '资源库',
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.SYS_ADMIN],
      },
    },
  ],
};

export default tbresources;
