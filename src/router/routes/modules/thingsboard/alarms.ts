import type { AppRouteModule } from '/@/router/types';
import { RoleEnum } from '/@/enums/roleEnum';
import { LAYOUT } from '/@/router/constant';

const tbalarms: AppRouteModule = {
  path: '/tb-alarms',
  name: 'tbalarms',
  component: LAYOUT,
  redirect: '/tb-alarms/alarm',
  meta: {
    orderNo: 6002,
    hideChildrenInMenu: true,
    icon: 'tabler:alert-triangle-filled',
    title: '告警',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
  },
  children: [
    {
      path: 'alarm',
      name: 'alarm',
      component: () => import('/@/views/thingsborad/alarm/index.vue'),
      meta: {
        title: '告警',
        icon: 'tabler:alert-triangle-filled',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN, RoleEnum.CUSTOMER_USER],
      },
    },
  ],
};

export default tbalarms;
