import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbrule: AppRouteModule = {
  path: '/tb-rule',
  name: 'tbrule',
  component: LAYOUT,
  redirect: '/tb-rule/TbrulePage',
  meta: {
    orderNo: 6011,
    hideChildrenInMenu: true,
    icon: 'ic:sharp-settings-ethernet',
    title: '规则链库',
    roles: [RoleEnum.TENANT_ADMIN],
  },
  children: [
    {
      path: 'TbrulePage',
      name: 'TbrulePage',
      component: () => import('/@/views/thingsborad/rule/index.vue'),
      meta: {
        title: '规则链',
        icon: 'ic:sharp-settings-ethernet',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN],
      },
      children: [
        {
          path: 'editRuleChain',
          name: 'EditRuleChain',
          component: () => import('/@/views/thingsborad/rule/components/EditRuleChain/index.vue'),
          meta: {
            title: '绘制规则链',
            icon: 'ic:sharp-settings-ethernet',
            hideMenu: true,
            roles: [RoleEnum.TENANT_ADMIN],
          },
        },
        {
          path: 'rule-chain/:ruleChainId',
          name: 'Rulechains',
          component: () => import('/@/views/thingsborad/rule/drawRuleChain/index.vue'),
          meta: {
            single: true,
            icon: 'ant-design:subnode-outlined',
            tabIcon: 'ant-design:subnode-outlined',
            title: '绘制规则链',
          },
        },
      ],
    },
  ],
};

export default tbrule;
