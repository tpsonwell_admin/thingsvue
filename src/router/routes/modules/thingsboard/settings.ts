import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbsettings: AppRouteModule = {
  path: '/tb-settings',
  name: 'Tbsettings',
  component: LAYOUT,
  redirect: '/tb-settings/index',
  meta: {
    orderNo: 6018,
    hideChildrenInMenu: true,
    icon: 'ion:ios-settings',
    title: '设置',
    roles: [RoleEnum.TENANT_ADMIN, RoleEnum.SYS_ADMIN],
  },

  children: [
    {
      path: 'index',
      name: 'TbsettingsPage',
      component: () => import('/@/views/thingsborad/settings/index.vue'),
      meta: {
        title: '设置',
        icon: 'ion:ios-settings',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN],
      },
    },
    {
      path: 'index',
      name: 'AdminSettingsPage',
      component: () => import('/@/views/thingsborad/settings/adminSetting/index.vue'),
      meta: {
        title: '设置',
        icon: 'ion:ios-settings',
        hideMenu: true,
        roles: [RoleEnum.SYS_ADMIN],
      },
    },
  ],
};

export default tbsettings;
