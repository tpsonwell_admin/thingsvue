import { RoleEnum } from '/@/enums/roleEnum';
import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const tbapi: AppRouteModule = {
  path: '/tb-api',
  name: 'tbapi',
  component: LAYOUT,
  redirect: '/tb-api/index',
  meta: {
    orderNo: 6016,
    hideChildrenInMenu: true,
    icon: 'carbon:api-1',
    title: 'API使用统计',
    roles: [RoleEnum.TENANT_ADMIN],
  },
  children: [
    {
      path: 'index',
      name: 'TbapiPage',
      component: () => import('/@/views/thingsborad/api/index.vue'),
      meta: {
        hideChildrenInMenu: true,
        title: 'API使用统计',
        icon: 'carbon:api-1',
        hideMenu: true,
        roles: [RoleEnum.TENANT_ADMIN],
        ignoreKeepAlive: false,
      },
    },
    {
      path: 'api-detail',
      name: 'apiDetail',
      component: () => import('/@/views/thingsborad/api/components/Details.vue'),
      meta: {
        hideMenu: true,
        icon: 'carbon:api-1',
        title: 'API详情',
        roles: [RoleEnum.TENANT_ADMIN],
        hideTab: true,
        ignoreKeepAlive: false,
      },
    },
  ],
};

export default tbapi;
