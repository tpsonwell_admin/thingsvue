import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const tbaccount: AppRouteModule = {
  path: '/tb-account',
  name: 'tbaccount',
  component: LAYOUT,
  meta: {
    // hideChildrenInMenu: true,
    hideMenu: true,
    icon: 'material-symbols:person',
    title: '账户',
  },
  children: [
    {
      path: 'index',
      name: 'index',
      component: () => import('/@/views/thingsborad/account/index.vue'),
      meta: {
        title: '账户',
      },
    },
  ],
};

export default tbaccount;
