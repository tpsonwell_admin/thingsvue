import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
// 事件三种类型table
export const eventERRORTableSchema: BasicColumn[] = [
  {
    title: '事件时间',
    width: 100,
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '服务器',
    width: 100,
    dataIndex: 'server',
  },
  {
    title: '方法',
    width: 100,
    dataIndex: 'method',
  },
  {
    title: '错误',
    width: 100,
    dataIndex: 'error',
  },
];
export const eventLC_EVENTTableSchema: BasicColumn[] = [
  {
    title: '事件时间',
    width: 100,
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '服务器',
    width: 100,
    dataIndex: 'body',
    customRender: ({ value }) => {
      return value.server;
    },
  },
  {
    title: '事件',
    width: 100,
    dataIndex: 'body',
    customRender: ({ value }) => {
      return value.event;
    },
  },
  {
    title: '状态',
    width: 100,
    dataIndex: 'body',
    customRender: ({ value }) => {
      return value.success ? '成功' : '失败';
    },
  },
  {
    title: '错误',
    width: 100,
    dataIndex: 'error',
  },
];
export const eventSTATSTableSchema: BasicColumn[] = [
  {
    title: '事件时间',
    width: 100,
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '服务器',
    width: 100,
    dataIndex: 'server',
  },
  {
    title: '消息处理',
    width: 100,
    dataIndex: 'messaging',
  },
  {
    title: '错误发生',
    width: 100,
    dataIndex: 'errorOccurred',
  },
];

// 事件三种事件搜索表单
export const eventERRORFormSchema: FormSchema[] = [
  {
    field: 'server',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入服务器',
    },
  },
  {
    field: 'method',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入方法',
    },
  },
  {
    field: 'error',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入错误',
    },
  },
  {
    field: 'timeRange',
    label: '',
    component: 'RangePicker',
    componentProps: {
      placeholder: ['开始时间', '结束时间'],
      showTime: true,
    },
  },
];
export const eventLC_EVENTFormSchema: FormSchema[] = [
  {
    field: 'server',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入服务器',
    },
  },
  {
    field: 'event',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入事件',
    },
  },
  {
    field: 'state',
    label: '',
    component: 'Select',
    componentProps: {
      placeholder: '请选择状态',
      options: [
        {
          label: '全部',
          value: 'all',
        },
        {
          label: '成功',
          value: 'success',
        },
        {
          label: '错误',
          value: 'error',
        },
      ],
    },
  },
  {
    field: 'error',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入错误',
    },
  },
  {
    field: 'timeRange',
    label: '',
    component: 'RangePicker',
    componentProps: {
      placeholder: ['开始时间', '结束时间'],
      showTime: true,
    },
  },
];
export const eventSTATSFormSchema: FormSchema[] = [
  {
    field: 'server',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入服务器',
    },
  },
  {
    field: 'leastMessage',
    label: '',
    component: 'InputNumber',
    componentProps: {
      placeholder: '请输入最少处理消息数',
    },
  },
  {
    field: 'maximumMessage',
    label: '',
    component: 'InputNumber',
    componentProps: {
      placeholder: '请输入最大处理消息数',
    },
  },
  {
    field: 'leastError',
    label: '',
    component: 'InputNumber',
    componentProps: {
      placeholder: '请输入最少发生错误数',
    },
  },
  {
    field: 'maximumError',
    label: '',
    component: 'InputNumber',
    componentProps: {
      placeholder: '请输入最大发送错误数',
    },
  },
  {
    field: 'timeRange',
    label: '',
    component: 'RangePicker',
    componentProps: {
      placeholder: ['开始时间', '结束时间'],
      showTime: true,
    },
  },
];
