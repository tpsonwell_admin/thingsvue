import { FormSchema } from '/@/components/Form';
export const editAssociationSchemas: FormSchema[] = [
  {
    field: 'jsonValue',
    component: 'InputTextArea',
    label: '附加信息（JSON）',
    slot: 'jsonSlot',
    colProps: {
      span: 24,
    },
    rules: [
      {
        validator: (_, value) => {
          const isJSON = (value: string) => {
            try {
              const json = JSON.parse(value);
              return typeof json === 'object';
            } catch (err) {
              return false;
            }
          };
          if (!value) return Promise.resolve();
          if (!isJSON(value) || value == 'null') {
            return Promise.reject('请输入正确的JSON格式');
          }
          return Promise.resolve();
        },
      },
    ],
  },
];
