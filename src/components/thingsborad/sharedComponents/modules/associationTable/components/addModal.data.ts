import { FormSchema } from '/@/components/Form';
import {
  getEntityDeviceList,
  getTenantAssetInfos,
  getTenantEntityViews,
  getTenant,
  getCustomerList,
  getUsers,
  getDashboards,
  getEdge,
} from '/@/api/thingsborad/entity/entity';

export const addAssociationSchemas: FormSchema[] = [
  {
    field: 'associationType',
    component: 'Select',
    componentProps: {
      placeholder: '请选择关联类型',
      options: [
        {
          label: 'Contains',
          value: 'Contains',
        },
        {
          label: 'Manages',
          value: 'Manages',
        },
      ],
    },
    label: '关联类型',
    required: true,
  },
  {
    field: 'toEntity',
    component: 'Select',
    componentProps: ({ formActionType }) => {
      return {
        listHeight: 160,
        placeholder: '请选择类型',
        options: [
          { label: '设备', value: 'DEVICE' },
          { label: '资产', value: 'ASSET' },
          { label: '实体视图', value: 'ENTITY_VIEW' },
          { label: '租户', value: 'TENANT' },
          { label: '客户', value: 'CUSTOMER' },
          { label: '用户', value: 'USER' },
          { label: '仪表盘', value: 'DASHBOARD' },
          { label: '边缘', value: 'EDGE' },
        ],
        onChange: (val) => {
          const { updateSchema } = formActionType;
          let api: any;
          let params: any = {
            page: 0,
            pageSize: 50,
            sortProperty: 'createdTime',
            sortOrder: 'ASC',
          };
          switch (val) {
            case 'DEVICE':
              api = getEntityDeviceList;
              break;
            case 'ASSET':
              api = getTenantAssetInfos;
              break;
            case 'ENTITY_VIEW':
              api = getTenantEntityViews;
              break;
            case 'TENANT':
              api = getTenant;
              params = '租户id';
              break;
            case 'CUSTOMER':
              api = getCustomerList;
              break;
            case 'USER':
              api = getUsers;
              break;
            case 'DASHBOARD':
              api = getDashboards;
              break;
            case 'EDGE':
              api = getEdge;
              break;
          }
          updateSchema({
            field: 'entityList',
            componentProps: {
              placeholder: '请选择实体',
              listHeight: 160,
              mode: 'tags',
              showSearch: true,
              showArrow: false,
              api,
              params,
              resultField: 'data',
              labelField: 'name',
              valueField: 'id.id',
              immediate: true,
            },
          });
        },
      };
    },
    label: '到实体',
    required: true,
  },
  {
    field: 'entityList',
    component: 'ApiSelect',
    label: '实体',
    componentProps: {
      placeholder: '请选择实体',
    },
    ifShow: ({ values }) => {
      return values?.toEntity;
    },
    required: true,
  },
  {
    field: 'jsonValue',
    component: 'InputTextArea',
    label: '附加信息（JSON）',
    componentProps: {
      placeholder: '请输入附加信息（JSON）',
    },
    slot: 'jsonSlot',
    colProps: {
      span: 24,
    },
    rules: [
      {
        validator: (_, value) => {
          const isJSON = (value: string) => {
            try {
              const json = JSON.parse(value);
              return typeof json === 'object';
            } catch (err) {
              return false;
            }
          };
          if (!value) return Promise.resolve();
          if (!isJSON(value) || value == 'null') {
            return Promise.reject('请输入正确的JSON格式');
          }
          return Promise.resolve();
        },
      },
    ],
  },
];
