import { BasicColumn, FormSchema } from '/@/components/Table';
// 关联两种类型table
export const associationFromTableSchema: BasicColumn[] = [
  {
    title: '类型',
    width: 100,
    dataIndex: 'type',
  },
  {
    title: '到实体类型',
    width: 100,
    dataIndex: 'toEntityType',
    customRender: ({ record }) => {
      let reText = '';
      switch (record.toEntityType) {
        case 'DEVICE':
          reText = '设备';
          break;
        case 'ASSET':
          reText = '资产';
          break;
        case 'ENTITY_VIEW':
          reText = '实体视图';
          break;
        case 'TENANT':
          reText = '租户';
          break;
        case 'CUSTOMER':
          reText = '客户';
          break;
        case 'USER':
          reText = '用户';
          break;
        case 'DASHBOARD':
          reText = '仪表盘';
          break;
        case 'EDGE':
          reText = '边缘';
          break;
      }
      return reText;
    },
  },
  {
    title: '到实体名称',
    width: 100,
    dataIndex: 'toEntityName',
    customRender: ({ record }) => {
      return record.toEntityName;
    },
  },
];
export const associationToTableSchema: BasicColumn[] = [
  {
    title: '类型',
    width: 100,
    dataIndex: 'type',
  },
  {
    title: '从实体类型',
    width: 100,
    dataIndex: 'fromEntityType',
    customRender: ({ record }) => {
      let reText = '';
      switch (record.fromEntityType) {
        case 'DEVICE':
          reText = '设备';
          break;
        case 'ASSET':
          reText = '资产';
          break;
        case 'ENTITY_VIEW':
          reText = '实体视图';
          break;
        case 'TENANT':
          reText = '租户';
          break;
        case 'CUSTOMER':
          reText = '客户';
          break;
        case 'USER':
          reText = '用户';
          break;
        case 'DASHBOARD':
          reText = '仪表盘';
          break;
        case 'EDGE':
          reText = '边缘';
          break;
      }
      return reText;
    },
  },
  {
    title: '从实体名称',
    width: 100,
    dataIndex: 'fromEntityName',
    customRender: ({ record }) => {
      return record.fromEntityName;
    },
  },
];

export const associationDataFormSchema: FormSchema[] = [
  {
    field: 'keyName',
    label: '',
    component: 'Input',
    componentProps: ({ tableAction }) => {
      return {
        placeholder: '请输入检索条件',
        onChange: ({ target }) => {
          // if (timeout) {
          //   clearTimeout(timeout);
          //   timeout = null;
          // }
          // timeout = setTimeout(filterFunc.bind(null, target.value), 300);
        },
      };
    },
  },
];
