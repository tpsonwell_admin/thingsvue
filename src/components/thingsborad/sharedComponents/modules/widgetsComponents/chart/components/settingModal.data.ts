import { FormSchema } from '/@/components/Form/index';
import { getEntityDeviceList } from '/@/api/thingsborad/entity/entity';
import { useDebounceFn } from '@vueuse/shared';
import { UseFormReturnType, useForm, FormProps } from '/@/components/Form';
import { buildUUID } from '/@/utils/uuid';
import { ref } from 'vue';

export const schemas: FormSchema[] = [
  {
    field: 'Divider1',
    label: '数据源',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'type',
    label: '类型',
    component: 'Select',
    componentProps: ({ formActionType }) => {
      return {
        placeholder: '请选择类型',
        allowClear: false,
        options: [
          {
            label: '设备',
            value: 'device',
          },
          {
            label: '告警',
            value: 'alarm',
          },
        ],
        onChange: () => {
          const { setFieldsValue } = formActionType;
          setFieldsValue({
            deviceId: undefined,
          });
        },
      };
    },
    defaultValue: 'device',
    required: true,
  },
  {
    field: 'deviceId',
    label: '设备',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        pageSize: 50,
        page: 0,
        textSearch: '',
        sortProperty: 'name',
        sortOrder: 'ASC',
      });
      return {
        placeholder: '请选择设备',
        listHeight: 160,
        api: getEntityDeviceList,
        params: params.value,
        resultField: 'data',
        labelField: 'name',
        valueField: 'id',
        objectToJson: true,
        showSearch: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
        immediate: true,
      };
    },
    ifShow: ({ values }) => values.type === 'device',
    defaultValue: undefined,
    required: true,
  },
  {
    field: 'Divider2',
    label: '图表配置',
    component: 'Divider',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'chartType',
    label: '图表类型',
    component: 'Select',
    componentProps: {
      allowClear: false,
      options: [
        {
          label: '折线图',
          value: 'line',
        },
        {
          label: '柱状图',
          value: 'bar',
        },
        {
          label: '饼图',
          value: 'pie',
        },
        {
          label: '地图',
          value: 'map',
        },
      ],
    },
    defaultValue: 'line',
    required: true,
  },
  {
    field: 'series',
    label: 'Series',
    component: 'Input',
    slot: 'seriesSlot',
    colProps: {
      span: 24,
    },
    ifShow: ({ model }) => model.deviceId,
    defaultValue: [],
  },
];
export const seriesFormSchema: FormSchema[] = [
  {
    field: 'key',
    label: '',
    component: 'ApiSelect',
    colProps: {
      span: 6,
      style: {
        marginRight: '20px',
      },
    },
  },
  {
    field: 'name',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入图例名称',
    },
    colProps: {
      span: 6,
      style: {
        marginRight: '20px',
      },
    },
  },
  {
    field: 'color',
    label: '',
    componentProps: {
      placeholder: '请输入图例颜色',
    },
    component: 'Input',
    colProps: {
      span: 6,
    },
  },
  {
    field: 'deleteBtn',
    label: '',
    component: 'Input',
    slot: 'deleteSlot',
    colProps: {
      span: 4,
      style: {
        textAlign: 'center',
      },
    },
  },
];

export type FormItem = {
  key: string;
  Form: UseFormReturnType;
};
export const baseFormConfig: Partial<FormProps> = {
  showActionButtonGroup: false,
};
export const seriesFormList: FormItem[] = [
  {
    key: buildUUID(),
    Form: useForm(Object.assign({ schemas: seriesFormSchema }, baseFormConfig) as FormProps),
  },
];
