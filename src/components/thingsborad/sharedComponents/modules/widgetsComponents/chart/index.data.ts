import type { EChartsOption } from 'echarts';
import { reflectOptions } from '/@/views/thingsborad/dashboards/components/ConfigureDashboard/components/modalClassify/chartModal/chartModal.data';
export function publicChartOptions(formData: Recordable): EChartsOption {
  const { dataZoom, _theme, tooltip, grid } = formData;
  return {
    grid: {
      left: `${grid.left}%`,
      right: `${grid.right}%`,
      bottom: `${grid.bottom}%`,
      top: `${grid.top}%`,
      containLabel: true,
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'shadow',
      },
      ...tooltip,
    },
    dataZoom,
    xAxis: {
      type: 'time',
      // minInterval: 3600 * 1000 * 24,
      // maxInterval: 3600 * 1000 * 24,
      // boundaryGap: false,
      // axisLabel: {
      //   formatter: {
      //     year: '{yyyy}',
      //     month: '{MMM}',
      //     day: '{d}',
      //     hour: '{HH}:{mm}',
      //     minute: '{HH}:{mm}',
      //     second: '{HH}:{mm}:{ss}',
      //     millisecond: '{hh}:{mm}:{ss} {SSS}',
      //     none: '{yyy}-{MM}-{dd} {hh}:{mm}:{ss} {SSS}',
      //   },
      // },
    },
    yAxis: {
      type: 'value',
    },
  };
}
// 设置示例数据
export function setChartExampleData(formData: Recordable) {
  let options: EChartsOption = {};
  if (formData.chartType == 'line' || formData.chartType == 'bar') {
    options = {
      ...publicChartOptions(formData),
      legend: {
        orient: formData.legend.orient,
        type: 'scroll',
        left: `${formData.legend.left}%`,
        right: `${formData.legend.right}%`,
        bottom: `${formData.legend.bottom}%`,
        top: `${formData.legend.top}%`,
        data: ['示例1', '示例2'],
      },
      series: [
        {
          name: '示例1',
          data: [
            [1713862741820, 10],
            [1713863741820, 20],
            [1713864741820, 30],
            [1713865741820, 40],
            [1713866741820, 0],
            [1713867741820, 50],
          ],
          type: formData.chartType,
          itemStyle: {
            color: 'red',
          },
        },
        {
          name: '示例2',
          data: [
            [1713862741820, 40],
            [1713863741820, 32],
            [1713864741820, 69],
            [1713865741820, 19],
            [1713866741820, 30],
            [1713867741820, 50],
          ],
          type: formData.chartType,
          itemStyle: {
            color: 'blue',
          },
        },
      ],
    };
  } else if (formData.chartType == 'pie') {
    const { name, chartType, radius, tooltip, emphasis } = formData;
    options = {
      tooltip: {
        backgroundColor: tooltip.backgroundColor,
        borderColor: tooltip.borderColor,
        borderWidth: tooltip.borderWidth,
        textStyle: {
          color: tooltip.textStyle.color,
          fontSize: tooltip.textStyle.fontSize,
          fontWeight: tooltip.textStyle.fontWeight,
        },
      },
      series: [
        {
          name: name,
          type: chartType,
          radius: `${radius}%`,
          data: [
            { value: 1048, name: '示例1' },
            { value: 735, name: '示例2' },
            { value: 580, name: '示例3' },
            { value: 484, name: '示例4' },
            { value: 300, name: '示例5' },
          ],
          emphasis: {
            itemStyle: {
              shadowBlur: emphasis.itemStyle.shadowBlur,
              shadowOffsetX: 0,
              shadowColor: emphasis.itemStyle.shadowColor,
            },
          },
        },
      ],
    };
  }
  return {
    options,
  };
}

// ws第一次发送消息的处理函数
export const processingChartPublicFormData = (cmdId: number, formData: Recordable) => {
  const sendMsg: Recordable = {
    cmdId,
    query: {
      pageLink: {
        pageSize: 1024,
        page: 0,
        sortOrder: {
          key: {
            type: 'ENTITY_FIELD',
            key: 'createdTime',
          },
          direction: 'DESC',
        },
      },
      entityFields: [
        {
          type: 'ENTITY_FIELD',
          key: 'name',
        },
        {
          type: 'ENTITY_FIELD',
          key: 'label',
        },
        {
          type: 'ENTITY_FIELD',
          key: 'additionalInfo',
        },
      ],
      latestValues: [],
      entityFilter: {
        type: 'singleEntity',
        singleEntity: JSON.parse(formData.deviceId),
      },
    },
  };
  if (formData.chartType === 'line' || formData.chartType == 'bar') {
    sendMsg.query.latestValues = [];
  } else if (formData.chartType == 'pie') {
    formData.series.forEach((item: Recordable) => {
      sendMsg.query.latestValues.push(JSON.parse(item.key));
    });
  }

  return {
    entityDataCmds: [sendMsg],
  };
};
// ws第二次发送消息的处理函数
export const processingChartPrivateFormData = (cmdId: number, formData: Recordable) => {
  const keys: string[] = [];
  let sendMsg: Recordable = {};
  if (formData.chartType === 'line' || formData.chartType == 'bar') {
    formData.series.forEach((item: Recordable) => {
      keys.push(JSON.parse(item.key).key);
    });
    const { agg, limit, times, interval } = formData;
    const endTs = new Date().getTime();
    const startTs = endTs - reflectOptions[times].startTs;
    sendMsg = {
      cmdId,
      historyCmd: {
        keys,
        startTs,
        endTs,
        interval,
        limit: !!limit ? limit : reflectOptions[times].startTs / interval,
        agg,
      },
    };
  }
  return {
    entityDataCmds: [sendMsg],
  };
};

// ws第一次获得数据的处理函数
export const processingChartPublicReceiveData = (data: Recordable, formData: Recordable) => {
  let options: EChartsOption = {};
  const { name, chartType, radius, emphasis, tooltip } = formData;
  
  options = {
    tooltip: {
      backgroundColor: tooltip.backgroundColor,
      borderColor: tooltip.borderColor,
      borderWidth: tooltip.borderWidth,
      textStyle: {
        color: tooltip.textStyle.color,
        fontSize: tooltip.textStyle.fontSize,
        fontWeight: tooltip.textStyle.fontWeight,
      },
    },
    series: [
      {
        name: name,
        type: chartType,
        radius: `${radius}%`,
        data: [
          { value: 1048, name: '示例1' },
          { value: 735, name: '示例2' },
          { value: 580, name: '示例3' },
          { value: 484, name: '示例4' },
          { value: 300, name: '示例5' },
        ],
        emphasis: {
          itemStyle: {
            shadowBlur: emphasis.itemStyle.shadowBlur,
            shadowOffsetX: 0,
            shadowColor: emphasis.itemStyle.shadowColor,
          },
        },
      },
    ],
  };
  return { options, title: formData.cardTitle };
};
// ws第二次获得数据的处理函数
export const processingChartPrivateReceiveData = (data: Recordable, formData: Recordable) => {
  let legendKeys: string[] = [];
  const series: Recordable[] = [];
  let options: EChartsOption = {};
  if (formData.chartType == 'line' || formData.chartType == 'bar') {
    if (data.update) {
      // 图例数组
      legendKeys = Object.keys(data.update[0].timeseries);
      // 图例对应数据
      formData.series.forEach((item: Recordable) => {
        const seriesData: string[][] = [];
        data.update[0].timeseries[JSON.parse(item.key).key].forEach((item: Recordable) => {
          seriesData.push(Object.values(item));
        });
        series.push({
          name: JSON.parse(item.key).key,
          data: seriesData,
          type: formData.chartType,
          itemStyle: {
            color: item.color,
          },
        });
      });
    }
    options = {
      ...publicChartOptions(formData),
      legend: {
        orient: formData.legend.orient,
        type: 'scroll',
        left: `${formData.legend.left}%`,
        top: `${formData.legend.top}%`,
        data: legendKeys,
      },
      series,
    };
  } else if (formData.chartType == 'pie') {
    if (data.data) {
    }
  }
  return { options, title: formData.cardTitle };
};
