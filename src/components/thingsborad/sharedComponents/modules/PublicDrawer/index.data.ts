export interface TabItem {
  key: string;
  name: string;
}
export const tabListData: TabItem[] = [
  {
    key: 'detail',
    name: '详情',
  },
  {
    key: 'attribute',
    name: '属性',
  },
  {
    key: 'telemetry',
    name: '最新遥测数据',
  },
  {
    key: 'alarm',
    name: '告警',
  },
  {
    key: 'event',
    name: '事件',
  },
  {
    key: 'association',
    name: '关联',
  },
  {
    key: 'auditLogs',
    name: '审计日志',
  },
  {
    key: 'version',
    name: '版本控制',
  },
  {
    key: 'help',
    name: '帮助',
  },
];

export const enum ENTITY_TYPE {
  DEVICE = 'DEVICE',
  ASSET = 'ASSET',
  ALARM = 'ALARM',
  ENTITY_VIEW = 'ENTITY_VIEW',
  CUSTOMER = 'CUSTOMER',
  EDGE_INSTANCE = 'EDGE',
  RESOUCRCE_LIBRARY = 'RESOUCRCE_LIBRARY',
  RULE_CHAIN = 'RULE_CHAIN',
  DEVICE_PROFILE = 'DEVICE_PROFILE',
  ASSET_PROFILE = 'ASSET_PROFILE',
  OTA = 'OTA',
  TENANT = 'TENANT',
  TENANT_PROFILE = 'TENANT_PROFILE',
  QUEUES = 'QUEUES',
  DASHBOARD = 'DASHBOARD',
  TANANT_USER = 'TANANT_USER',
  CUSTOMER_USER = 'CUSTOMER_USER',
  WIDGET_TYPE = 'WIDGET_TYPE',
  WIDGETS_BUNDLE = 'WIDGETS_BUNDLE',
  RULE_NODE = 'RULE_NODE',
}
