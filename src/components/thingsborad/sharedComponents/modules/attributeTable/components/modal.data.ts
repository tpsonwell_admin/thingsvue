import { FormSchema } from '/@/components/Form';
// 添加属性键值对
export const addKeyValueSchema: FormSchema[] = [
  {
    field: 'keyName',
    label: '键名',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入键名',
      };
    },
    ifShow: () => {
      return true;
    },
    required: true,
  },
  {
    field: 'valueType',
    label: '值类型',
    component: 'Select',
    componentProps: {
      placeholder: '请选择值类型',
      listHeight: 160,
      allowClear: false,
      options: [
        { label: '字符串', value: 'string' },
        { label: '整数', value: 'integer' },
        { label: '双精度小数', value: 'decimal' },
        { label: '布尔值', value: 'boolean' },
        { label: 'JSON', value: 'json' },
      ],
    },
    required: true,
    defaultValue: 'string',
  },
  {
    field: 'stringValue',
    label: '字符串值',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入字符串值',
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'string';
    },
    required: true,
  },
  {
    field: 'integerValue',
    label: '整数值',
    component: 'InputNumber',
    componentProps: () => {
      return {
        placeholder: '请输入整数值',
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'integer';
    },
    required: true,
  },
  {
    field: 'decimalValue',
    label: '双精度小数',
    component: 'InputNumber',
    componentProps: () => {
      return {
        placeholder: '请输入双精度小数',
        stringMode: true,
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'decimal';
    },
    required: true,
  },
  {
    field: 'booleanValue',
    label: '假',
    component: 'Checkbox',
    componentProps: ({ formActionType }) => {
      return {
        onChange: (e: any) => {
          const { updateSchema } = formActionType;
          updateSchema({ field: 'booleanValue', label: e.target.checked ? '真' : '假' });
        },
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'boolean';
    },
    defaultValue: false,
  },
  {
    field: 'jsonValue',
    label: 'JSON',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入JSON值',
      };
    },
    rules: [
      {
        required: true,
        validator: (_, value) => {
          const isJSON = (value: string) => {
            try {
              const json = JSON.parse(value);
              return typeof json === 'object';
            } catch (err) {
              return false;
            }
          };
          if (!value) return Promise.reject('不能为空');
          if (!isJSON(value) || value == 'null' || value == '{}') {
            return Promise.reject('请输入正确的JSON格式');
          }
          return Promise.resolve();
        },
      },
    ],
    defaultValue: '{}',
    ifShow: (value) => {
      return value?.model?.valueType == 'json';
    },
  },
];
// 编辑属性键值对
export const editAttributeSchema: FormSchema[] = [
  {
    field: 'valueType',
    label: '值类型',
    component: 'Select',
    componentProps: () => {
      return {
        placeholder: '请选择值类型',
        listHeight: 160,
        allowClear: false,
        options: [
          { label: '字符串', value: 'string' },
          { label: '整数', value: 'integer' },
          { label: '双精度小数', value: 'decimal' },
          { label: '布尔值', value: 'boolean' },
          { label: 'JSON', value: 'json' },
        ],
      };
    },
    required: true,
  },
  {
    field: 'stringValue',
    label: '字符串值',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入字符串值',
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'string';
    },
    required: true,
  },
  {
    field: 'integerValue',
    label: '整数值',
    component: 'InputNumber',
    componentProps: () => {
      return {
        placeholder: '请输入整数值',
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'integer';
    },
    required: true,
  },
  {
    field: 'decimalValue',
    label: '双精度小数',
    component: 'InputNumber',
    componentProps: () => {
      return {
        placeholder: '请输入双精度小数',
      };
    },
    ifShow: (value) => {
      return value?.model?.valueType == 'decimal';
    },
    required: true,
  },
  {
    field: 'booleanValue',
    label: '假',
    component: 'Checkbox',
    ifShow: (value) => {
      return value?.model?.valueType == 'boolean';
    },
    defaultValue: false,
    required: true,
  },
  {
    field: 'jsonValue',
    label: 'JSON',
    component: 'Input',
    componentProps: () => {
      return {
        placeholder: '请输入JSON值',
      };
    },
    defaultValue: '{}',
    ifShow: (value) => {
      return value?.model?.valueType == 'json';
    },
    rules: [
      {
        required: true,
        validator: (_, value) => {
          const isJSON = (value: string) => {
            try {
              const json = JSON.parse(value);
              return typeof json === 'object';
            } catch (err) {
              return false;
            }
          };
          if (!value) return Promise.reject('不能为空');
          if (!isJSON(value) || value == 'null' || value == '{}') {
            return Promise.reject('请输入正确的JSON格式');
          }
          return Promise.resolve();
        },
      },
    ],
    required: true,
  },
];
