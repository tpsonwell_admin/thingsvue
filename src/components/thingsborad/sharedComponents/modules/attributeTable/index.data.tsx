import { BasicColumn } from '/@/components/Table/src/types/table';
import { Icon } from '/@/components/Icon';
import { useMessage } from '/@/hooks/web/useMessage';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import { FormSchema } from '/@/components/Form';
import { StyleValue } from 'vue';

// 表格配置项
export const tableSchema: BasicColumn[] = [
  {
    title: '最后更新时间',
    width: 100,
    dataIndex: 'lastUpdateTs',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: (a: any, b: any) => {
      const { lastUpdateTs: aLastUpdateTs } = a;
      const { lastUpdateTs: bLastUpdateTs } = b;
      return aLastUpdateTs - bLastUpdateTs;
    },
  },
  {
    title: '键名',
    width: 100,
    dataIndex: 'key',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };

      return (
        <div>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <span style={style}>{options.value}</span>
            <a-button onClick={copy} type="text" size="small" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </a-button>
          </div>
        </div>
      );
    },
  },
  {
    title: '数值',
    width: 100,
    dataIndex: 'value',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };
      return (
        <div>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <span style={style}>{options.value}</span>
            <a-button onClick={copy} type="text" size="small" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </a-button>
          </div>
        </div>
      );
    },
  },
];
// 表单配置项
export const formSchema: FormSchema[] = [
  {
    field: 'keyName',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入检索条件',
    },
  },
];

// 下拉框options
export const options = [
  { label: '客户端属性', value: 'CLIENT_SCOPE' },
  { label: '服务端属性', value: 'SERVER_SCOPE' },
  { label: '共享属性', value: 'SHARED_SCOPE' },
];
