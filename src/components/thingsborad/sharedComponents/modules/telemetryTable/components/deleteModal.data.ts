import { FormSchema } from '/@/components/Form';
export const deleteTelemetrySchemas: FormSchema[] = [
  {
    field: 'deleteStrategy',
    component: 'Select',
    label: '删除策略',
    componentProps: {
      placeholder: '请选择删除策略',
      listHeight: 160,
      allowClear: false,
      options: [
        { label: 'Delete all data', value: 'DeleteAllData' },
        { label: 'Delete all data except latest value', value: 'DeleteAllDataExceptLatestValue' },
        { label: 'Delete latest value', value: 'DeleteLatestValue' },
        { label: 'Delete all data for time period', value: 'DeleteAllDataForTimePeriod' },
      ],
    },
    defaultValue: 'DeleteAllData',
  },
  {
    field: 'rewrite',
    component: 'Switch',
    label: 'Rewrite latest value',
    ifShow: (value) => {
      return (
        value?.model?.deleteStrategy == 'DeleteLatestValue' ||
        value?.model.deleteStrategy == 'DeleteAllDataForTimePeriod'
      );
    },
    defaultValue: true,
  },
  {
    field: 'startTime',
    component: 'DatePicker',
    label: 'endTime',
    ifShow: (value) => {
      return value?.model.deleteStrategy == 'DeleteAllDataForTimePeriod';
    },
    defaultValue: true,
  },
  {
    field: 'endTime',
    component: 'DatePicker',
    label: 'startTime',
    ifShow: (value) => {
      return value?.model.deleteStrategy == 'DeleteAllDataForTimePeriod';
    },
    defaultValue: true,
  },
];
