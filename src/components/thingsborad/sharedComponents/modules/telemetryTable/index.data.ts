import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Form';
// 遥测数据
export const telemetryTableSchema: BasicColumn[] = [
  {
    title: '最后更新时间',
    width: 100,
    dataIndex: 'lastUpdateTs',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: (a: any, b: any) => {
      const { lastUpdateTs: aLastUpdateTs } = a;
      const { lastUpdateTs: bLastUpdateTs } = b;
      return aLastUpdateTs - bLastUpdateTs;
    },
  },
  {
    title: '键名',
    width: 100,
    dataIndex: 'key',
  },
  {
    title: '键值',
    width: 100,
    dataIndex: 'value',
  },
];
// 表单配置项
export const formSchema: FormSchema[] = [
  {
    field: 'keyName',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入检索条件',
    },
  },
];
