import { FormSchema } from '/@/components/Form';
import { getEntityDeviceList } from '/@/api/thingsborad/entity/entity';
import { getEntityKeys } from '/@/api/thingsborad/dashboards/dashboard';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/shared';
export const settingSchema: FormSchema[] = [
  {
    field: 'device',
    label: '设备',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        pageSize: 50,
        page: 0,
        textSearch: '',
        sortProperty: 'name',
        sortOrder: 'ASC',
      });
      return {
        listHeight: 160,
        api: getEntityDeviceList,
        params: params.value,
        resultField: 'data',
        labelField: 'title',
        valueField: 'id',
        objectToJson: true,
        showSearch: true,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
        immediate: true,
      };
    },
  },
  {
    field: 'keys',
    label: 'Data Keys',
    component: 'ApiSelect',
    componentProps: (opt) => {
      // const { getFieldsValue } = formModel;
      return {
        listHeight: 160,
        api: getEntityKeys,
        params: {
          payload: {
            entityFilter: {
              type: 'singleEntity',
              singleEntity: {
                entityType: 'DEVICE',
                id: '527da070-dd24-11ee-bd5d-eb7b38a2de3e',
              },
            },
            pageLink: {
              pageSize: 100,
              page: 0,
              sortOrder: {
                key: {
                  type: 'ENTITY_FIELD',
                  key: 'createdTime',
                },
                direction: 'DESC',
              },
            },
          },
          query: {
            attributes: true,
            timeseries: true,
          },
        },
        resultField: 'data',
        labelField: 'title',
        valueField: 'id',
        objectToJson: true,
        showSearch: true,
        immediate: true,
      };
    },
  },
];
