import { UseFormReturnType } from '/@/components/Form';
import { FormSchema } from '/@/components/Form/index';

export interface ListItem {
  id: string;
  avatar: string;
  // 通知的标题内容
  title: string;
  // 是否在标题上显示删除线
  titleDelete?: boolean;
  datetime: string;
  type: string;
  read?: boolean;
  description: string;
  clickClose?: boolean;
  extra?: string;
  color?: string;
}

export interface TabItem {
  key: string;
  name: string;
  Form?: UseFormReturnType;
  unreadlist?: ListItem[];
}

export const tabListData: TabItem[] = [
  {
    key: '1',
    name: 'LWM2M模式',
  },
  {
    key: '2',
    name: 'BootStrap',
  },
  {
    key: '3',
    name: '其他设置',
  },
  {
    key: '4',
    name: '设备配置JSON',
  },
];

export const SettingSchemas: FormSchema[] = [
  {
    field: 'fwUpdateStrategy',
    label: '固件升级策略',
    component: 'Select',
    colProps: {
      span: 24,
    },
    defaultValue: 1,
    componentProps: {
      allowClear: false,
      options: [
        { label: '发布固件升级二制文件使用Object 5和Resource 0包', value: 1 },
        {
          label: '自动生成唯一的CoAP地址下载包和发布软件更新作为Object 5和Resource 1(软件包URI)。',
          value: 2,
        },
        { label: '发布固件升级二制文件使用Object 19和Resource 0数据。', value: 3 },
      ],
    },
  },
  {
    field: 'fwUpdateResource',
    label: '固件更新COAP资源',
    component: 'Input',
    required: true,
    defaultValue: 'coap://localhost:5685',
    colProps: {
      span: 24,
    },
    show: (value) => {
      if (value?.model.fwUpdateStrategy === 2) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'swUpdateStrategy',
    label: '软件更新策略',
    component: 'Select',
    colProps: {
      span: 24,
    },
    defaultValue: 1,
    componentProps: {
      allowClear: false,
      options: [
        { label: '发布二制文件使用Object 9和Resource 2(包)', value: 1 },
        {
          label: '自动生成唯一的CoAP地址下载包和发布软件更新作为Object 9和Resource 3(软件包URI)。',
          value: 2,
        },
      ],
    },
  },
  {
    field: 'swUpdateResource',
    label: '软件更新COAP资源',
    component: 'Input',
    required: true,
    defaultValue: 'coap://localhost:5685',
    colProps: {
      span: 24,
    },
    show: (value) => {
      if (value?.model.swUpdateStrategy === 2) {
        return true;
      } else {
        return false;
      }
    },
  },
  // {
  //   field: 'clientSettings',
  //   component: 'Input',
  //   label: '',
  //   slot: 'clientSettings',
  //   colProps: {
  //     span: 24,
  //   },
  // },
  {
    field: 'powerMode',
    component: 'Select',
    label: '节能模式',
    colProps: {
      span: 24,
    },
    defaultValue: 'DRX',
    componentProps: {
      allowClear: false,
      options: [
        { label: '节能模式(PSM)', value: 'PSM' },
        { label: '非连续接收(DRX)', value: 'DRX' },
        { label: '连续接收(eDRX)', value: 'E_DRX' },
      ],
    },
  },
  {
    field: 'psmActivityTimer',
    component: 'InputNumber',
    label: 'PSM活动计时器',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    componentProps: {
      min: '2',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnitsPsm',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'edrxCycle',
    component: 'InputNumber',
    label: 'eDRX循环',
    colProps: {
      span: 12,
    },
    defaultValue: 81,
    componentProps: {
      min: '6',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnitsCyc',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'pagingTransmissionWindow',
    component: 'InputNumber',
    label: '分页传输窗口',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    componentProps: {
      min: '2',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnitsTra',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'compositeOperationsSupport',
    component: 'Switch',
    label: '支持复合读取/写入/观察操作',
    defaultValue: false,
    colProps: {
      span: 24,
    },
  },
];

export const clientSettingsSchemas: FormSchema[] = [
  {
    field: 'powerMode',
    component: 'Select',
    label: '节能模式',
    colProps: {
      span: 24,
    },
    defaultValue: 'DRX',
    componentProps: {
      allowClear: false,
      options: [
        { label: '节能模式(PSM)', value: 'PSM' },
        { label: '非连续接收(DRX)', value: 'DRX' },
        { label: '连续接收(eDRX)', value: 'E_DRX' },
      ],
    },
  },
  {
    field: 'psmActivityTimer',
    component: 'InputNumber',
    label: 'PSM活动计时器',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    componentProps: {
      min: '2',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnits1',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'edrxCycle',
    component: 'InputNumber',
    label: 'eDRX循环',
    colProps: {
      span: 12,
    },
    defaultValue: 81,
    componentProps: {
      min: '6',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnits2',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'pagingTransmissionWindow',
    component: 'InputNumber',
    label: '分页传输窗口',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    componentProps: {
      min: '2',
      parser: formatNumber,
    },
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'timeUnits3',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    show: (value) => {
      if (value?.model.powerMode === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
];

export const LWM2MServerSchemas: FormSchema[] = [
  {
    field: 'securityMode',
    component: 'Select',
    label: '安全配置模式',
    defaultValue: 'NO_SEC',
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      options: [
        { label: 'Pre-Shared Key', value: 'PSK' },
        { label: 'Raw Public Key', value: 'RPK' },
        { label: 'X.509 Certificate', value: 'X509' },
        { label: 'No Security', value: 'NO_SEC' },
      ],
      allowClear: false,
    },
  },
  {
    field: 'shortServerId',
    component: 'InputNumber',
    label: '服务器ID',
    defaultValue: 123,
    required: true,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 1,
      max: 65534,
      parser: formatNumber,
    },
  },
  {
    field: 'host',
    component: 'Input',
    label: '主机',
    defaultValue: '0.0.0.0',
    required: true,
    colProps: {
      span: 12,
    },
  },
  {
    field: 'port',
    component: 'InputNumber',
    label: '端口',
    defaultValue: 5685,
    required: true,
    colProps: {
      span: 12,
    },
  },
  {
    field: 'clientHoldOffTime',
    component: 'InputNumber',
    label: '停留时间',
    required: true,
    defaultValue: 1,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
      parser: formatNumber,
    },
  },
  {
    field: 'bootstrapServerAccountTimeout',
    component: 'InputNumber',
    label: '账户超时',
    required: true,
    defaultValue: 0,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
      parser: formatNumber,
    },
  },
  {
    field: 'serverPublicKey',
    component: 'Input',
    label: '服务器公钥',
    required: true,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (value?.model.securityMode === 'RPK' || value?.model.securityMode === 'X509') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'lifetime',
    component: 'InputNumber',
    label: '客户端注册生命周期',
    required: true,
    defaultValue: 300,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
      parser: formatNumber,
    },
  },
  {
    field: 'defaultMinPeriod',
    component: 'InputNumber',
    label: '最小期限',
    required: true,
    defaultValue: 1,
    colProps: {
      span: 12,
    },
    componentProps: {
      min: 0,
      parser: formatNumber,
    },
  },
  {
    field: 'binding',
    component: 'Select',
    label: '绑定',
    required: true,
    colProps: {
      span: 24,
    },
    defaultValue: 'U',
    componentProps: {
      options: [
        { label: 'U: 客户端通过UDP绑定。', value: 'U' },
        { label: 'M: 客户端通过MQTT绑定。', value: 'M' },
        { label: 'H: 客户端通过HTTP绑定。', value: 'H' },
        { label: 'T: 客户端通过TCP绑定。', value: 'T' },
        { label: 'S: 客户端通过SMS绑定。', value: 'S' },
        { label: 'N: 客户端通过非IP绑定将响应发送到请求(支持LWM2M 1.1)。', value: 'N' },
        { label: 'UQ: 通过UDP队列模式连接(不支持LWM2M 1.1)。', value: 'UQ' },
        { label: 'UQS: 通过UDP和SMS活动连接(不支持LWM2M 1.1)。', value: 'UQS' },
        { label: 'TQ: 通过TCP队列模式连接(不支持LWM2M 1.1)。', value: 'TQ' },
        { label: 'TQS: 通过TCP和SMS活动连接(不支持LWM2M 1.1)。', value: 'TQS' },
        { label: 'SQ: 通过队列模式的SMS连接(不支持LWM2M 1.1)', value: 'SQ' },
      ],
      listHeight: 160,
      virtual: false,
      allowClear: false,
    },
  },
  {
    field: 'notifIfDisabled',
    component: 'Checkbox',
    label: '禁用或离线时通知存储',
    required: true,
    defaultValue: true,
    colProps: {
      span: 24,
    },
  },
];

function formatNumber(text) {
  const decimalIndex = text.indexOf('.');
  return /^\d+$/.test(text) ? text : text.slice(0, decimalIndex);
}
