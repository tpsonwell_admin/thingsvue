import { FormSchema } from '/@/components/Form/index';
import { useMessage } from '/@/hooks/web/useMessage';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import Icon from '/@/components/Icon';
import {
  getRuleChainList,
  getDashboards,
  getQueues,
} from '/@/api/thingsborad/profile/deviceProfile/deviceProfile';

export const schemas: FormSchema[] = [
  {
    field: 'name',
    component: 'Input',
    label: '名称',
    colProps: {
      span: 12,
    },
    required: true,
  },
  {
    field: 'defaultRuleChainId',
    component: 'ApiSelect',
    label: '默认规则链',
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'CORE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
    colProps: {
      span: 12,
    },
  },
  {
    field: 'defaultDashboardId',
    component: 'ApiSelect',
    label: '移动端仪表盘',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getDashboards,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'title',
        sortOrder: 'ASC',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
  },
  {
    field: 'defaultQueueName',
    component: 'ApiSelect',
    label: '队列',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getQueues,
      params: {
        pageSize: 10,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        serviceType: 'TB_RULE_ENGINE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'name',
      immediate: true,
    },
  },
  {
    field: 'defaultEdgeRuleChainId',
    component: 'ApiSelect',
    label: '默认边缘规则链',
    colProps: {
      span: 12,
    },
    componentProps: {
      api: getRuleChainList,
      params: {
        pageSize: 50,
        page: 0,
        sortProperty: 'name',
        sortOrder: 'ASC',
        type: 'EDGE',
      },
      resultField: 'data',
      labelField: 'name',
      valueField: 'value',
      immediate: true,
      onOptionsChange: async (res) => {
        res.forEach((item) => {
          item.value = JSON.stringify(item.id);
        });
      },
    },
  },
  {
    field: 'image',
    component: 'Upload',
    slot: 'img',
    label: '设备配置图片',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'description',
    component: 'InputTextArea',
    label: '说明',
    colProps: {
      span: 24,
    },
  },
];

export const schemasPreCfig: FormSchema[] = [
  {
    field: 'type',
    component: 'Select',
    label: '预配置策略',
    colProps: {
      span: 24,
    },
    componentProps: {
      options: [
        { label: '禁用', value: 'DISABLED' },
        { label: '允许创建新设备', value: 'ALLOW_CREATE_NEW_DEVICES' },
        { label: '检查预配置的设备', value: 'CHECK_PRE_PROVISIONED_DEVICES' },
        { label: 'X509证书链', value: 'X509_CERTIFICATE_CHAIN' },
      ],
    },
    defaultValue: 'DISABLED',
  },
  {
    field: 'provisionDeviceKey',
    component: 'Input',
    label: '预配置设配密钥名',
    defaultValue: generate(20),
    ifShow: (value) => {
      if (
        value?.model?.type === 'ALLOW_CREATE_NEW_DEVICES' ||
        value?.model?.type === 'CHECK_PRE_PROVISIONED_DEVICES'
      ) {
        if (!value.model.provisionDeviceKey) {
          value.model.provisionDeviceKey = generate(20);
        }
        return true;
      } else {
        return false;
      }
    },
    renderComponentContent: (data) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const copy = () => {
        clipboardRef.value = data.model.provisionDeviceKey;
        createMessage.success('复制成功');
      };
      return {
        suffix: () => {
          return (
            <a-button onClick={copy} type="text" size="large" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </a-button>
          );
        },
      };
    },
    colProps: {
      span: 12,
    },
  },
  {
    field: 'provisionDeviceSecret',
    component: 'Input',
    label: '预配置设配密钥',
    defaultValue: generate(20),
    ifShow: (value) => {
      if (
        value?.model?.type === 'ALLOW_CREATE_NEW_DEVICES' ||
        value?.model?.type === 'CHECK_PRE_PROVISIONED_DEVICES'
      ) {
        if (!value.model.provisionDeviceSecret) {
          value.model.provisionDeviceSecret = generate(20);
        }
        return true;
      } else {
        return false;
      }
    },
    renderComponentContent: (data) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const copy = () => {
        clipboardRef.value = data.model.provisionDeviceSecret;
        createMessage.success('复制成功');
      };
      return {
        suffix: () => {
          return (
            <a-button onClick={copy} type="text" size="large" shape="circle">
              <Icon icon="ant-design:copy-outlined"></Icon>
            </a-button>
          );
        },
      };
    },
    colProps: {
      span: 12,
    },
  },
  {
    field: 'allowCreateNewDevicesByX509Certificate',
    component: 'Switch',
    label: '创建新设备',
    subLabel: '如果选择创新新设备,则将客户端证书用作设备凭据',
    colProps: {
      span: 24,
    },
    defaultValue: true,
    ifShow: (value) => {
      if (value?.model?.type === 'X509_CERTIFICATE_CHAIN') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'PEMprovisionDeviceSecret',
    component: 'InputTextArea',
    label: 'PEM格式的证书',
    colProps: {
      span: 24,
    },
    required: true,
    componentProps: {
      style: {
        height: '100px',
      },
    },
    ifShow: (value) => {
      if (value?.model?.type === 'X509_CERTIFICATE_CHAIN') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'certificateRegExPattern',
    component: 'Input',
    label: 'CN正则表达式变量',
    colProps: {
      span: 24,
    },
    required: true,
    ifShow: (value) => {
      if (value?.model?.type === 'X509_CERTIFICATE_CHAIN') {
        return true;
      } else {
        return false;
      }
    },
  },
];
function generate(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;

  // 保证生成固定长度的字符串
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}
