import { FormSchema } from '/@/components/Form/index';

export const mqttSchemas: FormSchema[] = [
  {
    field: 'sparkplug',
    component: 'Checkbox',
    label: '',
    renderComponentContent: 'MQTT Sparkplug B边缘网络(EoN)节点',
    defaultValue: false,
    colProps: {
      span: 24,
    },
  },
  {
    field: 'sparkplugAttributesMetricNames',
    component: 'Input',
    label: '',
    slot: 'sparkplugAttributesMetricNames',
    colProps: {
      span: 24,
    },
    show: (value) => {
      if (value?.model.sparkplug === true) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'deviceTelemetryTopic',
    component: 'Input',
    label: '遥测数据Topic筛选器',
    required: true,
    colProps: {
      span: 8,
    },
    defaultValue: 'v1/devices/me/telemetry',
    show: (value) => {
      if (value?.model.sparkplug === false) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'deviceAttributesTopic',
    component: 'Input',
    label: '属性Topic筛选器',
    required: true,
    colProps: {
      span: 8,
    },
    defaultValue: 'v1/devices/me/attributes',
    show: (value) => {
      if (value?.model.sparkplug === false) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'deviceAttributesSubscribeTopic',
    component: 'Input',
    label: '订阅属性Topic筛选器',
    required: true,
    colProps: {
      span: 8,
    },
    defaultValue: 'v1/devices/me/attributes',
    show: (value) => {
      if (value?.model.sparkplug === false) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.transportPayloadType',
    label: 'Payload',
    component: 'Select',
    colProps: {
      span: 24,
    },
    defaultValue: 'JSON',
    componentProps: {
      allowClear: false,
      options: [
        { label: 'JSON', value: 'JSON' },
        { label: 'Protobuf', value: 'PROTOBUF' },
      ],
    },
    show: (value) => {
      if (value?.model.sparkplug === false) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.enableCompatibilityWithJsonPayloadFormat',
    label: '',
    component: 'Checkbox',
    renderComponentContent: '启用与其他payload格式兼容',
    defaultValue: false,
    colProps: {
      span: 24,
    },
    show: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.useJsonPayloadFormatForDefaultDownlinkTopics',
    label: '',
    component: 'Checkbox',
    renderComponentContent: '缺省下行主题采用json格式',
    defaultValue: false,
    colProps: {
      span: 24,
    },
    show: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model[
          'transportPayloadTypeConfiguration.enableCompatibilityWithJsonPayloadFormat'
        ] === true &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.deviceAttributesProtoSchema',
    component: 'Input',
    slot: 'deviceAttributesProtoSchema',
    label: '遥测数据proto schema',
    required: true,
    defaultValue: `
    syntax ="proto3";
    package attributes;

    message SensorConfiguration {
      optional string firmwareVersion = 1;
      optional string serialNumber = 2;
    }
    `,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.deviceRpcRequestProtoSchema',
    component: 'Input',
    label: '属性proto schema',
    slot: 'deviceRpcRequestProtoSchema',
    required: true,
    defaultValue: `
    syntax ="proto3";
    package rpc;
    
    message RpcRequestMsg {
      optional string method = 1;
      optional int32 requestId = 2;
      optional string params = 3;
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.deviceRpcResponseProtoSchema',
    component: 'Input',
    label: 'RPC 请求 proto schema',
    slot: 'deviceRpcResponseProtoSchema',
    required: true,
    defaultValue: `
    syntax ="proto3";
    package rpc;
    
    message RpcResponseMsg {
      optional string payload = 1;
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'transportPayloadTypeConfiguration.deviceTelemetryProtoSchema',
    component: 'Input',
    label: 'RPC 相应 proto schema',
    slot: 'deviceTelemetryProtoSchema',
    required: true,
    defaultValue: `
    syntax ="proto3";
    package telemetry;
    
    message SensorDataReading {
    
      optional double temperature = 1;
      optional double humidity = 2;
      InnerObject innerObject = 3;
    
      message InnerObject {
        optional string key1 = 1;
        optional bool key2 = 2;
        optional double key3 = 3;
        optional int32 key4 = 4;
        optional string key5 = 5;
      }
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['transportPayloadTypeConfiguration.transportPayloadType'] === 'PROTOBUF' &&
        value?.model.sparkplug === false
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'sendAckOnValidationException',
    component: 'Checkbox',
    label: '',
    renderComponentContent: '发布消息验证失败时发送PUBACK',
    colProps: {
      span: 24,
    },
    defaultValue: false,
    show: (value) => {
      if (value?.model.sparkplug === false) {
        return true;
      } else {
        return false;
      }
    },
  },
];

export const coapSchemas: FormSchema[] = [
  {
    field: 'coapDeviceTypeConfiguration.coapDeviceType',
    component: 'Select',
    label: 'CoAP设备类型',
    colProps: {
      span: 24,
    },
    defaultValue: 'DEFAULT',
    componentProps: {
      allowClear: false,
      options: [
        { label: '默认', value: 'DEFAULT' },
        { label: 'Efento NB-IoT', value: 'EFENTO' },
      ],
    },
  },
  {
    field: 'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.transportPayloadType',
    label: 'Payload',
    component: 'Select',
    colProps: {
      span: 24,
    },
    defaultValue: 'JSON',
    componentProps: {
      allowClear: false,
      options: [
        { label: 'JSON', value: 'JSON' },
        { label: 'Protobuf', value: 'PROTOBUF' },
      ],
    },
    ifShow: (value) => {
      if (value?.model['coapDeviceTypeConfiguration.coapDeviceType'] === 'DEFAULT') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field:
      'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.deviceAttributesProtoSchema',
    component: 'Input',
    slot: 'deviceAttributesProtoSchema',
    label: '遥测数据proto schema',
    defaultValue: `
    syntax ="proto3";
    package attributes;

    message SensorConfiguration {
      optional string firmwareVersion = 1;
      optional string serialNumber = 2;
    }
    `,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['coapDeviceTypeConfiguration.coapDeviceType'] === 'DEFAULT' &&
        value?.model[
          'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.transportPayloadType'
        ] === 'PROTOBUF'
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field:
      'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.deviceRpcRequestProtoSchema',
    component: 'Input',
    label: '属性proto schema',
    slot: 'deviceRpcRequestProtoSchema',
    defaultValue: `
    syntax ="proto3";
    package rpc;
    
    message RpcRequestMsg {
      optional string method = 1;
      optional int32 requestId = 2;
      optional string params = 3;
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['coapDeviceTypeConfiguration.coapDeviceType'] === 'DEFAULT' &&
        value?.model[
          'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.transportPayloadType'
        ] === 'PROTOBUF'
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field:
      'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.deviceRpcResponseProtoSchema',
    component: 'Input',
    label: 'RPC 请求 proto schema',
    slot: 'deviceRpcResponseProtoSchema',
    defaultValue: `
    syntax ="proto3";
    package rpc;
    
    message RpcResponseMsg {
      optional string payload = 1;
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['coapDeviceTypeConfiguration.coapDeviceType'] === 'DEFAULT' &&
        value?.model[
          'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.transportPayloadType'
        ] === 'PROTOBUF'
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field:
      'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.deviceTelemetryProtoSchema',
    component: 'Input',
    label: 'RPC 相应 proto schema',
    slot: 'deviceTelemetryProtoSchema',
    defaultValue: `
    syntax ="proto3";
    package telemetry;
    
    message SensorDataReading {
    
      optional double temperature = 1;
      optional double humidity = 2;
      InnerObject innerObject = 3;
    
      message InnerObject {
        optional string key1 = 1;
        optional bool key2 = 2;
        optional double key3 = 3;
        optional int32 key4 = 4;
        optional string key5 = 5;
      }
    }`,
    colProps: {
      span: 24,
    },
    ifShow: (value) => {
      if (
        value?.model['coapDeviceTypeConfiguration.coapDeviceType'] === 'DEFAULT' &&
        value?.model[
          'coapDeviceTypeConfiguration.transportPayloadTypeConfiguration.transportPayloadType'
        ] === 'PROTOBUF'
      ) {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'clientSettings.powerMode',
    component: 'Select',
    label: '节能模式',
    colProps: {
      span: 24,
    },
    defaultValue: 'DRX',
    componentProps: {
      allowClear: false,
      options: [
        { label: '节能模式(PSM)', value: 'PSM' },
        { label: '非连续接收(DRX)', value: 'DRX' },
        { label: '连续接收(eDRX)', value: 'E_DRX' },
      ],
    },
  },
  {
    field: 'clientSettings.psmActivityTimer',
    component: 'InputNumber',
    label: 'PSM活动计时器',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    required: true,
    componentProps: {
      min: '0',
      parser: formatNumber,
    },
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'clientSettings.timeUnitsPSM',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'PSM') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'clientSettings.edrxCycle',
    component: 'InputNumber',
    label: 'eDRX循环',
    colProps: {
      span: 12,
    },
    required: true,
    defaultValue: 81,
    componentProps: {
      min: '0',
      parser: formatNumber,
    },
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'clientSettings.timeUnitsCyc',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
  {
    field: 'clientSettings.pagingTransmissionWindow',
    component: 'InputNumber',
    label: '分页传输窗口',
    colProps: {
      span: 12,
    },
    defaultValue: 10,
    required: true,
    componentProps: {
      min: '0',
      parser: formatNumber,
    },
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: 'clientSettings.timeUnitsTra',
    component: 'Select',
    label: '时间单位',
    colProps: {
      span: 12,
    },
    defaultValue: '秒',
    ifShow: (value) => {
      if (value?.model['clientSettings.powerMode'] === 'E_DRX') {
        return true;
      } else {
        return false;
      }
    },
    componentProps: {
      allowClear: false,
      options: [
        { label: '毫秒', value: '毫秒' },
        { label: '秒', value: '秒' },
        { label: '分钟', value: '分钟' },
        { label: '小时', value: '小时' },
      ],
    },
  },
];

function formatNumber(text) {
  const decimalIndex = text.indexOf('.');
  return /^\d+$/.test(text) ? text : text.slice(0, decimalIndex);
}

export const keyWordsTranslation = {
  EQUAL: '等于',
  NOT_EQUAL: '不等于',
  STARTS_WITH: '开始于',
  ENDS_WITH: '结束于',
  CONTAINS: '包含',
  NOT_CONTAINS: '不包含',
  IN: '存在',
  NOT_IN: '不存在',
  GREATER: '大于',
  LESS: '小于',
  GREATER_OR_EQUAL: '大于或等于',
  LESS_OR_EQUAL: '小于或等于',
  ANY_TIME: '始终启用',
  SPECIFIC_TIME: '定时启用',
  CUSTOM: '自定义启用',
  true: '真',
  false: '假',
  CURRENT_TENANT: '当前租户',
  CURRENT_CUSTOMER: '当前客户',
  CURRENT_DEVICE: '当前设备',
};
