import { DescItem } from '/@/components/Description/index';
import { JsonPreview } from '/@/components/CodeEditor';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

const toDuration = (duration: number) => {
  let text = '';
  const Y = dayjs.duration(duration).years() == 0 ? '' : dayjs.duration(duration).years() + '年';
  const M = dayjs.duration(duration).months() == 0 ? '' : dayjs.duration(duration).months() + '月';
  const D = dayjs.duration(duration).days() == 0 ? '' : dayjs.duration(duration).days() + '天';
  const h = dayjs.duration(duration).hours() == 0 ? '' : dayjs.duration(duration).hours() + '时';
  const m =
    dayjs.duration(duration).minutes() == 0 ? '' : dayjs.duration(duration).minutes() + '分';
  const s =
    dayjs.duration(duration).seconds() == 0 ? '' : dayjs.duration(duration).seconds() + '秒';
  text = Y + M + D + h + m + s;
  return text;
};
export const detailSchema: DescItem[] = [
  {
    field: 'originatorName',
    label: '发起者',
  },
  {
    field: 'severity',
    label: '严重程度',
  },
  {
    field: 'startTs',
    label: '开始时间',
    render: (value) => {
      return dayjs(value).format('YYYY-MM-DD HH:mm:ss');
    },
  },
  {
    field: 'duration',
    label: '持续时间',
    render: (value, data) => {
      let text = '';
      if (data.startTs || data.endTs) {
        if (data.startTs && (data.status === 'ACTIVE_ACK' || data.status === 'ACTIVE_UNACK')) {
          const duration = Date.now() - data.startTs;
          text = toDuration(duration);
        }
        if (data.endTs && (data.status === 'CLEARED_ACK' || data.status === 'CLEARED_UNACK')) {
          const duration = data.startTs - data.startTs;
          text = toDuration(duration);
        }
      }
      return <span>{text}</span>;
    },
  },
  {
    field: 'type',
    label: '类型',
  },
  {
    field: 'status',
    label: '状态',
    render: (value: string) => {
      let text = '';
      switch (value) {
        case 'ACTIVE_UNACK':
          text = '激活未确认';
          break;
        case 'ACTIVE_ACK':
          text = '激活已确认';
          break;
        case 'CLEARED_UNACK':
          text = '清除未确认';
          break;
        case 'CLEARED_ACK':
          text = '清除已确认';
          break;
      }
      return <span>{text}</span>;
    },
  },
  {
    field: 'details',
    label: '附加信息',
    render: (value: string) => {
      return <JsonPreview data={value} />;
    },
  },
];
