import { FormSchema } from '/@/components/Form';
export interface userInfo {
  sub: string;
  userId: string;
  scopes: object;
  sessionId: string;
  iss: string;
  iat: number;
  exp: number;
  enabled: boolean;
  isPublic: boolean;
  tenantId: string;
  customerId: string;
}
export const schemas: FormSchema[] = [
  {
    field: 'comment',
    component: 'Input',
    label: '',
    slot: 'comment',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'br',
    component: 'Divider',
    label: '评论',
    componentProps: {
      orientation: 'center',
    },
    colProps: {
      span: 24,
    },
  },
];
