import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { getUsers, getAlarmTypeList, getTrustedList } from '/@/api/thingsborad/alarm';
import { ref } from 'vue';
import { useDebounceFn } from '@vueuse/core';

export const columns: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'createdTime',
    format: 'date|YYYY-MM-DD HH:mm:ss',
    sorter: true,
  },
  {
    title: '发起者',
    dataIndex: 'originatorName',
  },
  {
    title: '类型',
    dataIndex: 'type',
    sorter: true,
  },
  {
    title: '严重程度',
    dataIndex: 'severity',
    customRender: ({ value }) => {
      let text = '';
      switch (value) {
        case 'CRITICAL':
          text = '危险';
          break;
        case 'MAJOR':
          text = '重要';
          break;
        case 'MINOR':
          text = '次要';
          break;
        case 'WARNING':
          text = '警告';
          break;
        case 'INDETERMINATE':
          text = '不确定';
          break;
      }
      return <span>{text}</span>;
    },
    sorter: true,
  },
  {
    title: '受托人',
    dataIndex: 'assignOptionsValue',
    edit: true,
    editComponent: 'ApiSelect',
    editComponentProps: ({ record }) => {
      return {
        listHeight: 160,
        api: getUsers,
        params: {
          pagination: {
            page: 0,
            pageSize: 50,
            sortProperty: 'email',
            sortOrder: 'ASC',
          },
          alarmId: record.id,
        },
        resultField: 'data',
        labelField: '',
        valueField: 'id.id',
        immediate: true,
        onOptionsChange: (options) => {
          options.forEach((item) => {
            item.label =
              item.firstName && item.lastName ? item.firstName + '  ' + item.lastName : item.email;
          });
          options.unshift({ label: '未分配', value: 'unassigned' });
        },
      };
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    customRender: ({ value }) => {
      let text = '';
      switch (value) {
        case 'ACTIVE_UNACK':
          text = '激活未确认';
          break;
        case 'ACTIVE_ACK':
          text = '激活已确认';
          break;
        case 'CLEARED_UNACK':
          text = '清除未确认';
          break;
        case 'CLEARED_ACK':
          text = '清除已确认';
          break;
      }
      return <span>{text}</span>;
    },
    sorter: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'status',
    label: '',
    component: 'Select',
    componentProps: {
      placeholder: '请选择告警状态',
      maxTagCount: 'responsive',
      mode: 'multiple',
      options: [
        { label: '激活', value: 'ACTIVE' },
        { label: '已清除', value: 'CLEARED' },
        { label: '已确认', value: 'ACK' },
        { label: '未确认', value: 'UNACK' },
      ],
    },
  },
  {
    field: 'severity',
    label: '',
    component: 'Select',
    componentProps: {
      placeholder: '请选择警报严重性',
      maxTagCount: 'responsive',
      mode: 'multiple',
      options: [
        { label: '危险', value: 'CRITICAL' },
        { label: '重要', value: 'MAJOR' },
        { label: '次要', value: 'MINOR' },
        { label: '警告', value: 'WARNING' },
        { label: '不确定', value: 'INDETERMINATE' },
      ],
    },
  },
  {
    field: 'textSearch',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入关键字',
    },
  },

  {
    field: 'assigneeId',
    label: '',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 50,
        sortProperty: 'email',
        sortOrder: 'ASC',
        textSearch: '',
      });
      return {
        listHeight: 160,
        placeholder: '请选择受托人',
        api: getTrustedList,
        showSearch: true,
        params: params.value,
        immediate: false,
        resultField: 'data',
        labelField: '',
        valueField: 'id.id',
        onOptionsChange: (options) => {
          options.forEach((item) => {
            item.label =
              item.firstName && item.lastName ? item.firstName + '  ' + item.lastName : item.email;
          });
          options.unshift({ label: '所有', value: null });
        },
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
  },
  {
    field: 'type',
    label: '',
    component: 'ApiSelect',
    componentProps: () => {
      const params = ref({
        page: 0,
        pageSize: 50,
        textSearch: '',
      });
      return {
        placeholder: '请选择告警类型',
        listHeight: 160,
        maxTagCount: 'responsive',
        mode: 'multiple',
        api: getAlarmTypeList,
        params: params.value,
        resultField: 'data',
        labelField: 'type',
        valueField: 'type',
        immediate: false,
        onSearch: useDebounceFn((e) => {
          params.value.textSearch = e;
        }, 300),
      };
    },
  },
];
