import { FormSchema } from '/@/components/Form/index';

export const schemas: FormSchema[] = [
  {
    field: 'branch',
    label: '分支',
    component: 'Input',
    componentProps: { placeholder: '请输入分支' },
    required: true,
  },
  {
    field: 'versionName',
    label: '版本名称',
    component: 'Input',
    componentProps: { placeholder: '请输入版本名称' },
    required: true,
  },
  {
    field: 'syncStrategy',
    label: '同步策略',
    component: 'Select',
    componentProps: {
      allowClear: false,
      placeholder: '请选择默认同步策略',
      options: [
        { label: '合并', value: 'MERGE' },
        { label: '覆盖', value: 'OVERWRITE' },
      ],
    },
    required: true,
  },
  {
    field: 'slot',
    component: 'Input',
    label: '导出的实体',
    slot: 'CollapseSlot',
    colProps: { span: 24 },
  },
];
