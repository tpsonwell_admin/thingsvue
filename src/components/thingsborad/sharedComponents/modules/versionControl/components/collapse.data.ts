export interface List {
  label: string;
  value: string;
  ifShow?: Boolean;
}
export const typeList: List[] = [
  { label: '资产', value: 'ASSET', ifShow: true },
  { label: '设备', value: 'DEVICE', ifShow: true },
  { label: '实体视图', value: 'ENTITY_VIEW', ifShow: true },
  { label: '仪表盘', value: 'DASHBOARD', ifShow: true },
  { label: '客户', value: 'CUSTOMER', ifShow: true },
  { label: '设备配置', value: 'DEVICE_PROFILE', ifShow: true },
  { label: '资产配置', value: 'ASSET_PROFILE', ifShow: true },
  { label: '规则链库', value: 'RULE_CHAIN', ifShow: true },
  { label: 'Widgets', value: 'WIDGETS_BUNDLE', ifShow: true },
  { label: '部件包', value: 'WIDGETS_BUNDLE', ifShow: true },
  { label: 'Notification templates', value: 'NOTIFICATION_TEMPLATE', ifShow: true },
  { label: 'Notification recipients', value: 'NOTIFICATION_TARGET', ifShow: true },
  { label: 'Notification rules', value: 'NOTIFICATION_RULE', ifShow: true },
];
