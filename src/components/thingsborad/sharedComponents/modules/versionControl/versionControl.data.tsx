import { BasicColumn } from '/@/components/Table/src/types/table';
import { FormSchema } from '/@/components/Form';
import { Icon } from '/@/components/Icon';
import { useMessage } from '/@/hooks/web/useMessage';
import { useCopyToClipboard } from '/@/hooks/web/useCopyToClipboard';
import { Button } from 'ant-design-vue';
import { StyleValue } from 'vue';
import { getBranches } from '/@/api/thingsborad/advanced/versionControl/versionControl';

export const versionControlTableSchema: BasicColumn[] = [
  {
    title: '创建时间',
    dataIndex: 'timestamp',
    width: 200,
    sorter: true,
    format: 'date|YYYY-MM-DD HH:mm:ss',
  },
  {
    title: '版本Id',
    dataIndex: 'id',
    customRender: (options) => {
      const { clipboardRef } = useCopyToClipboard();
      const { createMessage } = useMessage();
      const style: StyleValue = {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        wordBreak: 'keep-all',
        margin: '0',
      };
      const copy = () => {
        clipboardRef.value = options.value;
        createMessage.success('复制成功');
      };
      return (
        <div v-show={options.value != ''} style={{ display: 'flex', justifyContent: 'center' }}>
          <p style={style}>{options.value}</p>
          {/* <span>{options.value}</span> */}
          <Button onClick={copy} type="text" size="small" shape="circle">
            <Icon icon="ant-design:copy-outlined"></Icon>
          </Button>
        </div>
      );
    },
  },
  {
    title: '版本名称',
    width: 100,
    dataIndex: 'name',
  },
  {
    title: '作者',
    dataIndex: 'author',
  },
];

export const searchSchema: FormSchema[] = [
  {
    field: 'branch',
    label: '',
    component: 'ApiSelect',
    componentProps: ({ formActionType }) => {
      return {
        placeholder: '请选择分支',
        listHeight: 160,
        api: getBranches,
        labelField: 'name',
        valueField: 'name',
        immediate: true,
        onOptionsChange: (value) => {
          const { setFieldsValue } = formActionType;
          value.forEach((item) => {
            item.default ? setFieldsValue({ branch: item.label }) : null;
          });
        },
      };
    },
    defaultValue: 'main',
  },
  {
    field: 'textSearch',
    label: '',
    component: 'Input',
    componentProps: {
      placeholder: '请输入关键字',
    },
  },
];
