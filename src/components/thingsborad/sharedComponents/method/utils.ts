import dayjs from 'dayjs';
import * as crypto from 'crypto';
import { log } from 'console';

//
/**
 * 计算到现在的时间过去了多久
 * 时间格式为时间戳
 * @param {string} pastTimestamp   -  需要计算的过去时间。
 * @returns {string} 返回过去了几秒几分钟几小时几天几周。
 */
export function getTimeInterval(pastTimestamp: string): string {
  // 将时间戳转换为 Day.js 对象
  // 存储计算值
  let pastData;
  // 获取当前时间
  const now = dayjs();
  const pastDate = dayjs(pastTimestamp, 'YYYY-MM-DD HH:mm:ss');
  // 计算与当前时间的差值
  const secondsAgo = now.diff(pastDate, 'second');
  const minutesAgo = now.diff(pastDate, 'minute');
  const hoursAgo = now.diff(pastDate, 'hour');
  const daysAgo = now.diff(pastDate, 'day');
  const weeksAgo = now.diff(pastDate, 'week');

  // 判断是否过去了几秒、几天或几周
  if (secondsAgo > 0 && secondsAgo <= 60) {
    pastData = `${secondsAgo}秒`;
  } else if (minutesAgo > 0 && minutesAgo <= 60) {
    pastData = `${minutesAgo}分钟`;
  } else if (hoursAgo > 0 && hoursAgo <= 24) {
    pastData = `${hoursAgo}小时`;
  } else if (daysAgo > 0 && daysAgo <= 7) {
    pastData = `${daysAgo}天`;
  } else {
    pastData = `${weeksAgo}周`;
  }

  return pastData;
}

/**
 * 随机生成指定长度的字符串
 * @param {string} length   -  需要生成的字符串长度
 * @returns {string} 返回指定长度的字符串。
 * 使用了 Node.js 的 crypto 模块中的 randomInt 函数来生成随机索引，然后根据该索引从预定义的字符集中选取字符拼接成最终的随机字符串。
 */
export function generateRandomString(length = 16): string {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length - 2; i++) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
}
