// UserTaskNode.js
import { RectNode, RectNodeModel } from '@logicflow/core';
class TBRectView extends RectNodeModel {
  initNodeData(data) {
    super.initNodeData(data);
    const properties = this.properties;
    this.draggable = properties.type == 'input' ? false : true;
    this.width = 170;
    this.height = 40;
    this.radius = 5;
    const rule = {
      message: '只允许右锚点连接左锚点，且是一对一关系',
      validate: (sourceNode, targetNode, sourceAnchor, targetAnchor) => {
        return (
          sourceNode.outgoing.nodes.length < 1 &&
          sourceAnchor.type === 'right' &&
          targetAnchor.type === 'left' &&
          targetNode.id != sourceNode.incoming.nodes[0]?.id
        );
      },
    };
    this.sourceRules.push(rule);
  }
  // 节点样式
  getNodeStyle() {
    const style = super.getNodeStyle();
    style.stroke = 'black';
    const properties = this.properties;
    switch (properties.type) {
      case 'action':
        style.fill = '#f1928f';
        break;
      case 'attributes':
        style.fill = '#f1e861';
        break;
      case 'change':
        style.fill = '#79cef1';
        break;
      default:
        style.fill = '#a3eaa9';
        break;
    }
    return style;
  }
  // 定义锚点样式
  getAnchorStyle(anchorInfo) {
    const style = super.getAnchorStyle(anchorInfo);
    if (anchorInfo.type === 'left') {
      style.fill = 'green';
      style.hover.fill = 'transparent';
      style.hover.stroke = 'transpanrent';
      style.className = 'lf-hide-default';
    } else {
      style.fill = 'green';
    }
    return style;
  }
  // 锚点个数
  getDefaultAnchor() {
    const { width, height, x, y, id } = this;
    const properties = this.properties;
    return properties.type == 'input'
      ? [
          {
            x: x + width / 2,
            y,
            type: 'right',
            id: `${id}_1`,
          },
        ]
      : [
          {
            x: x - width / 2,
            y,
            type: 'left',
            id: `${id}_0`,
          },
          {
            x: x + width / 2,
            y,
            type: 'right',
            id: `${id}_1`,
          },
        ];
  }
}

class TBRectModel extends RectNode {}

export default {
  type: 'TBRect',
  view: TBRectModel,
  model: TBRectView,
};

export const processSchemas = (text: string, schema: any) => {
  let list: Array<any> = [];
  switch (text) {
    case 'save attributes':
      list = [
        ...schema,
        {
          field: 'sendAttributesUpdatedNotification',
          label: 'Send attributes updated notification',
          component: 'Switch',
          ifShow: ({ model }) => model.scope == 'SERVER_SCOPE' || model.scope == 'SHARED_SCOPE',
        },
        {
          field: 'notifyDevice',
          label: 'Notify device',
          component: 'Switch',
          ifShow: ({ model }) => model.scope == 'SHARED_SCOPE',
        },
        {
          field: 'description',
          label: '描述',
          component: 'InputTextArea',
          componentProps: {
            placeholder: '请输入描述',
          },
          colProps: {
            span: 24,
          },
        },
      ];
      break;
    default:
      list = schema;
      break;
  }
  return list;
};
