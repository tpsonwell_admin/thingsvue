import { PolylineEdgeModel, PolylineEdge, EdgeData } from '@logicflow/core';

class SequenceModel extends PolylineEdgeModel {
  getEdgeStyle() {
    const style = super.getEdgeStyle();
    const { properties } = this;
    if (properties.isActived) {
      style.strokeDasharray = '4 4';
    }
    style.stroke = 'orange';
    return style;
  }
  getData(): EdgeData & { pointsList: { x: any; y: any }[] } {
    const data = super.getData();
    data.properties = {
      schemas: [],
    };
    return data;
  }
}

export default {
  type: 'sequence',
  view: PolylineEdge,
  model: SequenceModel,
};
