export interface ListItem {
  id: Recordable;
  subject: string;
  text: string;
  actionButtonConfig: Recordable;
  icon: Recordable;
  createdTime: number;
  info: Recordable;
}

export function formatTimeAgo(timestamp: number): string {
  // 计算与当前时间相差的时间
  const currentTime = Date.now();
  const timeDiff = currentTime - timestamp;

  // 计算相差的分钟数
  const minutes = Math.floor(timeDiff / (1000 * 60));

  // 计算相差的小时数
  const hours = Math.floor(minutes / 60);

  // 计算相差的天数
  const days = Math.floor(hours / 24);

  // 计算相差的周数
  const weeks = Math.floor(days / 7);

  // 计算相差的月数
  const months = Math.floor(days / 30);

  // 计算相差的年数
  const years = Math.floor(days / 365);

  // 根据时间差返回相应的字符串
  if (minutes < 1) {
    return '刚刚';
  } else if (minutes < 60) {
    return `${minutes}分钟前`;
  } else if (hours < 24) {
    return `${hours}小时前`;
  } else if (days < 7) {
    return `${days}天前`;
  } else if (weeks <= 4) {
    return `${weeks}周前`;
  } else if (months < 12) {
    return `${months}月前`;
  }

  return `${years}年前`;
}
