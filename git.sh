#!/bin/bash

# 函数
function echoMsg(){
  echo -e ${1}
}
git add .

#是否是主分支
read -p "是否是主分支[y/n]" branch

if [ "$branch" == "y" ]
  then 
    # 选择提交类型
    read -p "提交类型 [a/u/f]：" comfirm
    #新增
    if [ "$comfirm" == "a" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "add: #$gitCommit"
    #更新
    elif [ "$comfirm" == "u" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "feat: #$gitCommit"
    #修复
    elif [ "$comfirm" == "f" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "fix: #$gitCommit"
    fi
    echoMsg "\n\n*************** 正在提交代码... ***************\n\n"
    git push -u origin master
  else
    read -p "请输入分支名称：" branchName
    # 选择提交类型
    read -p "提交类型 [a/u/f]：" comfirm
    #新增
    if [ "$comfirm" == "a" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "add: #$gitCommit"
    #更新
    elif [ "$comfirm" == "u" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "feat: #$gitCommit"
    #修复
    elif [ "$comfirm" == "f" ]
      then
    read -p "输入commit信息：" gitCommit
    git commit -m "fix: #$gitCommit"
    fi
    echoMsg "\n\n*************** 正在提交代码... ***************\n\n"
    git push origin $branchName
fi
read -p "脚本运行结束"
