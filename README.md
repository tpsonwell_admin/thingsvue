<center class ='img'>
<img  src="./public/img/login.svg" width="45%" />
</center>

<center><h1>ThingsVue</h1></center>

## 🎉 **简介**

ThingsVue 是一个免费开源的中后台模版，是[ThingsBoard](https://github.com/thingsboard/thingsboard)开源物联平台的前端版本。使用了最新的 vue3,vite2,TypeScript 等主流技术开发，开箱即用的中后台前端解决方案，也可用于学习参考。

[ThingsBoard](https://github.com/thingsboard/thingsboard)是一个用于数据收集、处理、可视化和设备管理的开源物联网平台。

[Vue vben admin](https://github.com/vbenjs/vue-vben-admin) + [ThingsBoard](https://github.com/thingsboard/thingsboard) 结合两者的优点致力于打造一款开箱即用且更符合国人的开发习惯的开源物联网平台

## 📌 **特性**

- **最新技术栈**：使用 Vue3/vite2 等前端前沿技术开发
- **TypeScript**: 应用程序级 JavaScript 的语言
- **主题**：可配置的主题
- **国际化**：内置完善的国际化方案
- **权限** 内置完善的动态路由权限生成方案
- **组件** 二次封装了多个常用的组件

## 🍭 **需要掌握的基础知识**

本项目需要一定前端基础知识，请确保掌握 Vue 的基础知识，以便能处理一些常见的问题。建议在开发前先学一下以下内容，提前了解和学习这些知识，会对项目理解非常有帮助:

- [Vue3 文档](https://vuejs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Vue-router](https://router.vuejs.org/)
- [Ant-Design-Vue](https://www.antdv.com/components/overview-cn)
- [Es6](https://es6.ruanyifeng.com/)
- [Vitejs](https://vitejs.dev/)
- [WindiCss](https://windicss.netlify.app/)
- [Echarts](https://echarts.apache.org/zh/index.html)
- [Antv/x6](https://x6.antv.antgroup.com/tutorial/about)
- [Pinia](http://pinia.cc/)
- [axios](https://www.axios-http.cn/)

## 🌐 **项目地址**

[Thingsvue](http://thingsvue.tpson.cn:7772/)

```
试用账号：tenant@thingsboard.org
密码：tenant123456
```

## 💻 **预览**

#### 首页

![首页](/public/img/首页.png)

#### 规则链

![规则链](/public/img/规则链.png)

规则引擎是对于处理复杂的事件具有灵活配置和高度定制化的特点，使用规则引擎的 Filter、Enrichment 和 Transform 节点通过设备和相关资产发出输入消息，使用规则引擎的 Action、Externala 节点触发各种操作与通信。基于[@antv/x6](https://x6.antv.vision/zh)实现交互组件和简单易用的节点定制能力 [参考项目](https://gitee.com/oliver225/thingsboard-ui-vue)

1. 规则引擎消息

> 规则引擎消息可以被被序列化并有着规定的数据结构同时可以表示系统中的各种消息，例如：

- 设备遥测, 属性更新或 RPC 调用；
- 实体生命周期事件: created、updated、deleted、assigned、unassigned、属性更新;
- 设备状态事件: connected, disconnected, active, inactive,等；
- 其他事件。

2. 规则节点

> 规则节点是规则引擎的基本组件每次处理单个输入消息并生成一个或多个输出消息。

- 规则节点是规则引擎的主要逻辑单元。
- 规则节点可以是 Filter、Enrichment、Transform 输入消息或者是执行 Action 与 External 节点对外部系统进行通信。

3. 规则节点关系

> 规则节点之间存在关联性每个节点都有对应关系类型，用于标识关系的逻辑标签。

- 当规则节点生成输出消息时，它总是将消息路由到下一个指定的节点并通过关系类型进行关联。
- 表示成功与否的规则节点关系是 Success 和 Failure。
- 表示逻辑运算的规则节点可以是 True 或 False。
- 一些特定的规则节点可能使用完全不同的关系类型例如：“Post Telemetry”、“Attributes Updated”、“Entity Created”等。

4. 规则链

> 规则链是规则节点及其关系的逻辑组；例如：下面的规则链将：

- 将所有遥测消息保存到数据库中;
- 如果消息中的温度字段高于 50 度，则发出“高温警报”;
- 如果消息中的温度字段低于-40 度，则发出“低温警报”;
- 如果在脚本中发生逻辑或语法错误时，则无法执行温度脚本检查控制台记录。

5. 消息处理结果

> 有三种消息处理结果：成功、失败和超时。

- 当消息被规则引擎中所有节点处理成功，那么该消息将被标注为”Success”。
- 当消息被规则引擎中任一节点处理失败，那么此消息将被标记”Failure”。
- 当处理超过配置的阈值时将消息标记为“Timeout”。

#### 设备配置

![设备配置](/public/img/设备配置.png)

#### 仪表盘

![仪表盘](/public/img/仪表盘.png) 所有仪表板都是使用部件库中的部件进行构建从而形成一个最终功能界面提供给用户进行使用，例如：可视化、远程控制、警报和静态 HTML 内容。有 5 种类型部件：

- 图表部件：基于 Echarts 图表显示设备特定历史时间的数据；
- 表格部件：以表格的形式显示与遥测，属性相关的告警信息；
- 控件部件：通过为目标设备指定 RPC 命令向设备下发控制指令例如：保温设备所设置的温度；
- 静态部件：显示静态可自定义的 HTML 内容不使用任何数据源。
- 卡片部件：显示指定属性或时序数据例如：设备型号或最新的温度数据；

#### API 统计

![api](/public/img/api.png)

## 🍮 **准备**

- [node](http://nodejs.org/)和 [git](https://git-scm.com/)-项目开发环境
- [Vite](https://vitejs.dev/)- 熟悉 vite 特性
- [Vue3](https://v3.vuejs.org/)- 熟悉 Vue 基础语法
- [TypeScript](https://www.typescriptlang.org/)- 熟悉`TypeScript`基本语法
- [Es6+](http://es6.ruanyifeng.com/)- 熟悉 es6 基本语法
- [Vue-Router-Next](https://next.router.vuejs.org/)- 熟悉 vue-router 基本使用
- [Ant-Design-Vue](https://2x.antdv.com/docs/vue/introduce-cn/)- ui 基本使用

## 🎽 **安装使用**

获取项目代码

```
git clone https://gitee.com/tpsonwell_admin/thingsvue.git
```

安装依赖

```
yarn
```

运行

```
yarn serve
```

打包

```
yarn build
```

## 🧭 **关于分支**

| 分支名称 | 备注                     |
| -------- | ------------------------ |
| master   | 稳定版, 跟随官方最新版本 |

## ⛄ **APP 端**

基于 uniapp 完成 1.0.0 版本开发，如需使用或者交流请入群或联系负责人

> 请扫码下载体验安卓版

<left class ='img'>
<img title="APP" src="./public/img/basic.png" width="20%" />
</left>

![数字能源](/public/img/app1.png) ![数字能源](/public/img/app2.png)

## 🎨 **企业应用场景实例**

**数字能源**

数字能源综合管理平台是**电力能耗运维管理**的智能解决方案，广泛适用于**政府**、**园区**、**高校**等事业单位。平台基于物联网技术，对电力能耗、电器设备运行参数等数据进行实时采集、分析，帮助企业发现异常能耗、发掘节能潜力，结合用户用电习惯智能分派节能目标，实现对能源的精细化管理，达到提节能降耗、降本增效的目的。 ![数字能源](/public/img/数字能源.png) ![数字能源2](/public/img/数字能源2.png) ![数字能源3](/public/img/数字能源3.jpg)

**消防智能预警监测系统**

消防智能预警监测系统是依托于物联网、大数据、人工智能，区块链等技术，结合电流指纹算法，对各类消防终端监测设备采集数据的分析与运用。实现”70+“款设备接入、“30+“业务场景覆盖、”500+“项目落地。广泛运用于**城市运营**、**社区管理**、**文旅**、**高校**、**园区**、**商业综合体**、**九小场所**等行业。致力于帮忙客户实现消防的智能化改造、提升智能化管理水平、降低监管压力、守护人民生命财产安全。 ![消防智能预警监测系统](/public/img/消防智能预警监测系统.png) ![消防智能预警监测系统2](/public/img/消防智能预警监测系统2.png) ![消防智能预警监测系统3](/public/img/消防智能预警监测系统3.jpg)

**可视化大屏拖拽系统**

可视化大屏拖拽系统是基于[AJ-Report](https://gitee.com/chinait/report)进行开发，实现可视化拖拽编辑的，直观，酷炫，具有科技感的图表工具全开源项目。 内置的基础功能包括数据源，数据集，报表管理，包含图表大屏和组态大屏两大类，项目部分截图如下![电力接线图](/public/img/组态1.png) ![虚拟电厂可视化大屏](/public/img/虚拟电厂.png)

## 🌈 **ThingsVue 迭代计划**

> 我们的开发团队将致力于平台功能的持续精进，定期进行维护与优化，并陆续推出含新功能的迭代版本。确保仪表盘、部件库、规则链等核心模块运作流畅，功能日臻完善，为用户提供优质的使用体验。

📩 若想了解更多更新细节与前沿动态，可以加入我们的开发者社区，与我们一同交流讨论。

#### **web 端**

- [x] V1.0.0 基于 things3.6.2 版本，使用 vue 完成国产化开发
- [x] V1.0.1 部件库与仪表盘初步功能实现
- [x] V1.0.2 APP 初始版本完成
- [ ] V1.0.3 基于 AJ-report 实现可视化大屏拖拽系统，包括图表可视化和组态可视化，预计 11 月初上线，欢迎体验
- [ ] V1.1.0 部件库国产化开发，基于 echarts，实现符合国内开发者和使用者的使用习惯，完成折线图，柱状图，饼图，仪表盘等图表部件，预计 11 月下旬发布
- [ ] V1.1.1 部件库国产化开发，基于 ant-table，实现表格部件
- [ ] V1.1.2 部件库国产化开发，实现最新值部件
- [ ] V1.1.3 部件库国产化开发，实现控件部件
- [ ] V1.1.4 部件库国产化开发，实现静态部件
- [ ] V1.2.0 引入视频监控系统，可接入多种摄像头，实现直播流实时播放

#### **APP 端**

- [x] V1.0.0 基于 things3.6.2 版本，使用 uniapp，实现安卓端 UI 重构
- [ ] V1.0.1 实现第二版 UI 开发，稳定版本输出，预计 11 月底发布，需要体验请扫码进群

🎉 请大家持续关注，watch、star、fork 一键三连

## 🙋‍♂️ **项目请联系**

### 欢迎 bug 反馈，需求建议，技术交流等

QQ 扫码入群（群号：392529836）：

<left class ='img'>
<img title="交流群" src="./public/img/qun.jpg" width="20%" />
</left>

### 项目合作请联系

<left class ='img'>
<img title="扫码沟通" src="./public/img/WechatIMG14.jpg" width="20%" />
</left>
<div>
</div>
