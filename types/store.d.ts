import { ErrorTypeEnum } from '/@/enums/exceptionEnum';
import { MenuModeEnum, MenuTypeEnum } from '/@/enums/menuEnum';
import { RoleInfo } from '/@/api/sys/model/userModel';

export interface SystemParams {
  hasRepository?: boolean;
}

// Lock screen information
export interface LockInfo {
  // Password required
  pwd?: string | undefined;
  // Is it locked?
  isLock?: boolean;
}

// Error-log information
export interface ErrorLogInfo {
  // Type of error
  type: ErrorTypeEnum;
  // Error file
  file: string;
  // Error name
  name?: string;
  // Error message
  message: string;
  // Error stack
  stack?: string;
  // Error detail
  detail: string;
  // Error url
  url: string;
  // Error time
  time?: string;
}

export interface UserInfo {
  id: {
    entityType: string;
    id: string;
  };
  createdTime: number;
  additionalInfo: {
    description: string;
    defaultDashboardId: string;
    defaultDashboardFullscreen: boolean;
    homeDashboardId: string;
    homeDashboardHideToolbar: boolean;
    userCredentialsEnabled: boolean;
    lang: string;
    failedLoginAttempts: number;
    lastLoginTs: number;
  };
  tenantId: {
    entityType: string;
    id: string;
  };
  customerId: {
    entityType: string;
    id: string;
  };
  email: string;
  authority: string;
  firstName: string;
  lastName: string;
  phone: string;
  name: string;
}
export interface BeforeMiniState {
  menuCollapsed?: boolean;
  menuSplit?: boolean;
  menuMode?: MenuModeEnum;
  menuType?: MenuTypeEnum;
}
export interface EntityId<T> {
  entityType?: T;
  id: string;
}
